<?php



class Admin_model extends CI_Model{	

	public function __construct(){
		//database loading
		$this->load->database();
	}

	public function signin($user_name,$password){
		$this->db->select('*')
				->from('login')
				->where('l_user',$user_name)
				->where('l_pwd',md5($password));
		$query=$this->db->get();
		
		// print_r($this->db);exit;
		// var_dump($query->num_rows());
		if($query->num_rows()==1){
			return $query->row_array();
		}
		else return false;
	}
	
	public function insert_item($table,$data){	 
		$this->db->insert($table,$data);		
		return $this->db->insert_id();
	}
	
	public function get_result_array($table,$field='',$id='',$order_field='',$order_type=''){
		$this->db->select('*')
				->from($table);
		
		if($id==''){
			if($order_field!='' && $order_type!=''){
				$this->db->order_by($order_field, $order_type);			
			}
			$query=$this->db->get();
			return $query->result_array();			
		}
		if($id!=''){
			$this->db->where($field,$id);
			$query=$this->db->get();
			return $query->row_array();			
		}
	}	
	
	public function update_data($table,$field,$id,$data){
		$this->db->where($field,$id)
				->update($table,$data);
		return $this->db->affected_rows();

	}
	
	public function get_data_with_one_join($table,$field='',$id='',$join_tb='',$join_tb_field='',$tb_filed=''){
		$this->db->select('*')
				->from($table)				
				->join($join_tb,''.$join_tb.'.'.$join_tb_field.'='.$table.'.'.$tb_filed.'');
		if($id==''){
			$query=$this->db->get();
			return $query->result_array();			
		}
		if($id!=''){
			$this->db->where($field,$id);
			$query=$this->db->get();
			return $query->row_array();			
		}
	}
	
	
	public function get_data_with_two_join($table,$field='',$id='',$join_tb='',$join_tb_field='',$tb_filed='',$join_tb1='',$join_tb_field1='',$tb_filed1=''){
		$this->db->select('*')
				->from($table)				
				->join($join_tb,''.$join_tb.'.'.$join_tb_field.'='.$table.'.'.$tb_filed.'')
				->join($join_tb1,''.$join_tb1.'.'.$join_tb_field1.'='.$table.'.'.$tb_filed1.'');
		if($id==''){
			$query=$this->db->get();
			return $query->result_array();			
		}
		if($id!=''){
			$this->db->where($field,$id);
			$query=$this->db->get();
			return $query->row_array();			
		} 
	}
	
	public function get_result_array_with_one_where($table,$field='',$id=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field,$id);	
		$query=$this->db->get();
	//	print_r($this->db);
		return $query->result_array();	
	}	
	public function get_result_array_with_two_where($table,$field='',$id='',$field1='',$id1=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field,$id);	
			$this->db->where($field1,$id1);	
		$query=$this->db->get();
		// print_r($this->db);exit;
		return $query->result_array();	
	}	
	
	public function get_result_array_with_one_where_one_join($table,$field='',$id='',$join_field='',$j_table1='',$j_field1=''){
		$this->db->select('*')
				->from($table)
				->join($j_table1,''.$j_table1.'.'.$j_field1.'='.$table.'.'.$join_field.'');
		$this->db->where($field,$id);	
		$query=$this->db->get();
		return $query->result_array();	
	}

	public function get_result_array_with_two_where_one_join($table,$field='',$value='',$field1='',$value1='',$join_field='',$j_table1='',$j_field1=''){
		$this->db->select('*')
				->from($table)
				->join($j_table1,''.$j_table1.'.'.$j_field1.'='.$table.'.'.$join_field.'');
		$this->db->where($field,$value);	
		$this->db->where($field1,$value1);	
		$query=$this->db->get();
		return $query->result_array();	
	}
	
	public function get_counts($table){
		$this->db->select('*')
				->from($table);
		$query=$this->db->get();
		return $query->num_rows();			 
		
	}	

	public function get_row_array_with_one_where($table,$field='',$id=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field,$id);	
		$query=$this->db->get();
		return $query->row_array();	
	}	
	

	public function get_row_array_with_two_where($table,$field='',$id='',$field1='',$id1=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field,$id);	
			$this->db->where($field1,$id1);	
		$query=$this->db->get();
		return $query->row_array();	
	}	

	public function get_row_array_with_three_where($table,$field1='',$id1='',$field2='',$id2='',$field3='',$id3=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field1,$id1);	
			$this->db->where($field2,$id2);	
			$this->db->where($field3,$id3);	
		$query=$this->db->get();
		return $query->row_array();	
	}	
 
	
	
	public function update_pwd($login_user,$old_pwd,$confirm_pwd){
		$this->db->select('l_pwd')
				->from('login')
				->where('l_pwd',$old_pwd)->where('l_id',$login_user);
		$query = $this->db->get();
		if($query->num_rows()==1){
			$this->db->where('l_pwd',$old_pwd)
					->where('l_id',$login_user)
					->update('login',array('l_pwd'=>$confirm_pwd));
			return $this->db->affected_rows();
		}
		else return '0';
	}





	public function get_m_prod_details($ml_p_id='',$ml_c_p_id='',$sub_m='',$ml_type=''){
		$this->db->select('*')
				->from('menus_list')
				->where('ml_active','1') 
				->order_by('ml_menu_position');	
		if($sub_m==''){
			$this->db->where('ml_parent_ml_id','0');							
		}
		if($sub_m=='1'){
			$this->db->where('ml_parent_ml_id',$ml_p_id);				
			$this->db->where('ml_child_p_ml_id','0');				
		}

		if($sub_m=='2'){
			$this->db->where('ml_parent_ml_id',$ml_p_id);				
			$this->db->where('ml_child_p_ml_id',$ml_c_p_id);				
		}
		if($ml_type==''){
			$this->db->where('ml_menu_type !=','3'); 
		}
		if($ml_type!=''){
			$this->db->where('ml_type ',$ml_type);  
		}

		$query=$this->db->get();
		return $query->result_array();	
	}

	public function get_menu_details($ml_p_id='',$ml_c_p_id='',$sub_m='',$ml_type=''){
		$this->db->select('*')
				->from('menus_list')
				->where('ml_active','1') 
				->order_by('ml_menu_position');	
		if($sub_m==''){
			$this->db->where('ml_parent_ml_id','0');							
		}
		if($sub_m=='1'){
			$this->db->where('ml_parent_ml_id',$ml_p_id);				
			$this->db->where('ml_child_p_ml_id','0');				
		}

		if($sub_m=='2'){
			$this->db->where('ml_parent_ml_id',$ml_p_id);				
			$this->db->where('ml_child_p_ml_id',$ml_c_p_id);				
		}
		if($ml_type==''){
			$this->db->where('ml_menu_type !=','3'); 
		}
		if($ml_type!=''){
			$this->db->where('ml_type ',$ml_type); 
			$this->db->where('ml_menu_type ','3'); 
		}

		$query=$this->db->get();
		return $query->result_array();	
	}


	 
	




 
	
	








	
	
	





	

	


	





}	

?>