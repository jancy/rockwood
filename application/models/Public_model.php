<?php



class Public_model extends CI_Model{	

	public function __construct(){
		//database loading
		$this->load->database();
	}	

	public function signin($user_name,$password){
		$this->db->select('*')
				->from('login')
				->where('i_type','1')
				->where('l_user',$user_name);
		$this->db->where('l_pwd',md5($password));
		$query=$this->db->get();
		if($query->num_rows()==1){
			return $query->row_array();
		}
		else return false;
	}
	
	
	public function get_result_array($table,$limit='',$start='',$field='',$id='',$order_field='',$order_type=''){
		$this->db->select('*')
				->from($table);
		if($id==''){
			if($order_field!='' && $order_type!=''){
				$this->db->order_by($order_field, $order_type);			
			}
			$this->db->limit($limit, $start);
			$query=$this->db->get();
			return $query->result_array();			
		}
		if($id!=''){
			$this->db->where($field,$id);
			$query=$this->db->get();
			return $query->row_array();			
		}
	} 

	public function get_counts($table,$field='',$id=''){
		$this->db->select('*')
				->from($table);
		if($id!=''){
			$this->db->where($field,$id);
		}
		$query=$this->db->get();
		return $query->num_rows();			
		
	}	
	
	
	public function insert_item($table,$data){	
		$this->db->insert($table,$data);		
		return $this->db->insert_id();
	}
	
	
	
	
	public function get_data_with_one_join($table,$field='',$id='',$join_tb='',$join_tb_field='',$tb_filed=''){
		$this->db->select('*')
				->from($table)				
				->join($join_tb,''.$join_tb.'.'.$join_tb_field.'='.$table.'.'.$tb_filed.'');
		if($id==''){
			$query=$this->db->get();
			return $query->result_array();			
		}
		if($id!=''){
			$this->db->where($field,$id);
			$query=$this->db->get();
			return $query->row_array();			
		}
	}
	
	
	
	public function get_data_with_two_join($table,$field='',$id='',$join_tb='',$join_tb_field='',$tb_filed='',$join_tb1='',$join_tb_field1='',$tb_filed1='',$limit='',$start=''){
		$this->db->select('*')
				->from($table)				
				->join($join_tb,''.$join_tb.'.'.$join_tb_field.'='.$table.'.'.$tb_filed.'')
				->join($join_tb1,''.$join_tb1.'.'.$join_tb_field1.'='.$table.'.'.$tb_filed1.'');
		if($limit!=''){
			$this->db->limit($limit, $start);		
		}
		
		
		if($id==''){
			$query=$this->db->get();
			return $query->result_array();			
		}
		if($id!=''){
			$this->db->where($field,$id);
			$query=$this->db->get();
			return $query->row_array();			
		}
	}
	 
	
	public function get_result_array_with_one_where($table,$field='',$id=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field,$id);	
		$query=$this->db->get();
		return $query->result_array();	
	}

	public function get_result_array_with_two_where($table,$field='',$val='',$field1='',$val1=''){
		$this->db->select('*')
				->from($table)
				->where($field,$val)	
				->where($field1,$val1);	
		$query=$this->db->get();
		return $query->result_array();	
	}

	public function get_result_array_with_one_where_and_limits($table,$limit='',$start='',$field='',$id=''){
		$this->db->select('*')
				->from($table)
				->where($field,$id)
				->limit($limit, $start);
		$query=$this->db->get();
		return $query->result_array();	
	}
	
	
	public function get_result_array_with_one_where_one_join($table,$field='',$id='',$join_field='',$j_table1='',$j_field1=''){
		$this->db->select('*')
				->from($table)
				->join($j_table1,''.$j_table1.'.'.$j_field1.'='.$table.'.'.$join_field.'');
		$this->db->where($field,$id);	
		$query=$this->db->get();
		return $query->result_array();	
	}
	
	public function get_row_array_with_one_where($table,$field='',$id=''){
		$this->db->select('*')
				->from($table);
			$this->db->where($field,$id);
		$query=$this->db->get();
		return $query->row_array();	
	}

	public function get_row_array_with_like($table,$field='',$id=''){
		$this->db->select('*')
				->from($table);
			// $this->db->where($field,$id);	
			$this->db->like($field,$id, 'after'); 	

		$query=$this->db->get();
		return $query->row_array();	
	}
	public function get_row_array_with_one_where_one_join($table,$field='',$id='',$join_field='',$j_table1='',$j_field1=''){
		$this->db->select('*')
				->from($table)
				->join($j_table1,''.$j_table1.'.'.$j_field1.'='.$table.'.'.$join_field.'');
		$this->db->where($field,$id);	
		$query=$this->db->get();
		return $query->row_array();	
	}
	
	
	public function get_result_array_with_two_join_and_where($table,$field='',$id='',$j_table1='',$j_field1='',$j_c_field1='',$j_table2='',$j_field2='',$j_c_field2='',$limit='',$start=''){
		$this->db->select('*')
				->from($table)
				->join($j_table1,''.$j_table1.'.'.$j_field1.'='.$table.'.'.$j_c_field1.'')
				->join($j_table2,''.$j_table2.'.'.$j_field2.'='.$table.'.'.$j_c_field2.'');
			$this->db->where($field,$id);
		if($limit!=''){
			$this->db->limit($limit, $start);		
		}
		$query=$this->db->get();
		return $query->result_array();	
	}


	
	public function update_data($table,$field,$id,$data){
		$this->db->where($field,$id)
				->update($table,$data);
		return $this->db->affected_rows();

	}
    
	function send_mail($from,$from_name,$to,$subject,$msg) {
		$config1 = array();
		$config1['protocol']    = 'smtp';
		$config1['smtp_host']    = 'ssl://smtp.gmail.com';
		$config1['smtp_port']    = '465';
		$config1['smtp_timeout'] = '7';
		$config1['smtp_user']    = 'jancycozak@gmail.com';
		$config1['smtp_pass']    = '7896@jancy';
		$config1['charset']    = 'utf-8';
		$config1['newline']    = "\r\n";
		$config1['mailtype'] = 'html'; // or html
		$config1['validation'] = TRUE; // bool whether to validate email or not      
		
		$this->email->initialize($config1);		
		
		$this->email->from($from, $from_name);
		$this->email->to($to);
		//
		$this->email->subject($subject);
		$this->email->message($msg);
		$this->email->send();
		return 'ok';
	}
	
	
	function timeago($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    

	public function get_menu_details($ml_p_id='',$ml_c_p_id='',$sub_m=''){
		$this->db->select('*')
				->from('menus_list')
				->where('ml_active','1') 
				->order_by('ml_menu_position');	
		if($sub_m==''){
			$this->db->where('ml_parent_ml_id','0');							
		}
		if($sub_m=='1'){
			$this->db->where('ml_parent_ml_id',$ml_p_id);				
			$this->db->where('ml_child_p_ml_id','0');				
		}

		if($sub_m=='2'){
			$this->db->where('ml_parent_ml_id',$ml_p_id);				
			$this->db->where('ml_child_p_ml_id',$ml_c_p_id);				
		}
		$this->db->where('ml_menu_type !=','3');				

		$query=$this->db->get();
		return $query->result_array();	
	}

	public function get_product_details($is_rent=''){
		$this->db->select('*')
				->from('menus_list')
				->where('ml_active','1') 
				->order_by('ml_menu_position');
				
		$this->db->where('ml_parent_ml_id !=','0');				
		$this->db->where('ml_child_p_ml_id !=','0');	 
		$this->db->where('ml_menu_type !=','3');				
		if($is_rent==1){
			$this->db->where('ml_is_rental','1');				

		}
		$query=$this->db->get();
		return $query->result_array();	
	}


	public function get_projects($type='',$ml_id=''){
		$this->db->select('*')
				->from('gallery_list')
				->join('gallery_title_list','gallery_title_list.gtl_id=gallery_list.gl_gtl_id')
				->join('menus_list','$menus_list.ml_id=gallery_title_list.gtl_ml_id')
				->where('ml_active','1') 
				->where('ml_type','project')  
				->where('ml_menu_type','3')
				->order_by('ml_id',"DESC");
		if($type=='1'){
			$this->db->group_by('gtl_id','DESC');
							
		} 

		$query=$this->db->get();
		return $query->result_array();	
	}

	public function get_blogs($type='',$ml_id=''){
		$this->db->select('*')
				->from('page_content_list') 
				->join('menus_list','$menus_list.ml_id=page_content_list.pcl_ml_id')
				->where('ml_active','1') 
				->where('ml_type','blog')  
				->order_by('ml_id',"DESC");  
		$query=$this->db->get();
		return $query->result_array();	
	}

	

	


	

	public function get_search_result($dsl_pincode='',$dsl_location=''){
		$this->db->select('*')
				->from('driving_school_list');
		if($dsl_pincode!=''){
			$this->db->where('dsl_pincode',$dsl_pincode);
		}
		if($dsl_location!=''){
			$this->db->like('dsl_location', $dsl_location);
		}
		$query=$this->db->get();
		return $query->result_array();			
		
	}





	





}	

?>