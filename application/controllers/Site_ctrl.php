<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_ctrl extends CI_Controller  {
 
	
	public function __construct(){ 
		parent::__construct();
		$data = array();
		$this->load->model('public_model');	
	}
	
	/*----------------- index -----------------*/ 
	public function index(){	   
		$ml_slug	= "home";	
		$data1['ml_slug_1'] = $ml_slug;
		$ml_data 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_slug',$ml_slug);
		// var_dump($ml_data);return;
		if(!is_array($ml_data)){
			redirect('site_ctrl/error_page');
		}else{
			
			$data['ml_data']	= $ml_data;
			$data1['ml_data']	= $ml_data;
			$data['ml_list'] 	= $this->public_model->get_menu_details();			 	
			$data['pro_list'] 	= $this->public_model->get_projects('1');
			$data['prod_list'] 	= $this->public_model->get_product_details();
			$data1['prod_list'] 	= $this->public_model->get_product_details();
			$data['bo_list'] 	= $this->public_model->get_blogs();
			$data1['prod_pcl_list'] 	= $this->public_model->get_result_array('page_content_list');	

			// echo $this->db->last_query();return;	

			// var_dump($ml_data);return;

			$data1['sl_list'] 	= $this->public_model->get_result_array_with_one_where('sliders_list','sl_ml_id',$ml_data['ml_id']);	
					 	
			$data1['pcl_list'] 	= $this->public_model->get_result_array_with_one_where('page_content_list','pcl_ml_id',$ml_data['ml_id']);


			$data1['gtl_list'] 	= $this->public_model->get_result_array_with_one_where('gallery_title_list','gtl_ml_id',$ml_data['ml_id']);	
					 	
			$data1['gl_list'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',$ml_data['ml_id']);

			$data1['vgl_list'] 	= $this->public_model->get_result_array_with_one_where('video_gallery_list','vgl_ml_id',$ml_data['ml_id']);			 	

			$data1['cut_say'] 	= $this->public_model->get_result_array_with_one_where('page_content_list','pcl_ml_id',"32");			 	
			$data1['client'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',"25");			 	


			$data1['pro_slider'] 	= $this->public_model->get_projects('1');	
			$data1['pro_gtl_list'] 	= $this->public_model->get_projects();	
			$data1['pro_cat_list'] 	= $this->public_model->get_result_array_with_two_where('menus_list','ml_type','project','ml_menu_type','3');	
			// echo $this->db->last_query();return;		 	
			// $data1['client'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',"25");	
 
			
			$data1['ml_parent_data'] 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_id',$ml_data['ml_parent_ml_id']);

			// echo $this->db->last_query();return;		 	
			// $data1['client'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',"25");			 	 
			$this->load->view('templates/header',$data); 
			
			if($ml_data['ml_parent_ml_id']==0){
				$this->load->view('templates/'.$ml_data['ml_type'].'',$data1);
			}
			else if($ml_data['ml_parent_ml_id']!=0 && $ml_data['ml_child_p_ml_id']==0){
				$this->load->view('templates/products_sub',$data1);
			}
			else if($ml_data['ml_parent_ml_id']!=0 && $ml_data['ml_child_p_ml_id']!=0){
				
				$data1['ml_child_data'] 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_id',$ml_data['ml_child_p_ml_id']);
				var_dump($data1['ml_child_data']);return;
				$data1['ml_child_list'] 	= $this->public_model->get_menu_details($ml_data['ml_parent_ml_id'],$ml_data['ml_child_p_ml_id'],'2');


				$this->load->view('templates/product_details',$data1);
			}
			$this->load->view('templates/footer',$data);
		}
	}


	/*----------------- load page -----------------*/
	public function page_loading(){	   
		// // echo $this->uri->segment(1);	
		// echo $this->uri->segment(2);	
		// echo $this->uri->segment(3);return;	
		$ml_slug	= $this->uri->segment(1);	
		if($this->uri->segment(2)!=''){
			$ml_slug	= $this->uri->segment(2);	
		}

		if($this->uri->segment(3)!=''){
			$ml_slug	= $this->uri->segment(3);	
		}
echo $ml_slug;
		$data1['ml_slug_1'] = $ml_slug;
		$ml_data 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_slug',$ml_slug);
		// var_dump($ml_data);return;
		if(!is_array($ml_data)){
			redirect('site_ctrl/error_page');
		}else{
			$data['ml_data']	= $ml_data;
			$data1['ml_data']	= $ml_data;
			$data['ml_list'] 	= $this->public_model->get_menu_details();			 	
			$data['pro_list'] 	= $this->public_model->get_projects('1');
			$data['prod_list'] 	= $this->public_model->get_product_details();
			$data1['prod_list'] 	= $this->public_model->get_product_details();
			$data1['rent_list'] 	= $this->public_model->get_product_details(1);
			// echo $this->db->last_query();return;	

			// var_dump($ml_data);return;
			$data['bo_list'] 	= $this->public_model->get_blogs();
			$data1['prod_pcl_list'] 	= $this->public_model->get_result_array('page_content_list');	

			// echo $this->db->last_query();return;	

			// var_dump($ml_data);return;

			$data1['sl_list'] 	= $this->public_model->get_result_array_with_one_where('sliders_list','sl_ml_id',$ml_data['ml_id']);	
					 	
			$data1['pcl_list'] 	= $this->public_model->get_result_array_with_one_where('page_content_list','pcl_ml_id',$ml_data['ml_id']);


			$data1['gtl_list'] 	= $this->public_model->get_result_array_with_one_where('gallery_title_list','gtl_ml_id',$ml_data['ml_id']);	
					 	
			$data1['gl_list'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',$ml_data['ml_id']);

			$data1['vgl_list'] 	= $this->public_model->get_result_array_with_one_where('video_gallery_list','vgl_ml_id',$ml_data['ml_id']);			 	

			$data1['cut_say'] 	= $this->public_model->get_result_array_with_one_where('page_content_list','pcl_ml_id',"32");			 	
			$data1['client'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',"25");			 	


			$data1['pro_slider'] 	= $this->public_model->get_projects('1');	
			$data1['pro_gtl_list'] 	= $this->public_model->get_projects();	
			$data1['pro_cat_list'] 	= $this->public_model->get_result_array_with_two_where('menus_list','ml_type','project','ml_menu_type','3');	
			// echo $this->db->last_query();return;		 	
			// $data1['client'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_ml_id',"25");	

			$this->load->view('templates/header',$data); 
			
			$data1['ml_parent_data'] 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_id',$ml_data['ml_parent_ml_id']);


			if($ml_data['ml_parent_ml_id']==0){
				$this->load->view('templates/'.$ml_data['ml_type'].'',$data1);
			}
			else if($ml_data['ml_parent_ml_id']!=0 && $ml_data['ml_child_p_ml_id']==0){
				$this->load->view('templates/products_sub',$data1);
			}
			else if($ml_data['ml_parent_ml_id']!=0 && $ml_data['ml_child_p_ml_id']!=0){
				
				$data1['ml_child_data'] 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_id',$ml_data['ml_child_p_ml_id']);
				// var_dump($data1['ml_child_data']);return;
				$data1['ml_child_list'] 	= $this->public_model->get_menu_details($ml_data['ml_parent_ml_id'],$ml_data['ml_child_p_ml_id'],'2');


				$this->load->view('templates/product_details',$data1);
			}
			$this->load->view('templates/footer',$data);
		}
	}

	/*----------------- load page deails -----------------*/
	public function page_details(){	  
		$ml_slug	= $this->uri->segment(2);	
		$ml_slug = str_replace('-',' ',$ml_slug);
		$data['ml_list'] 	= $this->public_model->get_menu_details();
		$data['pro_list'] 	= $this->public_model->get_projects('1');
		$data['prod_list'] 	= $this->public_model->get_product_details();


		$data1['gtl_data'] 	= $this->public_model->get_row_array_with_one_where('gallery_title_list','gtl_title',$ml_slug);	
				// echo $this->db->last_query();return; 	
					 	
		$data1['gl_list'] 	= $this->public_model->get_result_array_with_one_where('gallery_list','gl_gtl_id',$data1['gtl_data']['gtl_id']);

		$data1['ml_data'] 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_id',$data1['gtl_data']['gtl_ml_id']);
				// echo $this->db->last_query();return; 	

		$data1['pro_slider'] 	= $this->public_model->get_projects('1',$data1['ml_data']['ml_id']);	

					 	 
			$this->load->view('templates/header',$data);
			$this->load->view('templates/project_detail',$data1);
			$this->load->view('templates/footer',$data);
		
	}

	/*----------------- load post deails -----------------*/
	public function post_details(){	  
		$ml_slug	= $this->uri->segment(2);	
		$ml_slug = str_replace('-',' ',$ml_slug);

		$data['ml_list'] 	= $this->public_model->get_menu_details();
		$data['pro_list'] 	= $this->public_model->get_projects('1');
		$data['prod_list'] 	= $this->public_model->get_product_details();

		$data1['pcl_data'] 	= $this->public_model->get_row_array_with_like('page_content_list','pcl_title',$ml_slug);	
				// echo $this->db->last_query();return; 	
					 	
		
		$data1['ml_data'] 	= $this->public_model->get_row_array_with_one_where('menus_list','ml_id',$data1['pcl_data']['pcl_ml_id']);
		// echo $this->db->last_query();return; 	
		// var_dump($data1['ml_data']);return; 	
		$data1['sl_list'] 	= $this->public_model->get_result_array_with_one_where('sliders_list','sl_ml_id',$data1['pcl_data']['pcl_ml_id']);	

		$data1['pcl_list'] 	= $this->public_model->get_result_array_with_one_where('page_content_list','pcl_ml_id',$data1['pcl_data']['pcl_ml_id']);
		
					 	 
			$this->load->view('templates/header',$data);
			$this->load->view('templates/post_details',$data1);
			$this->load->view('templates/footer',$data);
		
	}


	public function error_page(){ 
		$data['ml_list'] 	= $this->public_model->get_menu_details();			 	
		$data['pro_list'] 	= $this->public_model->get_projects('1');
		$data['prod_list'] 	= $this->public_model->get_product_details();

		$this->load->view('templates/header',$data); 
		$this->load->view('templates/404-page');
		$this->load->view('templates/footer',$data);   
	}
	
 
	 
}
