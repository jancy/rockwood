<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page_ctrl extends admin_index {
 
	
	public function __construct(){ 
		parent::__construct();
		$data = array();
		$this->load->model('admin_model');	
	}
	
	// /*----------------- load page view page -----------------*/
	public function create_page1(){	
		$data['sdata'] 				= array();
		$data['subview'] 			= 'pages/create_page';
		$data['active_cls'] 		= 'pae_cls'; 		
		$data['view_name'] 			= "Create Page"; 		 
		$data['sdata']['ml_list'] 	= $this->admin_model->get_result_array('menus_list');			 	
		 
		$this->load->view('common/sidepanel',$data);
	}
	
	
	/*----------------- load page view page -----------------*/
	public function create_page($ml_id,$edit_id=''){	
		$ml_data 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$ml_id);
		// var_dump($ml_data['ml_type']);return;
		$data['sdata'] 				= array();
		$data['subview'] 			= 'a_templates/'.$ml_data['ml_type'].'';
		$data['active_cls'] 		= 'pae_cls'; 		
		$data['view_name'] 			= $ml_data['ml_type']; 		 
		$data['ml_list'] 			= $this->admin_model->get_menu_details();		
		$data['ml_cli_list'] 		= $this->admin_model->get_menu_details('','','','clients');	
		$data['ml_cus_list'] 		= $this->admin_model->get_menu_details('','','','customer-says');	

		$data['sdata']['ml_id'] 	= $ml_id;			 	
		if($ml_data['ml_type']=="home"){
			$data['sdata']['sl_list'] 	= $this->admin_model->get_result_array_with_two_where('sliders_list','sl_ml_id',$ml_id,'sl_active','1'); 
			if($edit_id!=''){
				$data['sdata']['sl_data'] = $this->admin_model->get_row_array_with_one_where('sliders_list','sl_id',$edit_id);

			}
		}
		if($ml_data['ml_type']=="about-us"){
			$data['sdata']['our_story'] 	= $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_id','1'); 	

			$data['sdata']['our_value'] 	= $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_id','2'); 
			
		}
		if($ml_data['ml_type']=="rentals"){
			$data['sdata']['re_data'] 	= $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_ml_id',$ml_id);  
		}
		if($ml_data['ml_type']=="services"){
			$data['sdata']['se_data'] 	= $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_ml_id',$ml_id);  
		}
		if($ml_data['ml_type']=="blog"){
			$data['sdata']['bl_list'] 	= $this->admin_model->get_result_array_with_two_where('page_content_list','pcl_ml_id',$ml_id,'pcl_active','1');	 
			if($edit_id!=''){
				$data['sdata']['bl_data'] = $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_id',$edit_id);
				// echo $this->db->last_query();		return;	
			}
		}
		if($ml_data['ml_type']=="project"){
			$data['sdata']['ml_list'] 			= $this->admin_model->get_menu_details('','','','project');		
				 
			if($edit_id!=''){
				$data['sdata']['ml_data'] = $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$edit_id);
				// echo $this->db->last_query();		return;	
			}
		}
		if($ml_data['ml_type']=="products"){
			$data['sdata']['ml_list'] 			= $this->admin_model->get_m_prod_details($ml_id,'','1','productsub');		
				// echo $this->db->last_query();		return;	
				 
			if($edit_id!=''){
				$data['sdata']['ml_data'] 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$edit_id);

				$data['sdata']['prod_data'] = $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_ml_id',$edit_id);
			}
		}
		if($ml_data['ml_type']=="client"){
			$data['sdata']['gtl_data'] = $this->admin_model->get_row_array_with_one_where('gallery_title_list','gtl_ml_id',$ml_id);			 	
			$data['sdata']['gl_data'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gtl_ml_id',$ml_id);
			var_dump($data['sdata']['gtl_data']);return;
		}
		if($ml_data['ml_type']=="customer-says"){
			$data['sdata']['cus_list'] 	= $this->admin_model->get_result_array_with_two_where('page_content_list','pcl_ml_id',$ml_id,'pcl_active','1');
				// echo $this->db->last_query();		return;	

			if($edit_id!=''){
				$data['sdata']['cus_data'] = $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_id',$edit_id);
				// echo $this->db->last_query();		return;	
			}
		}
		$this->load->view('common/sidepanel',$data);
	}

	/*----------------- load page view page -----------------*/
	public function add_projects($ml_id,$pro_cate_id,$gtl_id=''){	
		$ml_data 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$pro_cate_id);
		// var_dump($ml_data['ml_type']);return;
		$data['sdata'] 				= array();
		$data['subview'] 			= 'a_templates/project_caegory';
		$data['active_cls'] 		= 'pae_cls'; 		
		$data['page_name'] 			= $ml_data['ml_name']; 		 
		$data['view_name'] 			= 'Project/'.$ml_data['ml_name']; 		 
		$data['ml_list'] 			= $this->admin_model->get_menu_details();		
		$data['ml_cli_list'] 		= $this->admin_model->get_menu_details('','','','clients');	
		$data['ml_cus_list'] 		= $this->admin_model->get_menu_details('','','','customer-says');	

		$data['sdata']['ml_id'] 		= $ml_id;			 			 
		$data['sdata']['pro_cate_id'] 	= $pro_cate_id;			 			 
		
		$data['sdata']['gtl_list'] = $this->admin_model->get_result_array_with_two_where('gallery_title_list','gtl_ml_id',$pro_cate_id,'gtl_active','1');			 	
		$data['sdata']['gl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gl_ml_id',$pro_cate_id);	
		
		if($gtl_id!=''){
			$data['sdata']['gtl_data'] = $this->admin_model->get_row_array_with_one_where('gallery_title_list','gtl_id',$gtl_id);			 	
			$data['sdata']['gl_data'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gl_gtl_id',$gtl_id);
		}
		
		$this->load->view('common/sidepanel',$data);
	}

	/*----------------- load page view page -----------------*/
	public function add_sub_product($ml_id,$sub_cate_id,$sub_pro_id=''){	
		$ml_data 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$sub_cate_id);
		// var_dump($ml_data['ml_type']);return;
		$data['sdata'] 				= array();
		$data['subview'] 			= 'a_templates/products_sub';
		$data['active_cls'] 		= 'pae_cls'; 		
		$data['page_name'] 			= $ml_data['ml_name']; 		 
		$data['view_name'] 			= 'Product/'.$ml_data['ml_name']; 		 
		$data['ml_list'] 			= $this->admin_model->get_menu_details();		
		$data['ml_cli_list'] 		= $this->admin_model->get_menu_details('','','','clients');	
		$data['ml_cus_list'] 		= $this->admin_model->get_menu_details('','','','customer-says');	

		$data['sdata']['ml_id'] 		= $ml_id;			 			 
		$data['sdata']['sub_cate_id'] 	= $sub_cate_id;	
		$data['sdata']['sub_pro_id'] 	= $sub_pro_id;	

		$data['sdata']['ml_list'] 		= $this->admin_model->get_m_prod_details($ml_id,$sub_cate_id,'2','productdetails');		
		
		// echo $this->db->last_query();			
		// var_dump($data['ml_list']);return;

		$data['sdata']['gtl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_title_list','gtl_ml_id',$sub_cate_id);			 	
		$data['sdata']['gl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gl_ml_id',$sub_cate_id);	
		
		if($sub_pro_id!=''){
			$data['sdata']['ml_data'] 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$sub_pro_id);

			$data['sdata']['sub_data']['sl_list'] 	= $this->admin_model->get_result_array_with_one_where('sliders_list','sl_ml_id',$sub_pro_id); 

			$data['sdata']['sub_data']['pro_p_data'] 	= $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_ml_id',$sub_pro_id);	
			
			$data['sdata']['sub_data']['pro_gtl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_title_list','gtl_ml_id',$sub_pro_id);	

			$data['sdata']['sub_data']['gl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gl_ml_id',$sub_pro_id);

			$data['sdata']['sub_data']['vl_data'] = $this->admin_model->get_result_array_with_one_where('video_gallery_list','vgl_ml_id',$sub_pro_id);
			$data['sdata']['sub_data']['ml_id'] 		= $ml_id;			 			 
			$data['sdata']['sub_data']['sub_cate_id'] 	= $sub_cate_id;	
			$data['sdata']['sub_data']['sub_pro_id'] 	= $sub_pro_id;	


		}
		
		$this->load->view('common/sidepanel',$data);
	}


 
	 
	public function get_menu_data(){	
		$ml_id 	= $this->input->post('ml_id');
		$data['j_data']['ml_id'] 	= $ml_id;		
		$data['j_data']['ml_data'] 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$ml_id);

		$data['j_data']['sl_list'] 	= $this->admin_model->get_result_array_with_one_where('sliders_list','sl_ml_id',$ml_id);			 	
		$data['j_data']['pcl_list'] = $this->admin_model->get_result_array_with_one_where('page_content_list','pcl_ml_id',$ml_id);			 	
		$data['j_data']['gtl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_title_list','gtl_ml_id',$ml_id);			 	
		$data['j_data']['gl_list'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gl_ml_id',$ml_id);			 	
		$data['j_data']['vgl_list'] = $this->admin_model->get_result_array_with_one_where('video_gallery_list','vgl_ml_id',$ml_id);			 	
		
		echo json_encode($data['j_data']);
	}

	public function get_menu_data_by_id(){	
			// var_dump($pcl_id);  
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$data['j_data'] = array();
		if($type=='pcl_edit'){			
			$data['j_data']['pcl_data'] = $this->admin_model->get_row_array_with_one_where('page_content_list','pcl_id',$id);
			// echo  $this->db->last_query();			 				
		}
		
		if($type=='gtl_edit'){			
			$data['j_data']['gtl_data'] = $this->admin_model->get_row_array_with_one_where('gallery_title_list','gtl_id',$id);
			$data['j_data']['gl_data'] = $this->admin_model->get_result_array_with_one_where('gallery_list','gl_gtl_id',$id);
			// echo  $this->db->last_query();			 				
		}
		echo json_encode($data['j_data']);
	}



	public function delete_page_details(){	 
		$id = $this->input->post('id');
		$type = $this->input->post('type');
		$data['j_data'] = array();
		if($type=='slider_tr_del'){	 		
			$this->db->where('sl_id',$id)  
			// ->delete("menus_list");
				->update('sliders_list', array('sl_active' => '0'));
			$data['j_data']['del_status'] = '1';	 
		}		
		if($type=='pcl_tr_del'){ 					
			$this->db->where('pcl_id',$id) 
			->update('page_content_list', array('pcl_active' => '0'));
			$data['j_data']['del_status'] = '1';	 
		}	
		if($type=='vgl_tr_del'){		 			
			$this->db->where('vgl_id',$id)
				->update('video_gallery_list', array('vgl_active' => '0')); 
			$data['j_data']['del_status'] = '1';	 
		}
		if($type=='img_delete'){		 			
			$this->db->where('gl_id',$id)->delete("gallery_list");
			$data['j_data']['del_status'] = '1';	 
		}	
		
		if($type=='gtl_id_del'){		 			
			$this->db->where('gtl_id',$id)->delete("gallery_title_list");
			$this->db->where('gl_gtl_id',$id)->delete("gallery_list");
			$data['j_data']['del_status'] = '1';	 
		}
		if($type=='ml_tr_del'){		 			
			$this->db->where('ml_id',$id) 
				->update('menus_list', array('ml_active' => '0'));
			$data['j_data']['del_status'] = '1';	 
		}	
		echo json_encode($data['j_data']);
	}
	 
	/*----------------- add menu details  details and edit menu details  details -----------------*/
	public function add_menu_details($parent_id,$ml_id=false,$child_id=false){	
		// var_dump($_POST);return;	 
		
		$this->form_validation->set_rules('ml_name', 'Name', 'trim|required'); 
		$this->form_validation->set_rules('ml_slug', 'Slug', 'trim|required'); 
		$this->form_validation->set_rules('ml_title', 'Title', 'trim|required'); 
		$this->form_validation->set_rules('ml_desc', 'Description', 'trim|required'); 
		$this->form_validation->set_rules('ml_keyword', 'Keyword', 'trim|required');  
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('ml_name');      
			$e_msg .= form_error('ml_slug');      
			$e_msg .= form_error('ml_title');      
			$e_msg .= form_error('ml_desc');      
			$e_msg .= form_error('ml_keyword');       
			$this->session->set_flashdata('tos_error', $e_msg);
			if($child_id!=''){
				redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$child_id.'');	
			}
			else{
				redirect('page_ctrl/create_page/'.$parent_id.'/'.$child_id.'');	
			}
		}	
		$data['ml_details'] = array();
		foreach($_POST as $post_key=>$post_rows){
			if($post_key=='ml_slug'){
				$string = str_replace(' ', '-', $this->input->post($post_key)); 
   				$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
				$string = strtolower($string);
				$data['ml_details'][$post_key] 	= $string;
				
			}else{
				$data['ml_details'][$post_key] 	= $this->input->post($post_key);
			}
		}  
		if($ml_id==false && $ml_id==0){   
										
			$add_ml 	= $this->admin_model->insert_item('menus_list',$data['ml_details']);  
			if($add_ml!=false){
				$this->session->set_flashdata('tos_success', 'Menu details added.');
				if($child_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$child_id.'');	
				}
				else{
					redirect('page_ctrl/create_page/'.$parent_id.'/'.$child_id.'');	
				}
			}else {
				$this->session->set_flashdata('tos_error', 'Try again !...');
				if($child_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$child_id.'');	
				}
				else{
					redirect('page_ctrl/create_page/'.$parent_id.'/'.$child_id.'');	
				}
			}	
		}		
		else{
			$edit_ml = $this->admin_model->update_data('menus_list','ml_id',$ml_id,$data['ml_details']);
			if($edit_ml=='1'){
				$this->session->set_flashdata('tos_success', 'Menu details updated');
				if($child_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$child_id.'');	
				}
				else{
					redirect('page_ctrl/create_page/'.$parent_id.'/'.$child_id.'');	
				}
			}else {
				$this->session->set_flashdata('tos_warning', 'There is no changes!...');
				if($child_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$child_id.'');	
				}
				else{
					redirect('page_ctrl/create_page/'.$parent_id.'/'.$child_id.'');	
				}
			}
		} 			
	}



	/*----------------- add slider details  details and edit slider details  details -----------------*/
	public function add_slider_details($sl_id=false){	
		// var_dump($_POST);return;	 
		
		$this->form_validation->set_rules('sl_ml_id', 'Menu', 'trim|required'); 
		$this->form_validation->set_rules('sl_img', 'Image', 'trim|required');   
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('sl_ml_id');      
			$e_msg .= form_error('sl_img');             
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			redirect('page_ctrl/create_page/'.$_POST['sl_ml_id'].'');	
		}	
		$data['sl_details'] = array();
		foreach($_POST as $post_key=>$post_rows){
			if(($post_key!='filename') && ($post_key!='files')){
				// echo $post_key."<br>";

				$data['sl_details'][$post_key] 	= $this->input->post($post_key);
			}	
		}  

		// var_dump($data['sl_details']);return;	 
		if($sl_id==false){    

			$add_sl 	= $this->admin_model->insert_item('sliders_list',$data['sl_details']);  
			if($add_sl!=false){
				$this->session->set_flashdata('tos_success', 'Menu slider added.');
				redirect('page_ctrl/create_page/'.$_POST['sl_ml_id'].'');	
			}else {
				$this->session->set_flashdata('tos_error', 'Try again !...');
				redirect('page_ctrl/create_page/'.$_POST['sl_ml_id'].'');	
			}	
		}else{
			$edit_sl = $this->admin_model->update_data('sliders_list','sl_id',$sl_id,$data['sl_details']);
			if($edit_sl=='1'){
				$this->session->set_flashdata('tos_success', ' Slider details updated');
				redirect('page_ctrl/create_page/'.$_POST['sl_ml_id'].'');
			}else {
				$this->session->set_flashdata('tos_warning', 'There is no changes!...');
				redirect('page_ctrl/create_page/'.$_POST['sl_ml_id'].'');
			}
		} 
			 			
	}

	
	/*----------------- add content details  details and edit content details  details -----------------*/
	public function add_content_details($pcl_id=false,$parent_id='',$sub_parent_id=''){	
		// var_dump($_POST);return;
		$this->form_validation->set_rules('pcl_ml_id', 'Menu', 'trim|required'); 
		$this->form_validation->set_rules('pcl_title', 'Title', 'trim|required'); 
		$this->form_validation->set_rules('pcl_content', 'Content', 'trim|required');   
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('pcl_ml_id');      
			$e_msg .= form_error('pcl_title');      
			$e_msg .= form_error('pcl_content');             
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			if($parent_id!=''){
				redirect('page_ctrl/create_page/'.$parent_id.'');	
				
			}else if($sub_parent_id!=''){
				redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$_POST['pcl_ml_id']);	
				
			}else{
				redirect('page_ctrl/create_page/'.$_POST['pcl_ml_id'].'');	
			}
		}	

		// var_dump($_POST);return;	 

		
		$data['pcl_details'] = array();
		foreach($_POST as $post_key=>$post_rows){
			if($post_key!='filename' && $post_key!='files'){
				$data['pcl_details'][$post_key] 	= $this->input->post($post_key);
			}	
		}
		if($pcl_id==false){    
			$add_pcl 	= $this->admin_model->insert_item('page_content_list',$data['pcl_details']);  
			if($add_pcl!=false){
				$this->session->set_flashdata('tos_success', 'content added.');
				if($parent_id!=''){
					redirect('page_ctrl/create_page/'.$parent_id.'');	
					
				}else if($sub_parent_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$_POST['pcl_ml_id']);	
					
				}else{
					redirect('page_ctrl/create_page/'.$_POST['pcl_ml_id'].'');	
				}
			}else {
				$this->session->set_flashdata('tos_error', 'Try again !...');
				if($parent_id!=''){
					redirect('page_ctrl/create_page/'.$parent_id.'');	
					
				}else if($sub_parent_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$_POST['pcl_ml_id']);	
					
				}else{
					redirect('page_ctrl/create_page/'.$_POST['pcl_ml_id'].'');	
				}
			}
		}else{
			$edit_pcl = $this->admin_model->update_data('page_content_list','pcl_id',$pcl_id,$data['pcl_details']);
			if($edit_pcl=='1'){
				$this->session->set_flashdata('tos_success', ' Content details updated');
				if($parent_id!=''){
					redirect('page_ctrl/create_page/'.$parent_id.'');	
					
				}
				else if($sub_parent_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$_POST['pcl_ml_id']);	
					
				}else{
					redirect('page_ctrl/create_page/'.$_POST['pcl_ml_id'].'');	
				}
			}else {
				$this->session->set_flashdata('tos_warning', 'There is no changes!...');
				if($parent_id!=''){
					redirect('page_ctrl/create_page/'.$parent_id.'');	
					
				}
				else if($sub_parent_id!=''){
					redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$_POST['pcl_ml_id']);	
					
				}else{
					redirect('page_ctrl/create_page/'.$_POST['pcl_ml_id'].'');	
				}
			}
		} 	
			 			
	}

	/*----------------- add gallery details  details and edit gallery details  details -----------------*/
	public function add_gallery_details($ml_id,$pro_cate_id,$gtl_id=false){	
		// var_dump(sizeOf($_POST['gl_img']));  
		$this->form_validation->set_rules('gtl_ml_id', 'Menu', 'trim|required'); 
		$this->form_validation->set_rules('gtl_title', 'Title', 'trim|required');    
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('gtl_ml_id');      
			$e_msg .= form_error('gtl_title');                 
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			redirect('page_ctrl/add_projects/'.$ml_id.'/'.$pro_cate_id);	
		}	
		if(sizeOf($_POST['gl_img'])==0){
			$e_msg = "Images not uploaded";                    
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			redirect('page_ctrl/add_projects/'.$ml_id.'/'.$pro_cate_id);	
		}
 	 
		// var_dump($_POST); return;  

		
		$data['gtl_details'] = array();
		$data['gtl_details']['gtl_ml_id'] 		= $this->input->post('gtl_ml_id');
		$data['gtl_details']['gtl_title'] 		= $this->input->post('gtl_title');
		$data['gtl_details']['gtl_i_img'] 		= $this->input->post('gtl_i_img');
		$data['gtl_details']['gtl_desc'] 		= $this->input->post('gtl_desc');
		$data['gtl_details']['gtl_content'] 	= $this->input->post('gtl_content');
		$data['gtl_details']['gtl_content2'] 	= $this->input->post('gtl_content2');
		$data['gl_id'] 							= $this->input->post('gl_id');
		if($gtl_id==false){
			$add_gtl 	= $this->admin_model->insert_item('gallery_title_list',$data['gtl_details']);  
		}else{
			$edit_gtl = $this->admin_model->update_data('gallery_title_list','gtl_id',$gtl_id,$data['gtl_details']);			
		}
		$a_gl = 0; $e_gl = 0;
		foreach($_POST['gl_img'] as $post_key=>$post_gl_rows){
			$data['gl_details']['gl_ml_id'] 	= $this->input->post('gtl_ml_id'); 
			$data['gl_details']['gl_img'] 		= $post_gl_rows;
			if($gtl_id==false){
				$data['gl_details']['gl_gtl_id'] 	= $add_gtl;
			}else{
				$data['gl_details']['gl_gtl_id'] 	= $gtl_id;		
			}

			if($data['gl_id'][$post_key]==0){
				$add_gl 	= $this->admin_model->insert_item('gallery_list',$data['gl_details']);
				$a_gl = 1;  
			}else{
				$edit_gl = $this->admin_model->update_data('gallery_list','gl_id',$data['gl_id'][$post_key],$data['gl_details']);
				$e_gl = 1; 
			} 


		}   

		if($a_gl!=0 || $e_gl!=0){
			$this->session->set_flashdata('tos_success', 'Page gallery details saved.');
			redirect('page_ctrl/add_projects/'.$ml_id.'/'.$pro_cate_id);	
		} else {
			$this->session->set_flashdata('tos_error', 'Try again !...');
			redirect('page_ctrl/add_projects/'.$ml_id.'/'.$pro_cate_id);	
		}	
			 			
	}

	/*----------------- add product image details  details and edit product image details  details -----------------*/
	public function add_p_img_details($parent_id,$sub_parent_id,$product_id,$gtl_id=false){	
		// var_dump(sizeOf($_POST['gl_img']));  
		$this->form_validation->set_rules('gtl_ml_id', 'Menu', 'trim|required'); 
		$this->form_validation->set_rules('gtl_title', 'Title', 'trim|required');    
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('gtl_ml_id');      
			$e_msg .= form_error('gtl_title');                 
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	
		}	
		if(sizeOf($_POST['gl_img'])==0){
			$e_msg = "Images not uploaded";                    
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	
		}
 	 
		// var_dump($_POST); return;  

		
		$data['gtl_details'] = array();
		$data['gtl_details']['gtl_ml_id'] 		= $this->input->post('gtl_ml_id');
		$data['gtl_details']['gtl_title'] 		= $this->input->post('gtl_title');
		$data['gtl_details']['gtl_i_img'] 		= $this->input->post('gtl_i_img');
		$data['gtl_details']['gtl_desc'] 		= $this->input->post('gtl_desc');
		$data['gtl_details']['gtl_content'] 	= $this->input->post('gtl_content');
		$data['gtl_details']['gtl_content2'] 	= $this->input->post('gtl_content2');
		$data['gl_id'] 							= $this->input->post('gl_id');
		if($gtl_id==false){
			$add_gtl 	= $this->admin_model->insert_item('gallery_title_list',$data['gtl_details']);  
		}else{
			$edit_gtl = $this->admin_model->update_data('gallery_title_list','gtl_id',$gtl_id,$data['gtl_details']);			
		}
		$a_gl = 0; $e_gl = 0;
		foreach($_POST['gl_img'] as $post_key=>$post_gl_rows){
			$data['gl_details']['gl_ml_id'] 	= $this->input->post('gtl_ml_id'); 
			$data['gl_details']['gl_img'] 		= $post_gl_rows;
			if($gtl_id==false){
				$data['gl_details']['gl_gtl_id'] 	= $add_gtl;
			}else{
				$data['gl_details']['gl_gtl_id'] 	= $gtl_id;		
			}

			if($data['gl_id'][$post_key]==0){
				$add_gl 	= $this->admin_model->insert_item('gallery_list',$data['gl_details']);
				$a_gl = 1;  
			}else{
				$edit_gl = $this->admin_model->update_data('gallery_list','gl_id',$data['gl_id'][$post_key],$data['gl_details']);
				$e_gl = 1; 
			} 


		}   

		if($a_gl!=0 || $e_gl!=0){
			$this->session->set_flashdata('tos_success', 'Page gallery details saved.');
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	
		} else {
			$this->session->set_flashdata('tos_error', 'Try again !...');
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	
		}	
			 			
	}

	/*----------------- add video details  details and edit video details  details -----------------*/
	public function add_video_url_details($parent_id,$sub_parent_id,$product_id){	
		
		$this->form_validation->set_rules('vgl_ml_id', 'Menu', 'trim|required'); 
		$this->form_validation->set_rules('vgl_url', 'URL', 'trim|required');  
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('vgl_ml_id');      
			$e_msg .= form_error('vgl_url');                   
			$result = $this->session->set_flashdata('tos_error', $e_msg);
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	
		}	

		// var_dump($_POST);return;	 

		
		$data['vgl_details'] = array();
		foreach($_POST as $post_key=>$post_rows){
			if($post_key!='files'){
				$data['vgl_details'][$post_key] 	= $this->input->post($post_key);
			}	
		}  
		$add_vgl 	= $this->admin_model->insert_item('video_gallery_list',$data['vgl_details']);  
		if($add_vgl!=false){
			$this->session->set_flashdata('tos_success', 'Page video url added.');
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	

		}else {
			$this->session->set_flashdata('tos_error', 'Try again !...');
			redirect('page_ctrl/add_sub_product/'.$parent_id.'/'.$sub_parent_id.'/'.$product_id);	
 	
		}	
			 			
	}
 
}
