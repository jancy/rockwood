<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_login_ctrl extends admin_index {
 
	
	public function __construct(){
		parent::__construct(); 
		$data = array();
		$this->load->model('admin_model');
	}
	
	/*----------------- load dashboard -----------------*/
	public function index($d_type=''){	
		// var_dump($d_type);return;
		$data['sdata'] 			= array();
		$data['subview'] 		= 'dashboard/ad_dashboard';
		$data['active_cls'] 	= 'dash_cls';	
		$data['view_name'] 		= 'Dashboard'; 
		$data['ml_list'] 		= $this->admin_model->get_menu_details();		
		$data['ml_cli_list'] 	= $this->admin_model->get_menu_details('','','','clients');	
		$data['ml_cus_list'] 	= $this->admin_model->get_menu_details('','','','customer-says');			 	

// 		echo $this->db->last_query();			
// var_dump($data['ml_list']);return;		 	
		$data['sdata']['ml_count'] 	= $this->admin_model->get_counts('menus_list');							
		$this->load->view('common/sidepanel',$data); 
	}

	 

	/*----------------- load login page -----------------*/
	public function login(){
		$this->load->view('common/ad_login');
	}
	
	/*----------------- login submition -----------------*/
	
	public function submit(){
		$return = array('status'=>false);
		if(isset($_POST["u_name"])){
			 $user_name 			= $this->input->post("u_name");
			 $password 				= $this->input->post("pwd");
			 $l_data 				= $this->admin_model->signin($user_name,$password);
			 $l_data['panel_type']  = '0';
			// var_dump($l_data);//return;
			// echo $this->db->last_query(); return false;
			 if($l_data!=false){ 
				$this->session->set_userdata('login_state',$l_data );
				redirect('admin_login_ctrl/index');
				//return;
			}
			else redirect("admin");
		}
		else redirect("admin");
	}
	
	
	
 
	

	
	
	
	
	
	public function change_password(){	
		$data['sdata'] = array();
		$data['subview'] = 'change_password';
		$this->load->view('admin/sidepanel',$data);
	}
	
	
	public function reset_password(){

		$login_user = $this->user['l_id'];
		$old_pwd = md5($this->input->post('old_pwd'));
		$new_pwd = md5($this->input->post('new_pwd'));
		$confirm_pwd = md5($this->input->post('confirm_pwd'));
		if(isset($login_user)){
			if($new_pwd==$confirm_pwd){
				$u_pwd = $this->login_model->update_pwd($login_user,$old_pwd,$confirm_pwd);
				if($u_pwd==1){
					$this->session->set_flashdata('tos_success', 'Successfully Reseted...');
					redirect('admin_ctrl/change_password');
				}else {
					$this->session->set_flashdata('tos_error', 'Try again !...');
					redirect('admin_ctrl/change_password');
				}
			}
			else {
				$this->session->set_flashdata('tos_error', 'New password and confirm password are mismatch');
				redirect('admin_ctrl/change_password');
			}
		}else {
			$this->session->set_flashdata('tos_error', 'Invalid user');
			redirect('admin_ctrl/change_password');
		}
	}	
	
	public function logout(){		
		$redirect_url = 'admin_login_ctrl/login';		
		$this->session->sess_destroy();
		redirect(''.$redirect_url.'');
	}
	
	
	
	
}
