<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Img_upload_ctrl extends user_index {
	
	public function __construct(){
		parent::__construct();
		$data = array();
	}	 

	public function upload_slider_image(){
		$image_info = getimagesize($_FILES["filename"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];



		$return = array('status'=>false);
		$return['i'] =$this->input->post('i');
		$file_name = 'slider_'.base_convert(time(),10,36);
		$filename = $this->input->post('filename');	
		$filename = $this->input->post('filename');	
		$config['upload_path'] = 'public_html/upload/slider/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
		$config['max_size']	= '1000000';
		$config['max_width']  = '102400';
		$config['max_height']  = '76800';
		$config['file_name'] = $file_name.'.jpg';
		$config['overwrite'] = false;
		// if($image_width=='1200' && $image_height=='800'){
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('filename')) {
				$return['error']= $this->upload->display_errors();
				echo json_encode($return);
			}else{
				$data = $this->upload->data();
				$return['file']= $data['file_name'].'?t='.base_convert(time(),10,36);
				$config = array();
				//$config['image_library'] = 'gd2';
				$config['source_image']	= "public_html/upload/slider/".$data['file_name'];
				$config['new_image']	= "public_html/upload/slider/thumb/".$data['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = false;
				$config['width']	= 120;
				$config['height']	= 120;	
				$this->load->library('image_lib', $config); 
				$this->image_lib->resize();	
				$return['status']= true;	
				$return['e_type']= '2';	
				echo json_encode($return);
			}
		// }else{
		// 	$return['error']= 'maximum image size is 1200*800 ..';
		// 	$return['e_type']= '1';
		// 	echo json_encode($return);
		// }
	}

	public function upload_content_image(){
		$image_info = getimagesize($_FILES["filename"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];



		$return = array('status'=>false);
		$return['i'] =$this->input->post('i');

		// var_dump($image_info);return;
		$file_name = 'content_'.base_convert(time(),10,36);
		$filename = $this->input->post('filename');	
		$filename = $this->input->post('filename');	
		$config['upload_path'] = 'public_html/upload/content/';
		$config['allowed_types'] = 'gif|jpg|png|webp|jpeg|GIF|JPG|PNG|JPEG|WEBP';
		$config['max_size']	= '1000000';
		$config['max_width']  = '102400';
		$config['max_height']  = '76800';
		$config['file_name'] = $file_name.'.jpg';
		$config['overwrite'] = false;
		// if($image_width=='1200' && $image_height=='800'){
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('filename')) {
				$return['error']= $this->upload->display_errors();
				echo json_encode($return);
			}else{
				$data = $this->upload->data();
				$return['file']= $data['file_name'].'?t='.base_convert(time(),10,36);
				$config = array();
				//$config['image_library'] = 'gd2';
				$config['source_image']	= "public_html/upload/content/".$data['file_name'];
				$config['new_image']	= "public_html/upload/content/thumb/".$data['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = false;
				$config['width']	= 120;
				$config['height']	= 120;	
				$this->load->library('image_lib', $config); 
				$this->image_lib->resize();	
				$return['status']= true;	
				$return['e_type']= '2';	
				echo json_encode($return);
			}
		// }else{
		// 	$return['error']= 'maximum image size is 1200*800 ..';
		// 	$return['e_type']= '1';
		// 	echo json_encode($return);
		// }
	}

	

	public function upload_gallery_image(){
		$image_info = getimagesize($_FILES["filename"]["tmp_name"]);
		$image_width = $image_info[0];
		$image_height = $image_info[1];
		$return = array('status'=>false);
		$return['i'] =$this->input->post('i');
		$file_name = 'gallery_img_'.base_convert(time(),10,36);
		$filename = $this->input->post('filename');	
		$filename = $this->input->post('filename');	
		$config['upload_path'] = 'public_html/upload/gallery_img/';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|GIF|JPG|PNG|JPEG';
		$config['max_size']	= '1000000';
		$config['max_width']  = '102400';
		$config['max_height']  = '76800';
		$config['file_name'] = $file_name.'.jpg';
		$config['overwrite'] = false;
		// if($image_width=='1200' && $image_height=='800'){
			$this->load->library('upload', $config);
			if ( ! $this->upload->do_upload('filename')) {
				$return['error']= $this->upload->display_errors();
				echo json_encode($return);
			}else{
				$data = $this->upload->data();
				$return['file']= $data['file_name'].'?t='.base_convert(time(),10,36);
				$config = array();
				//$config['image_library'] = 'gd2';
				$config['source_image']	= "public_html/upload/gallery_img/".$data['file_name'];
				$config['new_image']	= "public_html/upload/gallery_img/thumb/".$data['file_name'];
				$config['create_thumb'] = FALSE;
				$config['maintain_ratio'] = false;
				$config['width']	= 120;
				$config['height']	= 120;	
				$this->load->library('image_lib', $config); 
				$this->image_lib->resize();	
				$return['status']= true;	
				$return['e_type']= '2';	
				echo json_encode($return);
			}
		// }else{
		// 	$return['error']= 'maximum image size is 1200*800 ..';
		// 	$return['e_type']= '1';
		// 	echo json_encode($return);
		// }
	}








}



?>