<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menus_ctrl extends admin_index {
 
	
	public function __construct(){ 
		parent::__construct();
		$data = array();
		$this->load->model('admin_model');	
	}
	
	/*----------------- load menu list page -----------------*/
	public function menus_list($ml_id=''){	
		$data['sdata'] 				= array();
		$data['subview'] 			= 'menus/menus_list';
		$data['active_cls'] 		= 'me_cls'; 		
		$data['view_name'] 			= "Menu's List"; 		
		if($ml_id==''){
			$data['view_type'] 			= "Add"; 		
 		}else{
			$data['view_type'] 			= "Edit"; 					 
		}
		// $data['sdata']['ml_list'] 	= $this->admin_model->get_menu_details('menus_list');	
		$data['ml_list'] 		= $this->admin_model->get_menu_details();		
		$data['ml_cli_list'] 	= $this->admin_model->get_menu_details('','','','clients');	
		$data['ml_cus_list'] 	= $this->admin_model->get_menu_details('','','','customer-says');
		$data['sdata']['ml_data'] 	= $this->admin_model->get_row_array_with_one_where('menus_list','ml_id',$ml_id);	
		$data['sdata']['ml_cli_list'] 	= $this->admin_model->get_menu_details('','','','clients');	
		$data['sdata']['ml_cus_list'] 	= $this->admin_model->get_menu_details('','','','customer-says');

		$this->load->view('common/sidepanel',$data);
	}
	
 
	/*----------------- add menu details  details and edit menu details  details -----------------*/
	public function add_menu_details($ml_id=false){	
		// var_dump($_POST);return;	 
		
		$this->form_validation->set_rules('ml_name', 'Name', 'trim|required'); 
		$this->form_validation->set_rules('ml_slug', 'Slug', 'trim|required'); 
		$this->form_validation->set_rules('ml_title', 'Title', 'trim|required'); 
		$this->form_validation->set_rules('ml_desc', 'Description', 'trim|required'); 
		$this->form_validation->set_rules('ml_keyword', 'Keyword', 'trim|required');  
		
		if ($this->form_validation->run() === FALSE) {
			$e_msg = '';
			$e_msg .= form_error('ml_name');      
			$e_msg .= form_error('ml_slug');      
			$e_msg .= form_error('ml_title');      
			$e_msg .= form_error('ml_desc');      
			$e_msg .= form_error('ml_keyword');       
			$this->session->set_flashdata('tos_error', $e_msg);
			redirect('menus_list');	
		}	
		$data['ml_details'] = array();
		foreach($_POST as $post_key=>$post_rows){
			if($post_key=='ml_slug'){
				$string = str_replace(' ', '-', $this->input->post($post_key)); 
   				$string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
				$string = strtolower($string);
				$data['ml_details'][$post_key] 	= $string;
			}else{
				$data['ml_details'][$post_key] 	= $this->input->post($post_key);
			}
		}  
		if($ml_id==false){   
										
			$add_ml 	= $this->admin_model->insert_item('menus_list',$data['ml_details']);  
			if($add_ml!=false){
				$this->session->set_flashdata('tos_success', 'Menu details added.');
				redirect('menus_list');
			}else {
				$this->session->set_flashdata('tos_error', 'Try again !...');
				redirect('menus_list');
			}	
		}		
		else{
			$edit_ml = $this->admin_model->update_data('menus_list','ml_id',$ml_id,$data['ml_details']);
			if($edit_ml=='1'){
				$this->session->set_flashdata('tos_success', 'Menu details updated');
				redirect('menus_list');
			}else {
				$this->session->set_flashdata('tos_warning', 'There is no changes!...');
				redirect('menus_list');
			}
		} 			
	}


	/*----------------- load arange menu list page -----------------*/
	public function arrange_menu_list(){	
		$data['sdata'] 				= array();
		$data['subview'] 			= 'menus/arrange_menu_list';
		$data['active_cls'] 		= 'ag_me_cls'; 		
		$data['view_name'] 			= "Arrange Menu's"; 		
		 
		$data['ml_list'] 		= $this->admin_model->get_menu_details();		
		$data['ml_cli_list'] 	= $this->admin_model->get_menu_details('','','','clients');	
		$data['ml_cus_list'] 	= $this->admin_model->get_menu_details('','','','customer-says');			

		$data['sdata']['ml_list'] 	= $this->admin_model->get_menu_details();			 	
		$data['sdata']['ml_null_list'] 	= $this->admin_model->get_menu_details();			 	
		// echo $this->db->last_query();return;
		$this->load->view('common/sidepanel',$data);
	}

		/*----------------- arange menu details   -----------------*/
		public function arrange_menu_details($ml_id=false){	 			
			 	
			$ml_pos = 1;
			foreach($_POST['ml_parent'] as $ml_p_row){ 
				$data['ml_details'] = array();
				
				$p_data = explode('_',$ml_p_row);
				$p_size = sizeOf($p_data);
				$ml_id = end($p_data);
				if($p_size=='2'){	echo "11";
					$data['ml_details']['ml_menu_position'] = $ml_pos;
					$data['ml_details']['ml_main_menu_id'] = $p_data[0];				
					
					$ml_pos = $ml_pos+1;
				}
				if($p_size=='3'){	echo "12";
					$data['ml_details']['ml_main_menu_id'] = '1';					
					$data['ml_details']['ml_parent_ml_id'] = $p_data[1];  
				}
				if($p_size=='4'){	echo "143";
					$data['ml_details']['ml_main_menu_id'] = '1';					
					$data['ml_details']['ml_parent_ml_id'] = $p_data[1];
					$data['ml_details']['ml_child_p_ml_id'] = $p_data[2];  
				}
				$arrange_ml = $this->admin_model->update_data('menus_list','ml_id',$ml_id,$data['ml_details']); 
			}  //return;
			$this->session->set_flashdata('tos_success', 'Menu details updated');
			redirect('menus_ctrl/arrange_menu_list');
		}
	
		/*----------------- delete menu   -----------------*/	
		public function delete_menu($ml_id){	 
			$this->db->where('ml_id',$ml_id)
			// ->delete("menus_list");
				->update('menus_list', array('ml_active' => '0'));
			$this->session->set_flashdata('tos_success', 'Menu Deleted');
			redirect('menus_list');
		}
	
 
}
