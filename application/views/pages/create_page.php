<!--  light box  -->
<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/added_plugins/lightbox/simple-lightbox.css">
<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/lightbox/simple-lightbox.js"></script>



<div class="row"> 
	<div class="col-12 col-sm-12">
		<div class="card-body">
			<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/create_page">

				<div class="col-sm-4" style=" margin: 0px; display: inline-block;margin-bottom:10px; ">  
					<select class="form-control menu_c_cls" name="ml_id">
						<option value="">Select Menu</option>
						<?php 
							foreach($ml_list as $ml_key=>$ml_row){ 
						?>
								<option value="<?php echo $ml_row['ml_id'];?>" <?php if(isset($ml_id) && ($ml_id==$ml_row['ml_id'])){echo "selected";}?>><?php echo $ml_row['ml_name'];?></option>
						<?php 
							} 
						?>
					</select>

					<button type="submit" class="btn menu_submit_cls" style="top:20px;display:none;">Save</button>
					<a class="btn btn-primary" href="<?php echo base_url();?>" style="top:20px;display:none;">View</a>
					 
				</div> 
			</form>
		</div>
		<div class="card card-primary card-outline card-outline-tabs">
			<div class="card-header p-0 border-bottom-0">
				<ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tab-slider-tab" data-toggle="pill" href="#tab-slider" role="tab" aria-controls="tab-slider" aria-selected="true">Slider </a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-content-tab" data-toggle="pill" href="#tab-content" role="tab" aria-controls="tab-content" aria-selected="false">Page Content</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-img-upload-tab" data-toggle="pill" href="#tab-img-upload" role="tab" aria-controls="tab-img-upload" aria-selected="false">Image Gallery </a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-img-upload-tab" data-toggle="pill" href="#tab-img-upload" role="tab" aria-controls="tab-img-upload" aria-selected="false">Add Multiple Image With Content </a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-video-upload-tab" data-toggle="pill" href="#tab-video-upload" role="tab" aria-controls="tab-video-upload" aria-selected="false">Video Gallery</a>
					</li>
				</ul>
			</div>
			<div class="card-body">
				<div class="tab-content" id="custom-tabs-four-tabContent">
					<div class="tab-pane fade show active" id="tab-slider" role="tabpanel" aria-labelledby="tab-slider-tab">					 
						<form class="form_submit"  method="POST" action="<?php echo base_url()?>page_ctrl/add_slider_details">
							
							<div class="col-md-12">
								<div class="card card-outline card-info">
									<div class="card-header">
										<h3 class="card-title">
											Content
										</h3>
									</div> 
									<div class="card-body">
										<textarea id="summernote1" class="sl_content_cls" style="height:10px;" name="sl_content">
											Please enter the content here ....
										</textarea>
									</div> 
								</div>
							</div>
							<div class=" col-md-4  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
								<input type="file" class="fileupload form-control" name="filename" id="avatar" style="opacity: 0;">									
								<input type="hidden" class="form-control form-white ml_id_cls" name="sl_ml_id" value="<?php if(isset($ml_data['ml_id'])){echo $ml_data['ml_id'];}?>"> 
								<input type="hidden" class="form-control form-white filename1" name="sl_img"  > 
								<label class="label_f_name" style="float: left;color:#000;margin-top: -27px;">  Upload Your Slider </label>	
								<p class="pro_id_shop" ></p>									
																
							</div>		

							<div class=" col-md-1"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">					
								<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-12" style="top:20px;">Save</button>
							</div> 
						</form>

						<div class="col-md-12" id="slider_div_id">							
						</div>
					</div>
					<div class="tab-pane fade" id="tab-content" role="tabpanel" aria-labelledby="tab-content-tab">
						<form class="form_submit pcl_form_cls"  method="POST" action="<?php echo base_url()?>page_ctrl/add_content_details">
							<div class=" col-md-2  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
								<input type="file" class="fileupload2 form-control" name="filename" id="avatar" style="opacity: 0;">
								<input type="hidden" class="form-control form-white pcl_img_cls filename2" name="pcl_img"  > 
								<label class="label_f_name2" style="float: left;color:#000;margin-top: -27px;">  Upload Your Image </label>	
								<p class="pro_id_shop2" ></p>									
																
							</div>
							<div class="form-group col-md-4"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
								<input type="text" name="pcl_title" class="form-control pcl_title_cls" placeholder="Enter title"   required> 

								<input type="hidden" class="form-control form-white ml_id_cls" name="pcl_ml_id" value="<?php if(isset($ml_data['ml_id'])){echo $ml_data['ml_id'];}?>"> 
							</div>		
							
							<div class="form-group col-md-5"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
								<input type="text" name="pcl_sub_title" class="form-control pcl_stitle_cls" placeholder="Enter Sub  title"   >   
							</div>

							<div class="col-md-12">
								<div class="card card-outline card-info">
									<div class="card-header">
										<h3 class="card-title">
											Content
										</h3>
									</div> 
									<div class="card-body">
										<textarea id="summernote" class="pcl_content_cls" style="height:50px;" name="pcl_content">
											Please enter the content here ....
										</textarea>
									</div> 
								</div>
							</div>
							<div class=" col-md-12"  style=" margin: 0px; display: inline-block;margin-bottom:10px;">		
								<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-1" style="top:20px;">Save</button>
							</div> 
  
							<div class=" col-md-12" > <br><br><br> </div>												
						</form>

						<div class="col-md-12" >	
							<div class="row">
          						<div class="col-12">
            						<div class="card">
										<div class="card-header">
											<h3 class="card-title">Page Content</h3>
										</div>
               
              							<div class="card-body">
											<table id="example2" class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>Title</th>
														<th> </th> 
													</tr>
												</thead>
												<tbody id="page_c_div_id">									
												
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>					
						</div>
					</div>
					<div class="tab-pane fade" id="tab-img-upload" role="tabpanel" aria-labelledby="tab-img-upload-tab">
						
						<form class="form_submit gtl_form_cls"  method="POST" action="<?php echo base_url()?>page_ctrl/add_gallery_details">
							<div class="form-group col-md-4" style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;">   
								<input type="text" name="gtl_title" class="form-control gtl_title" placeholder="Enter title"   required>  
							</div>
							<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
								<input type="file" class="fileupload_gtl form-control" name="filename" id="avatar" style="opacity: 0;">
								<input type="hidden" class="form-control form-white gtl_i_img_cls filename2" name="gtl_i_img"  > 
								<label class="label_gtl_i_img" style="float: left;color:#000;margin-top: -27px;">  Upload Icon Image </label>	
								<p class="pro_id_shop_gtl" ></p>									
																
							</div>
							 
							<div class=" col-md-4  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
								<input type="file" class="fileupload1 form-control" name="filename" id="avatar" style="opacity: 0;">									
								<input type="hidden" class="form-control form-white ml_id_cls" name="gtl_ml_id" value="<?php if(isset($ml_data['ml_id'])){echo $ml_data['ml_id'];}?>">  
								<label class="label_gtl_name" style="float: left;color:#000;margin-top: -27px;">  Upload Your Gallery Image </label>									
								<p class="pro_id_shop" ></p>									
							</div>		
							<div class="col-md-12" style="margin: 0px;display: inline-block;height: 50px;margin-bottom:10px;">  
								<textarea class="col-md-12 gtl_desc_cls"  name="gtl_desc">
									Short desciption here ....
								</textarea>
								
							</div>

							<div class="col-md-12" >
								<div class="card card-outline card-info">
									<div class="card-header">
										<h3 class="card-title">
											Main Content
										</h3>
									</div> 
									<div class="card-body">
										<textarea id="summernote_gtl1" class="gtl_content_cls" style="height:50px;" name="gtl_content">
										
										</textarea>
									</div> 
								</div>
							</div>
							<div class="col-md-12 other_content_cls" style="display:none;" >
								<div class="card card-outline card-info">
									<div class="card-header">
										<h3 class="card-title">
											Other Content
										</h3>
									</div> 
									<div class="card-body">
										<textarea id="summernote_gtl2" class="gtl_content_cls" style="height:50px;" name="gtl_content2">
											<ul>
												<li>Location: <span>Dubai, UAE</span></li>
												<li>Company website: <span><a class="text-second" href="www.fabtechint.com" target="_blank">www.fabtechint.com</a></span></li>
												<li>Machines Installed: <span class="tm-project-detail-list">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></li>									
											</ul>
										</textarea>
									</div> 
								</div>
							</div>
							

							<div class="col-md-12" id="gallery_img_div">							
							</div>

							<div class=" col-md-1"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">					
								<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-12" style="top:20px;">Save</button>
							</div> 
							<div class="col-md-12 g_img" id="gallery_img_load_div">									 					
							</div>
						</form>

					</div>
					<div class="tab-pane fade" id="tab-video-upload" role="tabpanel" aria-labelledby="tab-video-upload-tab">
						<form class="form_submit"  method="POST" action="<?php echo base_url()?>page_ctrl/add_video_url_details">
							<div class="form-group col-md-4" style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;">   
								<input type="text" name="vgl_url" class="form-control" placeholder="Enter Yputubr url"   required> 
								<input type="hidden" class="form-control form-white ml_id_cls" name="vgl_ml_id" value="<?php if(isset($ml_data['ml_id'])){echo $ml_data['ml_id'];}?>"> 
								 
							</div> 					
							<div class=" col-md-1"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">					
								<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-12" style="top:20px;">Save</button>
							</div>							
						</form>

						<div class="col-md-12" >	
							<div class="row">
								<div class="col-12">
									<div class="card">
										<div class="card-header">
											<h3 class="card-title">Page Video url's</h3>
										</div>
										<div class="card-body">
											<table id="example2" class="table table-bordered table-hover">
												<thead>
													<tr>
														<th>URL</th> 
														<th></th>
													</tr>
												</thead>
												<tbody id="page_v_url_id">									
														
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>					
						</div>
					</div>
				</div>
			</div>
			<!-- /.card -->
		</div>
	</div>
</div>


<script type="text/javascript">
	var up_status = '';
			
	$(document).ready(function(){
		$('.fileupload').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_slider_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename1').html('');
					$('.filename1').val(data.result.file); 					
					$('.label_f_name').text(data.result.file);
					$('.pro_id_shop').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				} else{
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}
				$('.pro_id_shop').html('');	
			}
		});
		 
		$('.fileupload1').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_gallery_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 
				up_status = '';		
				if(data.result.status==true){
					this_div_count = $('.g_div_cls').length;  		
					up_status='true'; 
					$('#gallery_img_div').append('<div class="col-lg-2 form-group row g_div_cls" id="gl_div_id_'+this_div_count+'" style="margin: 0px !important; display: inline-block;">'+
								'<img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/'+data.result.file+'" alt=""width="50" height="50">'+
								'<a class="close_cls" data-close_id="gl_div_id_'+this_div_count+'"><i class="fas fa-times"></i></a>'+
								'<input type="hidden" class="filename_'+this_div_count+'" readonly="" name="gl_img[]" value="'+data.result.file+'"> <br><br>'+ 
								'<input type="hidden" name="gl_id[]" value="0"> <br><br>'+					
								'</div>');	
					$('.pro_id_shop').html('');	
				}else if(data.result.status==false){
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}else{
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}	
				$('.pro_id_shop').html('');
			}
		});

		$('.fileupload2').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_content_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop2').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename2').html('');
					$('.filename2').val(data.result.file); 					
					$('.label_f_name2').text(data.result.file);
					$('.pro_id_shop2').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				} else{
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				}
				$('.pro_id_shop2').html('');	
			}
		});

		$('.fileupload_gtl').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_gallery_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop_gtl').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.gtl_i_img_cls').html('');
					$('.gtl_i_img_cls').val(data.result.file); 					
					$('.label_gtl_i_img').text(data.result.file);
					$('.pro_id_shop_gtl').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop_gtl').html('');				
					$('.pro_id_shop_gtl').append('Try again!..');
				} else{
					$('.pro_id_shop_gtl').html('');				
					$('.pro_id_shop_gtl').append('Try again!..');
				}
				$('.pro_id_shop_gtl').html('');	
			}
		});


		
	});
			
	function move(id,p_id) {
		//if(up_status=='true'){
			$('#'+id+'').css('display','block');
			var elem = document.getElementById(''+id+''); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
					$('#'+p_id+'').text('');
					$('#'+p_id+'').text(width+'%');
				}
			}
		//}		
	}

	$('.menu_c_cls').change(function(){
		ml_id = $(this).val();
		$('#slider_div_id').html('');
		$('#page_c_div_id').html('');
		$('#gallery_img_load_div').html('');
		$('#page_v_url_id').html('');
		$.ajax({
			type: "POST",
			url: "<?php echo base_url()?>page_ctrl/get_menu_data",
			data: {"ml_id":ml_id},
			dataType: "json",
			success: function(data){
				console.log(data.ml_data);
				$('.ml_id_cls').val(ml_id);
				$('.other_content_cls').css(ml_id);
				if(data.ml_data['ml_type']=="project"){
					$(".other_content_cls").css("display", "block");
					$(".other_content_cls").css("font-color", "black");
				}
				if(data.ml_data['ml_type']!="project"){
					$(".other_content_cls").css("display", "none");
				}

				console.log(data.sl_list);
				$.each(data.sl_list , function(i,n){ 
					$('#slider_div_id').append('<div class="col-md-2 slider_cls_'+n.sl_id+'" style="display:inline-block">'+
								'<div class="col-md-1 " style="    margin-left: -30px;"><a class="close_btn"  data-delete_type="slider_tr_del" data-delete_id="'+n.sl_id+'"> <i class="fas fa-times " aria-hidden="true"></i></a></div>'+	
								'<img src= "<?php echo base_url()?>/public_html/upload/slider/thumb/'+n.sl_img+'">'+
                            	'</div>');                                         
                 
                       
				}); 

				$.each(data.pcl_list , function(i1,n1){ 
					pcl_content = $(n1.pcl_content).text()
					$('#page_c_div_id').append('<tr class="pcl_tr_cls_'+n1.pcl_id+'"> <td>'+n1.pcl_title+'</td> <td>'+pcl_content.substring(0,120)+'....</td>'+
					' <td> <a class="edit_cls" data-edit_type="pcl_edit" data-edit_val_id="'+n1.pcl_id+'"><i class="fas fa-edit" aria-hidden="true"></i></a> &nbsp;'+
					'<a class="close_btn" data-delete_type="pcl_tr_del" data-delete_id="'+n1.pcl_id+'"><i class="fas fa-times" aria-hidden="true"></i></a></td>  </tr>');  
				});				

				$.each(data.vgl_list , function(i2,n2){ 
					$('#page_v_url_id').append('<tr class="vgl_tr_cls_'+n2.vgl_id+'"> <td>'+n2.vgl_url+'</td> <td>'+
											'<a class="close_btn" data-delete_type="vgl_tr_del" data-delete_id="'+n2.vgl_id+'"><i class="fas fa-times " aria-hidden="true"></i></a></td>  </tr>');  
				});

				$.each(data.gtl_list , function(i3,n3){ 
					gtl_div1 = '<div class="gallery col-md-12" id="gallery_div_m_'+n3.gtl_id+'"><div class="col-md-12"> <div class="col-md-3" style="display: inline;"> <h2>'+n3.gtl_title+'</h2></div>'+
								'<div class="col-md-3" style="display: inline;"><a class="edit_cls" data-edit_type="gtl_edit" data-edit_val_id="'+n3.gtl_id+'"><i class="fas fa-edit" aria-hidden="true"></i></a> &nbsp;'+
								'<a class="close_btn" data-delete_type="gtl_id_del" data-delete_id="'+n3.gtl_id+'"><i class="fas fa-times" aria-hidden="true"></i></a></div> </div>';
					$.each(data.gl_list , function(i4,n4){ 
						if(n3.gtl_id==n4.gl_gtl_id){
							gtl_div1 += '<div class="col-md-2" id="gl_div_id_0'+n4.gl_id+'" style="display: inline;"><a href="<?php echo base_url();?>public_html/upload/gallery_img/'+n4.gl_img+'" target="_blank" class="col-md-2 "><img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/'+n4.gl_img+'" alt="" title=""/></a>'+
							'<a class="close_btn" data-delete_type="img_delete" data-delete_id="'+n4.gl_id+'"><i class="fas fa-times"></i></a></div>';
						}  
					});
					gtl_div1 += '<div class="clear"></div> </div>';
					
					$('#gallery_img_load_div').append(gtl_div1);  
				});


			}
		});
	});

	$('body').on('click', '.edit_cls',function(){
		this_type = $(this).data('edit_type');
		this_id = $(this).data('edit_val_id');  

		if(this_type=="pcl_edit"){
			form_action = '<?php echo base_url()?>page_ctrl/add_content_details'
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/get_menu_data_by_id",
				data: {"type":this_type,"id":this_id},
				dataType: "json",
				success: function(data){ 
					$('.label_f_name2').html(''); 
					$('.pcl_img_cls').val(data['pcl_data']['pcl_img']); 
					$('.label_f_name2').html(data['pcl_data']['pcl_img']); 
					$('.pcl_title_cls').val(data['pcl_data']['pcl_title']); 
					$('.pcl_stitle_cls').val(data['pcl_data']['pcl_sub_title']); 
					$('#summernote').summernote('code','')
					$('#summernote').summernote('code',data['pcl_data']['pcl_content']);
					form_action = form_action+'/'+data['pcl_data']['pcl_id'];
					$('.pcl_form_cls').attr('action',''); 
					$('.pcl_form_cls').attr('action',form_action); 
					
					
				}
			});
		}


		if(this_type=="gtl_edit"){
			form_action = '<?php echo base_url()?>page_ctrl/add_gallery_details'
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/get_menu_data_by_id",
				data: {"type":this_type,"id":this_id},
				dataType: "json",
				success: function(data){ 
					$('.gtl_i_img_cls').val('');  
					$('.gtl_i_img_cls').val(data['gtl_data']['gtl_i_img']);  
					$('.label_gtl_i_img').html('');  
					$('.label_gtl_i_img').html(data['gtl_data']['gtl_i_img']);  
					$('.gtl_title').val(data['gtl_data']['gtl_title']);  
					$('.gtl_desc_cls').val(data['gtl_data']['gtl_desc']);  

					$('#summernote_gtl1').summernote('code', data['gtl_data']['gtl_content']);
					$('#summernote_gtl2').summernote('code', data['gtl_data']['gtl_content2']);

					$.each(data.gl_data , function(i4,n4){ 	
						$('#gallery_img_div').append('<div class="col-lg-2 form-group row g_div_cls" id="gl_div_id_0'+n4.gl_id+'" style="margin: 0px !important; display: inline-block;">'+
								'<img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/'+n4.gl_img+'" alt=""width="50" height="50">'+
								'<a class="close_btn" data-delete_type="img_delete" data-delete_id="'+n4.gl_id+'"><i class="fas fa-times"></i></a>'+
								'<input type="hidden" class="filename_0'+i4+'" readonly="" name="gl_img[]" value="'+n4.gl_img+'"> <br><br>'+ 
								'<input type="hidden" name="gl_id[]" value="'+n4.gl_id+'"> <br><br>'+ 
								'</div>'); 
					});
					
					form_action = form_action+'/'+data['gtl_data']['gtl_id'];


					$('.gtl_form_cls').attr('action',''); 
					$('.gtl_form_cls').attr('action',form_action); 
					
					
				}
			});
		}
	});

	$('body').on('click', '.close_cls',function(){
		this_c_id = $(this).data('close_id');  
		$("#"+this_c_id+'').remove();
	});

	$('body').on('click', '.close_btn',function(){
		console.log('fff');
		this_del_type = $(this).data('delete_type');
		this_del_id = $(this).data('delete_id'); 
		if(this_del_type=="slider_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".slider_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}
		if(this_del_type=="pcl_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".pcl_tr_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}
		if(this_del_type=="vgl_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".vgl_tr_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}

		if(this_del_type=="img_delete"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$("#gl_div_id_0"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}

		if(this_del_type=="gtl_id_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$("#gallery_div_m_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}

	});
</script>

<script>
  $(function () {
    // Summernote
    $('#summernote').summernote();
    $('#summernote1').summernote();

    $('#summernote_gtl1').summernote();
    $('#summernote_gtl2').summernote();

     
  })
</script>



<script>
    (function() {
        var $gallery = new SimpleLightbox('.g_img.gallery a', {});
    })();
</script>