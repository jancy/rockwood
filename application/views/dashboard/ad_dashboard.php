<style>
	.text-white{
		color:#000 !important;
	}
</style>
<div class="container-fluid mt-3">
	<div class="row">
		<div class="col-lg-3 col-sm-6 color_cls">
			<div class="card gradient-1">
				<div class="card-body">
					<h3 class="card-title text-white"> <a href="<?php echo base_url();?>menus_list">Menus </a></h3>
					<div class="d-inline-block">
						<h2 class="text-white"> <?php echo $ml_count;?> </h2> 
					</div>
					<span class="float-right display-5 opacity-5"><i class="fa fa-list"></i></span>
				</div>
			</div>
		</div>
		 
	</div>
	 
</div>
<!-- #/ container -->