
<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>

		<div id="content" class="site-content">
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<section class="bg-light tm-services">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4 class="text-primary"><?php echo $pcl_list[0]['pcl_title'];?></h4>
							<h2><?php echo $pcl_list[0]['pcl_sub_title'];?></h2>
							<p><?php echo $pcl_list[0]['pcl_content'];?></p>
							<div class="industris-space"></div>

							<div class="project-slider project-home" data-show="1" data-arrow="true">
								<?php 
									foreach($gtl_list as $gtl_row){
	 							?>
										<div>
											<div class="project-box">
												<?php 
													foreach($gl_list as $gl_row){
														if($gl_row['gl_gtl_id']==$gtl_row['gtl_id']){
												?>
															<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $gl_row['gl_img'];?>" alt="" class="">
												<?php
														}
													}
												?>
												<div class="col-md-offset-6 col-md-5 col-xs-offset-4 col-xs-8">
													<div class="project-slider-content">
														<h3><?php echo $gtl_row['gtl_title'];?></h3>
														<p><?php echo $gtl_row['gtl_content'];?></p>
													</div>
												</div>
											</div>
										</div>
								<?php
									}
								?>
								
	
							</div>

						</div>
					</div>
			    </div>
			</section>	
			<section class="tm-services-form">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h3>Get Your Free Consultation & Quote</h3>
							<p>As fellow entrepreneurs, we understand the need for space which gives your business room to breathe and grow.</p>
						</div>
					</div>
					<div class="industris-space-30"></div>
					<form class="home-form" method="post" action="">
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" name="name" class="form-control" id="name" placeholder="Your name" required>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<input type="email" name="email" class="form-control" id="email" placeholder="Email address" required>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<input type="text" name="phone" class="form-control" id="phone" placeholder="Phone number" required>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<button type="submit" class="wpcf7-form-control wpcf7-submit btn btn-primary btn-full-width">Submit now <i class="icon ion-md-checkmark-circle"></i></button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</section>	

		</div>
		