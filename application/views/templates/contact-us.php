
<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>
		<div id="content" class="site-content">
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<section class="bg-contact-info tm-contact">
				<div class="container">
					<div class="row">
						<div class="col-md-8 col-sm-12">
							<h4 class="text-primary">CONTACT INFO</h4>
							<div class="contact-info">
								<h2>Hotline : <a href="tel:+971 4 8816636" class="text-primary">+971 4 8816636</a> </h2>								
								<p><i class="icon ion-md-pin"></i> Rockwood International LLC, 501, Icon Tower, <br>Dubai Silicon Oasis, Dubai, UAE (<a href="https://goo.gl/maps/VCEZuRi56kJhcbXK8" target="_blank" class="text-primary">View map</a>)</p>
							</div>													
						</div>
						<div class="col-md-12 tm-contact-address">
							<hr>
							<div class="space-industris" style="height: 40px;"></div>	
							<div class="col-md-4 col-sm-6">
								<div class="icon-box">
									<div class="icon-box-title">
										<h4>Office</h4>
									</div>
									<p><a href="tel:+971 4 8816636"><i class="icon ion-md-phone-portrait"></i>+971 4 8816636</a></p>
									<p><a href="mailto:info@rockwoodmachinery.com"><i class="icon ion-md-mail"></i>info@rockwoodmachinery.com</a></p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="icon-box">
									<div class="icon-box-title">
										<h4>Sales</h4>
									</div>
									<p><a href="tel:+971 4 8816636"><i class="icon ion-md-phone-portrait"></i>+971 4 8816636</a></p>
									<p><a href="tel:+971 50 5541290"><i class="icon ion-md-phone-portrait"></i>+971 50 5541290</a></p>
									<p><a href="tel:+971 56 263 0542"><i class="icon ion-logo-whatsapp"></i>+971 56 263 0542</a></p>
									<p><a href="mailto:sales@rockwoodmachinery.com"><i class="icon ion-md-mail"></i>sales@rockwoodmachinery.com</a></p>
								</div>
							</div>
							<div class="col-md-4 col-sm-6">
								<div class="icon-box">
									<div class="icon-box-title">
										<h4>Support</h4>
									</div>
									<p><a href="tel:+971 4 8816636"><i class="icon ion-md-phone-portrait"></i>+971 4 8816636</a></p>
									<p><a href="tel:++971 55 5524756"><i class="icon ion-md-phone-portrait"></i>+971 55 5524756</a></p>
									<p><a href="mailto:service@rockwoodmachinery.com"><i class="icon ion-md-mail"></i>service@rockwoodmachinery.com</a></p>
								</div>
							</div>							
						</div>						
						<div class="col-md-12">
							<div class="space-industris" style="height: 40px;"></div>
							<form class="form-contact" action="contact.php" method="post">
								<h3>Contact form</h3>
								<div class="row">
									<div class="col-md-4 form-group">
										<input type="text" name="your_name" id="your-name" class="form-control" placeholder="Your name" required>
									</div>
									<div class="col-md-4 form-group">
										<input type="number" name="phone_number" id="phone-number" class="form-control" placeholder="Phone number" required>
									</div>
									<div class="col-md-4 form-group">
										<input type="email" name="your_email" id="your-email" class="form-control" placeholder="Email Address" required>
									</div>
									<div class="col-md-12 form-group">
										<textarea type="text" rows="6" name="your_message" id="your-message" class="form-control" placeholder="your message" required></textarea>
									</div>
									<div class="col-md-12">
										<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit btn btn-primary">										
									</div>
								</div>
							</form>
						</div>
					</div>
			    </div>
			</section>

			<section>
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<!-- <h3>View map:</h3> -->
							<div class="map">
								<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7224.787178737799!2d55.374135!3d25.122381!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa709096e8318d452!2sRockwood%20Machinery!5e0!3m2!1sen!2sin!4v1644584081724!5m2!1sen!2sin" width="1170" height="500" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
							</div>
							<div class="text-center">
								<a target="_blank" href="https://www.google.com/maps/dir//Rockwood+Machinery+-+Dubai+Silicon+Oasis+-+Industrial+Area+-+Dubai+-+United+Arab+Emirates/@25.122381,55.374135,16z/data=!4m8!4m7!1m0!1m5!1m1!1s0x3e5f64392137b20f:0xa709096e8318d452!2m2!1d55.374132!2d25.1223845?hl=en" class="btn btn-primary btn-m-r">Get derection</a>
								<a target="_blank" href="https://goo.gl/maps/uZ8F6xfn9e3DUHEs9" class="btn btn-border">Google Map</a>
							</div>
						</div>
					</div>
				</div>
			</section>
			
		</div>
	    