<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>

		<div id="content" class="site-content">
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section class="tm-rentals">
				<div class="container">
					<div class="row flex-row">
						<div class="col-md-7 col-sm-12 align-self-center">
							<h4 class="text-primary"><?php echo $pcl_list[0]['pcl_title'];?></h4>
							<h2><?php echo $pcl_list[0]['pcl_sub_title'];?></h2>					
							<p><?php echo $pcl_list[0]['pcl_content'];?></p>
						</div>
						<div class="col-md-5 col-sm-12">
							<img src="<?php echo base_url();?>public_html/upload/content/<?php echo $pcl_list[0]['pcl_img'];?>" alt="">
						</div>
					</div>
			    </div>
			</section>
			<section class="bg-default">
				<div class="container">
					<div class="row">						
						<div class="col-md-12">
							<form class="form-contact" action="contact.php" method="post">
								<h3 class="text-primary">For rental inquiry please fill the form</h3>
								<div class="row">
									<div class="col-md-4 form-group">
										<input type="text" name="your_name" id="your-name" class="form-control" placeholder="Your name" required>
									</div>
									<div class="col-md-4 form-group">
										<input type="number" name="phone_number" id="phone-number" class="form-control" placeholder="Phone number" required>
									</div>
									<div class="col-md-4 form-group">
										<input type="email" name="your_email" id="your-email" class="form-control" placeholder="Email Address" required>
									</div>
									<div class="col-md-12 form-group">
										<textarea type="text" rows="6" name="your_message" id="your-message" class="form-control" placeholder="your message" required></textarea>
									</div>
									<div class="col-md-12">
										<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit btn btn-border">
									</div>
								</div>
							</form>
						</div>
					</div>
			    </div>
			</section>
			<section class="tm-products-slider">
				<div class="container">	
					<div class="row">
						<div class="col-md-12">
							
							<div class="project-feature-slider project-feature" data-show="3" data-arrow="true">
								<?php  
									$rent = 0; 
									foreach($rent_list as $rent_key=>$rent_row1){  
										if($rent!=6){
								?>
											<div class="project-item col-lg-4 col-sm-6">
												<div class="inner">
													<img src="<?php echo base_url();?>public_html/upload/slider/<?php echo $rent_row1['ml_sub_img'];?>" alt=""> 
													<div class="project-info">
														<div class="project-content">
															<h3><a href="<?php echo base_url();?><?php echo $rent_row1['ml_slug'];?>"><?php echo $rent_row1['ml_name'];?></a></h3>
															<p> <?php echo $rent_row1['ml_s_desc'];?>....</p>
															<a href="<?php echo base_url();?><?php echo $rent_row1['ml_slug'];?>">View details <i class="icon ion-md-add-circle"></i></a>
														</div>
													</div>
												</div>
											</div>
	
								<?php
											$rent = $rent+1;
										}
									}
								?>								
	
							</div>
						</div>
					</div>					
				</div>
			</section>						
		</div>
	    