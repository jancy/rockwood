<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>

		<div id="content" class="site-content">
			 
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			 
			<section class="tm-overview">
				<div class="container tm-about-wrap">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<h4 class="text-primary">OUR STORY</h4>
								<h2><?php echo $pcl_list[0]['pcl_title'];?></h2>
								<p><?php echo $pcl_list[0]['pcl_content'];?></p>																
								<a href="<?php echo base_url();?>contact-us" class="btn btn-primary">Contact us</a>
								<div class="industris-space-sm"></div>
						</div>
						<div class="col-md-offset-1 col-md-5 col-sm-12 sm-center">
							<div class="about-img-right">
								<img src="<?php echo base_url();?>public_html/upload/content/<?php echo $pcl_list[0]['pcl_img'];?>" alt="">
								<div class="about-img-small">
									<div class="overlay"><img src="<?php echo base_url();?>public_html/site_assets/images/about-exp.jpg" class="circle-img" alt=""></div>
									<div class="about-content icon-box">
										<h2 class="text-primary"><span class="number" data-to="<?php echo $pcl_list[0]['pcl_sub_title'];?>" data-speed="2000">0</span>+</h2>
										<p class="text-white">Years of Experience</p>
									</div>
								</div>
							</div>
						</div>
					</div>
			    </div>

			    <div class="industris-space-60"></div>
			    <div class="industris-space"></div>
			    
			    <div class="container tm-about-icon">
			    	<div class="row">
			    		<div class="col-md-4 col-sm-6">
			    			<div class="icon-box">
			    				<div class="icon-box-title">
			    					<i class="icon ion-md-ribbon"></i><h4>Our Awards <span class="number" data-to="12" data-speed="2000">0</span><span>+</span></h4>
			    				</div>
			    				<p>Over 25 years with 12 different awards, we are extremely proud of that</p>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="icon-box">
			    				<div class="icon-box-title">
			    					<i class="icon ion-ios-briefcase"></i><h4>Our Work <span class="number" data-to="100" data-speed="2000">0</span><span>+</span></h4>
			    				</div>
			    				<p>More than 100 large and small projects are completed. It is an attempt to work with effort and passion</p>
			    			</div>
			    		</div>
			    		<div class="col-md-4 col-sm-6">
			    			<div class="icon-box">
			    				<div class="icon-box-title">
			    					<i class="icon ion-ios-contacts"></i><h4>Clients <span class="number" data-to="1000" data-speed="2000">0</span><span>+</span></h4>
			    				</div>
			    				<p>We have been chosen as their machinery partner by more than 1000 clients in the MEA region and we are still adding.</p>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</section>

			<section class="video-about">
				<div class="container-custom">
					<div class="bg-about-video"><img src="<?php echo base_url();?>public_html/site_assets/images/about-vission.jpg" alt=""></div>
					<div class="container">
				        <div class="breadc-box">
							<div class="row">
								<div class="col-md-7 col-sm-8 col-xs-12">
									<h4 class="text-white">A TRUSTED PARTNER FOR QUALITY AND ASSURANCE</h4>
									<h2 class="text-white">Connecting things, It’s all about people.</h2>
								</div>
								<!-- <div class="col-md-offset-1 col-md-6 col-sm-4 col-xs-12 text-right">
									<div class="about-video video-player">
										<a class="play-btn" href="https://www.youtube.com/watch?v=SyRchIzIq9I"><i class="icon ion-ios-play"></i></a>
									</div>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="bg-light tm-values">
				<div class="container">
					<div class="row">
						<div class="col-md-7">
							<h3>Our values</h3>
							<p><?php echo $pcl_list[1]['pcl_content'];?></p>							
							<div class="row">
								<div class="col-md-offset-1 col-md-6 no-padding-sm-left">
									<div class="about-author">										
										<div class="about-author-media">
											<a href="https://www.linkedin.com/company/rockwood-international-llc/"><i class="icon ion-logo-linkedin"></i></a>
											<a href="https://www.instagram.com/rockwoodinternational/"><i class="icon ion-logo-instagram"></i></a>
											<a href="https://www.facebook.com/rockwoodinternational/"><i class="icon ion-logo-facebook"></i></a>
										</div>
									</div>
								</div>
							</div>
							<div class="industris-space-sm"></div>
						</div>
						<div class="col-md-offset-1 col-md-4 about-border-left">
							<h3>Why we’re different</h3>
							<p>As fellow entrepreneurs, we understand the need for space which gives your business room</p>
							<ul class="unstyle">
								<li><i class="icon ion-md-checkmark-circle"></i><a href="#">1000+ clients and still adding</a></li>
								<li><i class="icon ion-md-checkmark-circle"></i><a href="#">Experience</a></li>
								<li><i class="icon ion-md-checkmark-circle"></i><a href="#">Most proven technology machines</a></li>
								<li><i class="icon ion-md-checkmark-circle"></i><a href="#">Integrity</a></li>
								<li><i class="icon ion-md-checkmark-circle"></i><a href="#">A trusted partner for quality and assurance</a></li>
								<li><i class="icon ion-md-checkmark-circle"></i><a href="#">It's all about you!</a></li>
							</ul>
							<a href="#" class="btn btn-primary">Get a quote</a>
						</div>
					</div>
				</div>
			</section>

			<section class="tm-clients-wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4 class="text-primary">CLIENTS</h4>
							<h2>What our customers say</h2>

							<div class="testi-slider" data-show="2" data-arrow="true">

							<?php 
								foreach($cut_say as $cut_row){
							?>
									<div>
										<div class="testi-box">
											<div class="testi-content">
												<p><?php echo $cut_row['pcl_content'];?></p>
											</div>
											<div class="testi-info">
												<!-- <img src="https://via.placeholder.com/90x90.png" alt="" class="circle-img"> -->
												<h4><?php echo $cut_row['pcl_title'];?><span><?php echo $cut_row['pcl_sub_title'];?></span></h4>
											</div>
										</div>
									</div>
							<?php
								}
							?> 
								

							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="padding-top-bottom-0">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="cta-box" style="background-image: url('<?php echo base_url();?>public_html/site_assets/images/about-download.jpg');">
								<div class="row">
									<div class="col-md-6">
										<h3>Download Our Complete Company Profile</h3>										
									</div>
									<div class="col-md-6 align-self-center text-right">
										<div class="career-download">
											<a href="images/company-profile-_RockWood.pdf" target="_blank" class="btn btn-border btn-download">Download<i class="i-btn icon ion-md-archive"></i></a>
										</div>
										<!-- <a href="images/company-profile-_RockWood.pdf" target="_blank" class="btn btn-border-white">Download</a> -->
										<!-- <a href="#" class="btn btn-primary">Contact us</a> -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section class="tm-clients">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							
                            <div class="partner-slider image-carousel text-center" data-show="5" data-arrow="false">
								<?php 
									foreach($client as $cil_row){
								?>
										<div>
											<div class="partner-item text-center clearfix">
												<div class="inner">
													<div class="thumb">
														<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $cil_row['gl_img'];?>" alt="">
													</div>
												</div>
											</div>
										</div>
								<?php
									}
								?> 
															
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	    

		