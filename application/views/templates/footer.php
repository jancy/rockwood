<footer id="site-footer" class="site-footer bg-footer">
	        <div class="main-footer">
	            <div class="container-custom">
	                <div class="row">

	                    <div class="col-md-3 col-sm-6">
	                    	<div class="widget-footer">
		                        <div id="media_image-1" class="widget widget_media_image">
		                            <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>public_html/site_assets/images/footer-logo.png" alt=""></a>
		                        </div>
		                        <div id="custom_html-1" class="widget_text widget widget_custom_html">
		                            <div class="textwidget custom-html-widget">
										<h4 class="text-primary">Address</h4>
		                                <p>Rockwood International LLC, 501, Icon Tower, Dubai Silicon Oasis, Dubai, UAE</p>
		                            </div>
		                        </div>
		                    </div>
	                    </div>
	                    <!-- end col-lg-3 -->
						<div class="col-md-3 col-sm-6">
	                    	<div class="widget-footer">
		                        <div id="custom_html-3" class="widget_text widge widget-footer widget_custom_html padding-left">
		                            <div class="textwidget custom-html-widget">
										<h4 class="text-primary">Quick Links</h4>
		                                <ul>
											<?php  
												foreach($ml_list as $ml_key=>$ml_row){  
											?>
													<li><a href="<?php echo base_url();?><?php echo $ml_row['ml_slug'];?>"><?php echo $ml_row['ml_name'];?></a></li> 
											<?php
												}
											?>
		                                </ul>
		                            </div>
		                        </div>
		                    </div>
	                    </div> 
	                    <!-- end col-lg-3 -->

	                    <div class="col-md-3 col-sm-6 tm-footer-projects">
	                    	<div class="widget-footer">
		                        <div id="custom_html-2" class="widget_text widget widget_custom_html padding-left">
		                            <div class="textwidget custom-html-widget">
										<h4 class="text-primary">Projects</h4>
		                                <ul>
											<?php  
												$pros = 0;
												foreach($pro_list as $pro_key=>$pro_row){  
													if($pros!=6){
											?>
														<li><a href="<?php echo base_url();?>projects/<?php echo str_replace(' ','-',$pro_row['gtl_title']);?>"><?php echo $pro_row['gtl_title'];?></a></li> 
											<?php
														$pros =$pros+1;
													}
												}
											?>
		                                </ul>
		                            </div>
		                        </div>
	                        </div>
	                    </div>
	                    <!-- end col-lg-3 -->

	                    

	                    <div class="col-md-3 col-sm-6 tm-footer-products">
	                    	<div class="widget-footer">
		                        <div id="custom_html-2" class="widget_text widget widget_custom_html padding-left">
		                            <div class="textwidget custom-html-widget">
										<h4 class="text-primary">Products</h4>
		                                <ul>
											<?php  
												$prods = 0;
												foreach($prod_list as $prod_key=>$prod_row){  
													if($prods!=6){
											?>
														<li><a href="<?php echo base_url();?><?php echo $prod_row['ml_slug'];?>"><?php echo $prod_row['ml_name'];?></a></li> 
											<?php
														$prods =$prods+1;
													}
												}
											?> 
		                                </ul>
		                            </div>
		                        </div>

	                            <div class="footer-social ot-socials bg-white">
	                                <a target="_blank" href="https://www.facebook.com/rockwoodinternational/" rel="noopener noreferrer"><i class="icon ion-logo-facebook"></i></a>
	                                <a target="_blank" href="https://www.instagram.com/rockwoodinternational/" rel="noopener noreferrer"><i class="icon ion-logo-instagram"></i></a>
	                                <a target="_blank" href="https://www.linkedin.com/company/rockwood-international-llc/" rel="noopener noreferrer"><i class="icon ion-logo-linkedin"></i></a>	                                
	                            </div>
	                        </div>
	                    </div>
	                    <!-- end col-lg-3 -->

	                </div>
	            </div>
	        </div>
	        <!-- .main-footer -->
	        <div class="footer-bottom">
	            <div class="container-custom">
	            	<div class="row">
	            		<div class="col-md-12">
							<div class="tm-copyright">
								<p>© 2022 RockWood | All Rights Reserved | Powered by Techminds</p>		                    
							</div>
			        		<a id="back-to-top" href="#" class="btn btn-back-to-top pull-right">Back to top<i class="icon ion-ios-arrow-dropup-circle"></i></a>
			        	</div>
			        </div>
	            </div>
	        </div>
	        <!-- .copyright-footer -->
	    </footer>
	</div>

	<script src='<?php echo base_url();?>public_html/site_assets/js/jquery.min.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/bootstrap.min.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/slick.min.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/jquery.sticky.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/countto.min.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/jquery.magnific-popup.min.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/jquery.isotope.min.js'></script>
	<script src='<?php echo base_url();?>public_html/site_assets/js/scripts.js'></script>
	<script>
		jQuery(window).scroll(function() { 
			if (jQuery(this).scrollTop() > 1){  
				jQuery('.main-header').addClass("tm-sticky-header");
			}
			else{
				jQuery('.main-header').removeClass("tm-sticky-header");
			}
		});
	</script>

	
</body>
</html>