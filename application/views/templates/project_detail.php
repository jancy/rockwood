<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/project-details-banner.jpg';
	}
?>

		<div id="content" class="site-content">
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li><a href="<?php echo base_url();?><?php echo $ml_data['ml_slug'];?>"><?php echo $ml_data['ml_name'];?></a></li>
			                        <li class="active"><?php echo $gtl_data['gtl_title'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>

			<section class="tm-project-details">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4 class="text-primary"><?php echo $ml_data['ml_title'];?></h4>
							<h2><?php echo $gtl_data['gtl_title'];?></h2>
							<div class="industris-space"></div>

							<div class="project-slider" data-show="1" data-arrow="true">
								<?php   
									foreach($gl_list as $pgl_row){  
								?>
										<div>
											<div class="project-box">
												<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $pgl_row['gl_img'];?>" alt="" class="">										
											</div>
										</div>
								<?php
									}
								?>

								 
								<div>
									<div class="project-box">
										<img src="images/fabtech-3.jpg" alt="" class="">
										
									</div>
								</div>

							</div>

						</div>
					</div>
			    </div>
			</section>

			<section class="no-padding-top tm-project-content">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<div class="project-detail-box">
								<?php echo $gtl_data['gtl_content2'];?>
							</div>
						</div>
						<div class="col-md-8">
							<!-- <h3>Process of implementation</h3> -->
							<p><?php echo $gtl_data['gtl_content'];?></p>
							<div class="industris-space-30"></div>
					        <div class="project-share share">
					        	<span>Share:</span>
					        	<a href="#"><i class="icon ion-logo-facebook"></i></a>
					        	<a href="#"><i class="icon ion-logo-instagram"></i></a>
					        	<a href="#"><i class="icon ion-logo-skype"></i></a>
					        	<a href="#"><i class="icon ion-logo-twitter"></i></a>
					        	<a href="#"><i class="icon ion-logo-youtube"></i></a>
					        </div>
						</div>
					</div>
				</div>
			</section>

			<section>
				<div class="container">
					<h3>Related project</h3>
					<div class="row">
						<?php  
							$ps = 0; 
	 						foreach($pro_slider as $pro_s_row1){ 
								if($ps!='3' && ($gtl_data['gtl_id']!=$pro_s_row1['gtl_id'])){ 
						?>
									<div class="col-md-4 col-sm-6">
										<div class="relate-project">
											<a href="<?php echo base_url();?>projects/<?php echo str_replace(' ','-',$pro_s_row1['gtl_title']);?>"><img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $pro_s_row1['gl_img'];?>" alt="relate project"></a>
										</div>
									</div>
						<?php
									$ps= $ps+1;
								}
							}
						?>
					</div>
				</div>
			</section>

		</div>
		