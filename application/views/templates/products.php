  
<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?> 
		<div id="content" class="site-content">
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section class="tm-overview-wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-7 col-sm-12">
							<h4 class="text-primary">OVERVIEW</h4>
							<?php  
								$pcl_img_n = ''; 
								$submenu1 	= $this->public_model->get_menu_details($ml_data['ml_id'],'','1'); 

								foreach($pcl_list as $pcl_row){  
									$pcl_img_n = $pcl_row['pcl_img'];
							?>
									<?php echo $pcl_row['pcl_content'];?>
							<?php
								}
							?>
							<br>
							<h3>Our Guarantees</h3>
							<div class="row">
								<div class="col-md-3 col-sm-4">
									<div class="feature-box">
										<div class="feature-icon">
											<i class="icon ion-ios-checkmark-circle-outline"></i>
										</div>
										<h4>Reliability</h4>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<div class="feature-box">
										<div class="feature-icon">
											<i class="icon ion-ios-ribbon"></i>
										</div>
										<h4>Dedication</h4>
									</div>
								</div>
								<div class="col-md-3 col-sm-4">
									<div class="feature-box">
										<div class="feature-icon">
											<i class="icon ion-ios-help-buoy"></i>
										</div>
										<h4>Diligence</h4>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-5 col-sm-12 sm-center">
							<div class="industris-space-sm"></div>
							<img src="<?php echo base_url();?>public_html/upload/content/<?php echo $pcl_img_n;?>" alt="">
						</div>
					</div>
			    </div>
			</section>

			<section class="bg-light tm-products-outer-wrap">
				<div class="container">
					<h4>OUR PRODUCTS</h4>
					<h2>What we do</h2>
					<div class="space-30" style="height: 30px"></div>
					<div class="row"> 
						<?php   
	 						foreach($submenu1 as $sub1_row){  
						?>
								<div class="col-md-4 col-sm-6">
									<a href="<?php echo base_url();?><?php echo $sub1_row['ml_slug'];?>">
										<div class="services-box services-box-img">
											<div class="services-overlay"></div>
											<div class="services-content">
												<h3><?php echo $sub1_row['ml_name'];?></h3>
												<span class="view-detail">View details<i class="icon ion-md-add-circle-outline"></i></span>
											</div>
											<img src="<?php echo base_url();?>public_html/upload/slider/<?php echo $sub1_row['ml_sub_img'];?>" alt="services image">
										</div>
									</a>
								</div>
						<?php
							}
						?>
						
					</div>
				</div>
			</section>
		</div>
	    
		