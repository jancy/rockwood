<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>

		<div id="content" class="site-content">
			
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li><a href="<?php echo base_url();?>">Products</a></li>
									<li><a href="<?php echo base_url();?><?php echo $ml_child_data['ml_slug'];?>"><?php echo $ml_child_data['ml_title'];?></a></li>
			                        <li class="active"><?php echo $ml_data['ml_title'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section class="tm-product-details">
				<div class="container">
					<div class="row tm-product-details-inner">
						<div class="col-md-4 col-sm-12">
							<div class="widget-area primary-sidebar">
								<div class="widget widget-nav-menu">
									<h4 class="widget-title"><?php echo $ml_child_data['ml_title'];?></h4>
									<div class="menu-services-menu-container">
							            <ul id="menu-services-menu" class="menu">
											<?php 
												foreach($ml_child_list as $sub2_row){
											?>
							                		<li class=" <?php if($ml_data['ml_id']==$sub2_row['ml_id']){ echo 'current-menu-item'; }?> "><a href="<?php echo base_url();?><?php echo $sub2_row['ml_slug'];?>"><?php echo $sub2_row['ml_title'];?></a></li>
											<?php
												}
											?> 
								        </ul>
								    </div>
								</div>
								<div class="widget widget-text">
									<h4 class="widget-title">Brochures</h4>
									<div class="textwidget">
							            <p>Download our complete Company Profile</p>
							            <a href="https://rockwood.mizagotechnology.com/images/company-profile-_RockWood.pdf" target="_blank">Download Brochures PDF<i class="icon ion-md-archive"></i></a>
							        </div>
								</div>
								<div class="widget widget-form">
									<h4 class="widget-title">Get a quote</h4>
									<form id="getaquote" class="form-getaquote" action="" method="post">
							            <input type="text" name="subject" id="subject" class="form-control" placeholder="Subject" required>
							            <input type="text" name="fname" id="fname" class="form-control" placeholder="First name" required>
							            <input type="text" name="lname" id="lname" class="form-control" placeholder="Last name" required>
							            <input type="text" name="company" id="company" class="form-control" placeholder="Company name" required>
							            <input type="submit" value="Get a quote" class="wpcf7-form-control wpcf7-submit btn btn-primary">
						            </form>
								</div>
							</div>
						</div>
						<div class="col-md-8 col-sm-12 tm-product-right-wrap">
							<div class="project-slider" data-show="1" data-arrow="true">
								<?php 
									foreach($sl_list as $sl_row){
								?>
									<div>
										<div class="project-box">
											<img src="<?php echo base_url();?>public_html/upload/slider/<?php echo $sl_row['sl_img'];?>" alt="" class="">										
										</div>
									</div>
								<?php
									}
								?> 
								  
							</div>
							<h4 class="text-primary">OVERVIEW</h4>
							<?php 
								foreach($pcl_list as $pcl_row){
							?>
							        <h2><?php echo $pcl_row['pcl_title'];?></h2>
									<div><?php echo $pcl_row['pcl_content'];?></div>
							<?php
								}
							?> 
							<?php 
								foreach($gtl_list as $gtl_row){
							?>
							        <h3><?php echo $gtl_row['gtl_title'];?></h3>

									<?php 
										foreach($gl_list as $gl_row){
											if($gtl_row['gtl_id']==$gl_row['gl_gtl_id']){
									?>
												<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $gl_row['gl_img'];?>" alt="">

									<?php
											}
										}
									?> 	
							<?php
								}
							?> 		
							 
							<hr>
							<div class="industris-space"></div>

							<!-- <h4 class="text-primary">fEATURED PROJECT</h4> -->
							<h3>Check out our wide range of <a href="#">Pipe notching machines in UAE</a></h3>

							<div class="project-feature-slider" data-show="2" data-arrow="true">

								<?php 
									foreach($vgl_list as $vgl_row){
								?>
									 	<div>
											<div class="project-feature-box">
												<iframe width="560" height="315" src="<?php echo $vgl_row['vgl_url'];?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
											</div>
										</div>
								<?php
									}
								?> 		
								

							</div>
						</div>
					</div>
			    </div>
			</section>

		</div>
		