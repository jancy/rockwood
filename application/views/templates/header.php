<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="description" content="<?php if(isset($ml_data['ml_desc'])){ echo $ml_data['ml_desc'];}?>">
  	<meta name="keywords" content="<?php if(isset($ml_data['ml_keyword'])){ echo $ml_data['ml_keyword'];}?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
	<title><?php if(isset($ml_data['ml_m_title'])){ echo $ml_data['ml_m_title'];}?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
	<link rel="stylesheet" id="bootstrap-css" href="<?php echo base_url();?>public_html/site_assets/css/bootstrap.css" type="text/css" media="all">
	<link rel="stylesheet" id="awesome-font-css" href="<?php echo base_url();?>public_html/site_assets/css/font-awesome.css" type="text/css" media="all">
	<link rel="stylesheet" id="ionicon-font-css" href="<?php echo base_url();?>public_html/site_assets/css/ionicon.css" type="text/css" media="all">
	<link rel="stylesheet" id="slick-slider-css" href="<?php echo base_url();?>public_html/site_assets/css/slick.css" type="text/css" media="all">
	<link rel="stylesheet" id="slick-theme-css" href="<?php echo base_url();?>public_html/site_assets/css/slick-theme.css" type="text/css" media="all">
	<link rel="stylesheet" id="magnific-popup-css" href="<?php echo base_url();?>public_html/site_assets/css/magnific-popup.css" type="text/css" media="all">
	<link rel="stylesheet" id="industris-style-css" href="<?php echo base_url();?>public_html/site_assets/style.css" type="text/css" media="all">

	<link rel="shortcut icon" href="<?php echo base_url();?>public_html/site_assets/images/favicon.png">
</head>

<body>
	<div id="page" class="site">
		<div class="collapse searchbar" id="searchbar">
	        <div class="search-area">
	            <div class="container">
	                <div class="row">
	                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
	                        <div class="input-group">
	                            <input type="text" class="form-control" placeholder="Search for...">
	                            <span class="input-group-btn">
	            					<button class="btn btn-primary" type="button">Go!</button>
	            				</span> 
	            			</div>
	                        <!-- /input-group -->
	                    </div>
	                    <!-- /.col-lg-6 -->
	                </div>
	            </div>
	        </div>
	    </div>
	    <header id="site-header" class="site-header mobile-header-blue header-style-1">
	        <div id="header_topbar" class="header-topbar clearfix">
	            <div class="container-custom">
	                <div class="row">
	                    <div class="col-md-8">
	                        <!-- Info on Topbar -->
	                        <ul class="topbar-left">
	                            <li><i class="icon ion-md-call"></i><a href="tel:+971 4 8816636">+971 4 8816636</a></li>
	                            <li><i class="icon ion-md-mail"></i><a href="mailto:sales@rockwoodmachinery.com"> sales@rockwoodmachinery.com</a></li>
	                        </ul>
	                    </div>
                        <!-- Info on topbar close -->
	                    <div class="col-md-4">
	                    	<ul class="topbar-right pull-right">
		                        <li><a target="_blank" href="https://www.facebook.com/rockwoodinternational/" rel="noopener noreferrer"><i class="icon ion-logo-facebook"></i></a></li>
	                            <li><a target="_blank" href="https://www.instagram.com/rockwoodinternational/" rel="noopener noreferrer"><i class="icon ion-logo-instagram"></i></a></li>
	                            <li><a target="_blank" href="https://www.linkedin.com/company/rockwood-international-llc/" rel="noopener noreferrer"><i class="icon ion-logo-linkedin"></i></a></li>
	                        </ul>
	                    </div>
	                </div>
	            </div>
	        </div>
	        <!-- Top bar close -->
	        <div class="main-header md-hidden sm-hidden">
	        	<div class="container-custom">
	        		<div class="row">
	        			<div class="col-md-3">
			        		<div class="logo-brand">
			        			<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>public_html/site_assets/images/Rw-logo.png" alt="industris"></a>
			        		</div>
			        	</div>
			        	<div class="col-md-9 tm-menu-wrap">
			        		<div class="main-navigation">
			        			<ul id="primary-menu" class="menu">
			        				<?php  
										foreach($ml_list as $ml_key=>$ml_row){ 
											$submenu1 	= $this->public_model->get_menu_details($ml_row['ml_id'],'','1'); 
											$z_val1 = sizeof($submenu1);
									?>
											<li class="menu-item <?php if($z_val1!=0){ echo'menu-item-has-children';}?>"><a href="<?php echo base_url();?><?php echo $ml_row['ml_slug'];?>"><?php echo $ml_row['ml_name'];?> </a>
												<?php
													if($z_val1!=0){
												?>
														<ul class="sub-menu">
															<?php  
																foreach($submenu1 as $sub1_key=>$sub1_row){
																	$submenu2 	= $this->public_model->get_menu_details($ml_row['ml_id'],$sub1_row['ml_id'],'2'); 
																	$z_val2 = sizeof($submenu2 );
																	$url_sub = '';
																	

															?>

																	<li class="menu-item <?php if($z_val2!=0){ echo'menu-item-has-children';}?> "><a href="<?php echo base_url();?><?php if($sub1_row['ml_type']=="productsub"){echo "products/";} echo $sub1_row['ml_slug'];?>"><?php echo $sub1_row['ml_name'];?> </a>
																		<ul class="sub-menu">
																			<?php                                            
																				foreach($submenu2 as $sub2_key=>$sub2_row){
																			?>

																					<li class="menu-item"><a href="<?php echo base_url();?><?php if($sub2_row['ml_type']=="productdetails"){echo "products/".$sub1_row['ml_slug']."/";} echo $sub2_row['ml_slug'];?> "><?php echo $sub2_row['ml_name'];?> </a></li>
																			<?php        
																				}  		 
																				
																			?>
																		</ul>
																	</li>
															<?php        
																}  		 
																
															?>
														</ul>
												<?php
													}
												?>
											</li>   
									<?php  
										} 	
									?>
								</ul>
								<ul class="topbar-right pull-right">
									<li class="toggle_search topbar-search"><a href="#"><i class="icon ion-md-search"></i></a></li>																		
								</ul>
		        				<!-- <a href="#" class="btn btn-primary">Get a quote<i class="icon ion-md-paper-plane"></i></a> -->
			        		</div>
			        	</div>
		        	</div>
	        	</div>
	        </div>
	        <!-- Main header start -->
	        
	        <!-- Mobile header start -->
	        <div class="mobile-header">
	        	<div class="container-custom">
	        		<div class="row">
	        			<div class="col-xs-6">
			        		<div class="logo-brand-mobile">
			        			<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>public_html/site_assets/images/Rw-logo.png" alt="industris"></a>
			        		</div>
			        	</div>
	        			<div class="col-xs-6">
			        		<div id="mmenu_toggle">
			                    <button></button>
			                </div>												
			        	</div>
			        	<div class="col-xs-12">
			        		<div class="mobile-nav">
								<ul id="primary-menu-mobile" class="mobile-menu">
								<?php  
										foreach($ml_list as $ml_key=>$ml_row){ 
											$submenu1 	= $this->public_model->get_menu_details($ml_row['ml_id'],'','1'); 
											$z_val1 = sizeof($submenu1);
									?>
											<li class="menu-item <?php if($z_val1!=0){ echo'menu-item-has-children';}?>"><a href="<?php echo base_url();?><?php echo $ml_row['ml_slug'];?>"><?php echo $ml_row['ml_name'];?> </a>
												<?php 
													if($z_val1!=0){ 
												?>
														<ul class="sub-menu">
															<?php  
																foreach($submenu1 as $sub1_key=>$sub1_row){
																	$submenu2 	= $this->public_model->get_menu_details($ml_row['ml_id'],$sub1_row['ml_id'],'2'); 
																	$z_val2 = sizeof($submenu2);

															?>

																	<li class="menu-item <?php if($z_val2!=0){ echo'menu-item-has-children';}?> "><a href="<?php echo base_url();?><?php echo $sub1_row['ml_slug'];?> "><?php echo $sub1_row['ml_name'];?> </a>
																		<?php 
																			if($z_val2!=0){  
																		?>
																				<ul class="sub-menu">
																					<?php                                            
																						foreach($submenu2 as $sub2_key=>$sub2_row){
																					?>

																							<li class="menu-item"><a href="<?php echo base_url();?><?php echo $sub2_row['ml_slug'];?> "><?php echo $sub2_row['ml_name'];?> </a></li>
																					<?php        
																						}  		 
																						
																					?>
																				</ul>
																		<?php
																			} 
																		?>
																	</li>
															<?php        
																}  		 
																
															?>
														</ul>
												<?php
													}
												?>
											</li>   
									<?php  
										} 	
									?>
			        			</ul>			        			
		        				<!-- <a href="#" class="btn btn-primary">Get a quote<i class="icon ion-md-paper-plane"></i></a> -->
			        		</div>
			        	</div>
		        	</div>
	        	</div>
	        </div>
	    </header>
        <!-- Mobile header start -->

		

