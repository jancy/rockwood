<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>

		<div id="content" class="site-content">
			<?php 
				$submenu2 	= $this->public_model->get_menu_details($ml_data['ml_parent_ml_id'],$ml_data['ml_id'],'2');
		
			?>

			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li><a href="<?php echo base_url();?><?php echo $ml_parent_data['ml_slug'];?>"><?php echo $ml_parent_data['ml_name'];?></a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section class="tm-product-sub">
				<div class="container">
					<div class="row">
						<div class="col-md-5">
							<div class="section-img-btn">
								<a href="<?php echo base_url();?>contact-us" class="btn btn-primary">Contact Us</a>
							</div>
						</div>
						<div class="col-md-7">
							<div class="industris-space"></div>
							<h4 class="text-primary">SMART MACHINES</h4>
							<h2>Find the perfect machine to suit your needs</h2>
							<p>We lead the market in offering a qualitative assortment of Machines that will help you achieve every one of your short and long-term business goals.</p>
							<p>Rockwood ensures that every piece of equipment we source out to industries in UAE and neighboring counties is on par with the global standards and advanced technology in compliance with universal norms. We provide equipment for a myriad of purposes and technical specifications.</p>							
 						</div>
					</div>
				</div>
			</section>

			<section class="tm-products-wrap" style="background-color: #ff8400;">
				<div class="container">
					<div class="row">
					<?php 
						foreach($submenu2 as $sub2_row){
					?>						
							<div class="col-md-3 col-sm-6">
								<a href="<?php echo base_url();?><?php echo $sub2_row['ml_slug'];?>">
									<div class="services-box-3">
										<div class="services-content">
											<h4><?php echo $sub2_row['ml_title'];?></h4>
											<span class="view-detail"><i class="no-margin-left icon ion-md-add-circle-outline"></i></span>
										</div>								
									</div>
								</a>
							</div>
					<?php
						}
					?>
											
					</div>
				</div>
			</section>

		</div>
		