<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>
		<div id="content" class="site-content">
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $ml_data['ml_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>index">Home</a></li>
									<li class="active"><?php echo $ml_data['ml_name'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<section class="bg-light tm-projects-wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4 class="text-primary">FEATURED PROJECTS</h4>
							<h2>The great work we did</h2>
							<div class="industris-space"></div>

							<div class="project-slider" data-show="1" data-arrow="true">
								<?php  
									$ps = 0;
									foreach($pro_slider as $pro_s_row){ 
										if($ps!='3'){
								?>
			 								<div>
												<div class="project-box">
													<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $pro_s_row['gl_img'];?>" alt="" class="">
													<div class="col-md-offset-6 col-md-5 col-xs-offset-4 col-xs-8">
														<div class="project-slider-content">
															<div class="project-meta"><?php echo $pro_s_row['gtl_desc'];?></div>
															<h3><a href="<?php echo base_url();?><?php echo $ml_slug_1;?>/<?php echo str_replace(' ','-',$pro_s_row['gtl_title']);?>"><?php echo $pro_s_row['gtl_title'];?></a></h3>
															<a class="btn-link" href="<?php echo base_url();?><?php echo $ml_slug_1;?>/<?php echo str_replace(' ','-',$pro_s_row['gtl_title']);?>">View details<i class="icon ion-md-add-circle"></i></a>
														</div>
													</div>
												</div>
											</div>
								<?php 
											$ps=$ps+1;
										}
									}
								?>
								 

							</div>

						</div>
					</div>
			    </div>
			</section>

			<section class="tm-project-list">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h4 class="text-primary">EXPLORE WORK</h4>
							<h2>Other projects</h2>
							<br>
						</div>
						<div class="col-md-12">
							<div class="project-filter" data-column="">

								<div id="filters" class="cat-filter">
									<a href="#" data-filter="*" class="filter-item all-cat selected">All projects</a>
									<?php   
										foreach($pro_cat_list as $pro_c_row1){  
									?>
											<a href="#" data-filter=".<?php echo $pro_c_row1['ml_id'];?>_cls" class="filter-item "><?php echo $pro_c_row1['ml_title'];?></a> 
									<?php
										}
									?>
								</div>

								<div id="projects" class="project-grid projects row ">
									<?php   
										foreach($pro_slider as $pro_s_row1){  
									?>
											<div class="project-item <?php echo $pro_s_row1['ml_id'];?>_cls col-lg-4 col-sm-6">
												<div class="inner">
													<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $pro_s_row1['gtl_i_img'];?>" alt=""> 
													<div class="project-info">
														<!-- <div class="project-meta">Dubai, UAE</div> -->
														<div class="project-content">
															<h3><a href="<?php echo base_url();?><?php echo $ml_slug_1;?>/<?php echo str_replace(' ','-',$pro_s_row1['gtl_title']);?>"><?php echo $pro_s_row1['gtl_title'];?></a></h3>
															<p><?php echo $pro_s_row1['gtl_desc'];?></p>
															<a href="<?php echo base_url();?><?php echo $ml_slug_1;?>/<?php echo str_replace(' ','-',$pro_s_row1['gtl_title']);?>">View details <i class="icon ion-md-add-circle"></i></a>
														</div>
													</div>
												</div>
											</div>
									<?php
										}
									?>
									
									
									

								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

		</div>
	    

	