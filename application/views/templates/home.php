	<div class="slider tm-banner-slider" data-show="1" data-arrow="true">
			<div>
				<div class="slider-item">
					<img src="<?php echo base_url();?>public_html/site_assets/images/banner (2).jpg" alt="" class="">
					<div class="container">
						<div class="row">
							<div class="col-md-10">
								<div class="slider-content">									
									<h1><span class="text-primary">Best Industrial Steel fabricators </span>in UAE</h1>
									<p>We provide a range of state-of-the-art heavy machines and industrial equipment for effectively and timely management operation and on-time completion of projects.</p>
									<a class="btn btn-border" href="<?php echo base_url();?>about-us">Know more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div>
				<div class="slider-item">
					<img src="<?php echo base_url();?>public_html/site_assets/images/banner.jpg" alt="" class="">
					<div class="container">
						<div class="row">
							<div class="col-md-10">
								<div class="slider-content">
									<!-- <h4>WELCOME TO INDUSTRIS...!</h4> -->
									<h2><span class="text-primary">WELD IT <br>RIGHT WITH</span> ROCKWOOD</h2>
									<p>We are committed to delivering superior quality industrial solutions and services supported by the modern and advanced technologies.</p>
									<a class="btn btn-border" href="<?php echo base_url();?>about-us">Know more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div>
				<div class="slider-item">
					<img src="<?php echo base_url();?>public_html/site_assets/images/banner (1).jpg" alt="" class="">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="slider-content">
									<h2><span class="text-primary">Leader in Commercial and industrial
									</span> ENGINEERING</h2>
									<p>We help you go beyond conventional exporting techniques with the right kind of labelling and engravings</p>
									<a class="btn btn-border" href="<?php echo base_url();?>about-us">Know more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<!-- <div class="slider tm-banner-slider" data-show="1" data-arrow="true">
	<?php 
		foreach($sl_list as $sl_row){
	?>
			<div>				
				<div class="slider-item">
					<img src="<?php echo base_url();?>public_html/upload/slider/<?php echo $sl_row['sl_img'];?>" alt="" class="">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<div class="slider-content">
									<?php echo $sl_row['sl_content'];?>
									<a class="btn btn-border" href="<?php echo base_url();?>about-us">Know more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	<?php
		}
	?> 
</div> -->

		<section class="no-padding-bottom tm-products">
			<div class="container">
				<div class="row">
					<div class="col-md-12">						
						<h4 class="text-primary"><div class="title-effect"><div class="bar bar-top"></div>
							<div class="bar bar-right"></div>
							<div class="bar bar-bottom"></div>
							<div class="bar bar-left"></div></div>Products</h4>
						<h2>EXPLORE OUR RANGE OF<br>STEEL FABRICATION & MACHINERY IN UAE</h2>
						<p><a href="<?php echo base_url();?>products">Click here to view our complete product range</a></p>
						<div class="industris-space-30"></div>
						<div class="col-md-4 col-sm-6 tm-product-list">
							<div class="services-box">
								<div class="services-icon">
									<img src="<?php echo base_url();?>public_html/site_assets/images/icons (6).png" alt="">
								</div>
								<div class="services-content">
									<h3><a href="<?php echo base_url();?>products">STUD WELDING</a></h3>
									<p>Insulation pin welding machines in UAE is a powerful and...</p>
									<a class="btn" href="<?php echo base_url();?>products">know more</a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 tm-product-list">
							<div class="services-box">
								<div class="services-icon">
									<img src="<?php echo base_url();?>public_html/site_assets/images/icons (5).png" alt="">
								</div>
								<div class="services-content">
									<h3><a href="<?php echo base_url();?>products">BENDING</a></h3>
									<p>Compact machine with amazing performance, designed, manu...</p>
									<a class="btn" href="<?php echo base_url();?>products">know more</a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 sm-clear tm-product-list">
							<div class="services-box">
								<div class="services-icon">
									<img src="<?php echo base_url();?>public_html/site_assets/images/icons (4).png" alt="">
								</div>
								<div class="services-content">
									<h3><a href="<?php echo base_url();?>products">COLUMN & BOOM</a></h3>
									<p>Select from our cost-effective and superior quality Welding...</p>
									<a class="btn" href="<?php echo base_url();?>products">know more</a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 md-clear tm-product-list">
							<div class="services-box no-margin-md-bottom">
								<div class="services-icon">
									<img src="<?php echo base_url();?>public_html/site_assets/images/icons (3).png" alt="">
								</div>
								<div class="services-content">
									<h3><a href="<?php echo base_url();?>products">INSULATION PINS</a></h3>
									<p>Insulation Rockwool pins and washers in UAE and Insulation...</p>
									<a class="btn" href="<?php echo base_url();?>products">know more</a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 sm-clear tm-product-list">
							<div class="services-box xs-no-margin-bottom">
								<div class="services-icon">
									<img src="<?php echo base_url();?>public_html/site_assets/images/icons (2).png" alt="">
								</div>
								<div class="services-content">
									<h3><a href="<?php echo base_url();?>products">LASER ENGRAVING</a></h3>
									<p>Fiber laser marking machines can work with most metal...</p>
									<a class="btn" href="<?php echo base_url();?>products">know more</a>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-6 tm-product-list">
							<div class="services-box no-margin-bottom">
								<div class="services-icon">
									<img src="<?php echo base_url();?>public_html/site_assets/images/icons (1).png" alt="">
								</div>
								<div class="services-content">
									<h3><a href="<?php echo base_url();?>products">ROAD PILE</a></h3>
									<p>We produce a complete range of pile-driving machines such as...</p>
									<a class="btn" href="<?php echo base_url();?>products">know more</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="industris-space-90"></div>
		</section>

		<section class="bg-primary no-padding tm-about" style="background-image:url('public_html/site_assets/images/bg.jpg');">
			<div class="">
				<div class="flex-row">
					<video autoplay muted loop id="myVideo">
						<source src="<?php echo base_url();?>public_html/site_assets/images/video-new.mp4" type="video/mp4">
					</video>
					<div class="video-section-right">
						<div class="block-right">
							<!-- <h4>OUR STORY</h4> -->
							<h2>ABOUT ROCKWOOD MACHINERY</h2>
							<p>Rockwood Machinery is one of the best heavy machinery and industrial equipment providers headquartered in the UAE. We operate alongside our suppliers and employees to help manufacture and build metal structures and systems on a large scale with our experienced team of welders, aluminum fabrication experts, and installation crew.</p>
							<p>Rockwood international has adopted the path of hardwork and discipline to ensure sustained growth. Backed by our focus on quality, this approach has generated remarkable dividends for the company. This is also reflected in the healthy rate of growth recorded by us.</p>							
						</div>
					</div>
					<div class="video-section-left">
						<div class="wrapper-div">
							<a class="btn" href="<?php echo base_url();?>about-us">Our History</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="no-padding-bottom tm-expertize">
			<div class="container">
				<div class="row">
					<h2 class="text-primary">OUR Areas of expertize</h2>
					<div class="industris-space-30"></div>
					<div class="row">
						<div class="col-md-12">
							
							<div class="project-feature-slider tm-expertize-slider project-feature" data-show="4" data-arrow="true">
	
								<div class="project-item col-lg-4 col-sm-6">
									<div class="inner">
										<img src="<?php echo base_url();?>public_html/site_assets/images/technology.png" alt=""> 
										<div class="project-info">
											<div class="project-content">
												<img src="<?php echo base_url();?>public_html/site_assets/images/icon-02.png" alt="img">
												<h4>Advanced Technologies</h4>
												<p>We use only up-to-date & efficient machines</p>
											</div>
										</div>
									</div>
								</div>
	
								<div class="project-item col-lg-4 col-sm-6">
									<div class="inner">
										<img src="<?php echo base_url();?>public_html/site_assets/images/clients.png" alt=""> 
										<div class="project-info">
											<div class="project-content">
												<img src="<?php echo base_url();?>public_html/site_assets/images/icon-01.png" alt="img">
												<h4>1000+ Satisfied Clients</h4>
												<p>Know our happy customers and the list if growing</p>												
											</div>
										</div>
									</div>
								</div>
	
				 				<div class="project-item col-lg-4 col-sm-6">
									<div class="inner">
										<img src="<?php echo base_url();?>public_html/site_assets/images/trusted-partner.png" alt=""> 
										<div class="project-info">											
											<div class="project-content">
												<img src="<?php echo base_url();?>public_html/site_assets/images/icon-03.png" alt="img">
												<h4>Trusted Q&A Partner</h4>
												<p>Quality embedded services, Always</p>		
											</div>
										</div>
									</div>
								</div>
	
								<div class="project-item col-lg-4 col-sm-6">
									<div class="inner">
										<img src="<?php echo base_url();?>public_html/site_assets/images/experts.png" alt=""> 
										<div class="project-info">
											<div class="project-content">
												<img src="<?php echo base_url();?>public_html/site_assets/images/icon-04.png" alt="img">
												<h4>Experts With Experience</h4>
												<p>Expert care from experienced hands</p>												
											</div>
										</div>
									</div>
								</div>								
	
								<div class="project-item col-lg-4 col-sm-6">
									<div class="inner">
										<img src="<?php echo base_url();?>public_html/site_assets/images/excellence.png" alt=""> 
										<div class="project-info">
											<div class="project-content">
												<img src="<?php echo base_url();?>public_html/site_assets/images/icon-05.png" alt="img">
												<h4>Par Excellence Service</h4>
												<p>Driven by the passion and hard work</p>	
											</div>
										</div>
									</div>
								</div>
								<div class="project-item col-lg-4 col-sm-6">
									<div class="inner">
										<img src="<?php echo base_url();?>public_html/site_assets/images/customer.png" alt=""> 
										<div class="project-info">											
											<div class="project-content">
												<img src="<?php echo base_url();?>public_html/site_assets/images/icon-06.png" alt="img">
												<h4>Customer-First Approach</h4>
												<p>Your needs and requirements, our priority</p>	
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="industris-space-90"></div>
						</div>
					</div>
				
				</div>
			</div>
		</section>
		<section class="tm-projects">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2 class="text-primary">OUR PROJECTS</h2>
						<div class="industris-space-30"></div>
						<!-- <div class="services-block-left">
							<div class="services-slider-img-left">
								<img src="<?php echo base_url();?>public_html/site_assets/images/services-solution.png" alt="">
							</div>
						</div> -->

						<div class="services-slider" data-show="3" data-arrow="true">
							<?php 
								$pjt = 0;
								foreach($pro_list as $proj_row){
									if($pjt!=5){
							?>
										<div class="services-item">
											<div class="services-box">
												<a href="<?php echo base_url();?>projects/<?php echo str_replace(' ','-',$proj_row['gtl_title']);?>">
												<div class="services-icon">
													<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $proj_row['gl_img'];?>" alt="">
												</div></a>
												<div class="services-content">
													<h3><a href="<?php echo base_url();?>projects/<?php echo str_replace(' ','-',$proj_row['gtl_title']);?>"><?php echo $proj_row['gtl_title'];?></a></h3>
													<!-- <h5 class="text-primary">Dubai, UAE</h5> -->
													<p><?php $gtl_content =  strip_tags($proj_row['gtl_content']); echo substr($gtl_content,0,170);?>...</p>
													<a class="btn btn-border" href="<?php echo base_url();?>projects/<?php echo str_replace(' ','-',$proj_row['gtl_title']);?>">Read more</a>
												</div>
											</div>
										</div>
							<?php
										$pjt = $pjt+1; 
									}
								}
							?>
							

						</div>

					</div>
				</div>
			</div>
			<div class="industris-space-90"></div>
		</section>
		<section class="bg-primary no-padding tm-solutions">
			<div class="">
				<div class="flex-row">					
					<div class="video-section-right">
						<div class="block-right">							
							<h2>High-Quality WELDING AND STEEL FABRICATION SERVICES in UAE</h2>
							<p>If you need an industrial metal and machinery project to be delivered on time and with ease, you have come to the right place. Rockwood Machinery got you covered from even small Fabrication works to Large Structural Manufacturing projects. Our expert team is fully trained and ready to carry your project, no matter what size, from start to finish with precision and transform your needs, ideas and vision into a structural reality.</p>
							<!-- <ul class="icon-box">
								<li>Value Added Service</li>								
								<li>Competitive Price</li>
								<li>Same Day Response</li>
								<li class="tm-count"><span class="number" data-to="95" data-speed="2000">0</span><span>%</span></li>
								<li class="tm-count"><span class="number" data-to="93" data-speed="2000">0</span><span>%</span></li>								
								<li class="tm-count"><span class="number" data-to="91" data-speed="2000">0</span><span>%</span></li>
							</ul> -->
							<a class="btn btn-border" href="<?php echo base_url();?>contact-us">Send enquiry</a>
						</div>
					</div>
					<div class="video-section-left" style="background-image:url('public_html/site_assets/images/solution-bg.jpg');">						
					</div>
				</div>
			</div>
		</section>
		<section class="padding-bottom-medium tm-blog">
			<div class="container">
				<div class="row flex-row">

					<div class="col-md-6">
						<h4><div class="title-effect"><div class="bar bar-top"></div>
							<div class="bar bar-right"></div>
							<div class="bar bar-bottom"></div>
							<div class="bar bar-left"></div></div>BLOG</h4>
						<h2 class="no-margin-bottom text-primary">LATEST FROM BLOGS</h2>
					</div>
					<div class="col-md-6 text-right align-self-end">
						<a class="btn btn-border tm-more-btn" href="<?php echo base_url();?>blog">Know More</a>
					</div>
				</div>
				<div class="industris-space-50"></div>

				<div class="services-slider" data-show="3" data-arrow="true">

					<?php 
						$pcll = 0;
						foreach($bo_list as $bol_row1){
							if($pcll!=5){
					?>
								<div class="services-item">
									<div class="services-box">
										<div class="services-icon">
											<img src="<?php echo base_url();?>public_html/upload/content/<?php echo $bol_row1['pcl_img'];?>" alt="">
										</div>
										<div class="services-content">
											<h5><?php echo $bol_row1['pcl_sub_title'];?></h5>
											<h3><a href="<?php echo base_url();?>post/<?php echo str_replace(' ','-',$bol_row1['pcl_title']);?>"><?php echo $bol_row1['pcl_title'];?>...</a></h3>								
											<p><?php $pcl_content =  strip_tags($bol_row1['pcl_content']); echo substr($pcl_content,0,170);?>...</p>
											<a class="btn btn-border" href="<?php echo base_url();?>post/<?php echo str_replace(' ','-',$bol_row1['pcl_title']);?>">Read more</a>
										</div>
									</div>
								</div>
					<?php
								$pcll = $pcll+1;
							}
						}
					?>					
				</div>
			</div>
		</section>
		<section class="bg-primary tm-newsletter">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h4><div class="title-effect"><div class="bar bar-top"></div>
							<div class="bar bar-right"></div>
							<div class="bar bar-bottom"></div>
							<div class="bar bar-left"></div></div>NEWSLETTER</h4>
						<h2>SUBSCRIBE FOR NEWSLETTER</h2>
						<form class="coming-soon-form">
							<div class="row">
								<div class="col-sm-8 tm-email">
									<div class="form-group">
										<input type="email" name="email" id="email" class="form-control" placeholder="Your Email Here" required>
									</div>
								</div>		
								<p>Sign up to get latest news and updates</p>						
								<button type="submit" class="btn btn-primary">Subscribe</button>								
							</div>
						</form>
					</div>
				</div>
			</div>
		</section>
		<section class="tm-clients">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>OUR CLIENTS</h2>
                        <div class="partner-slider image-carousel text-center" data-show="5" data-arrow="false">

							<?php 
								foreach($client as $cil_row){
							?>
						
									<div>
										<div class="partner-item text-center clearfix">
											<div class="inner">
												<div class="thumb">
													<img src="<?php echo base_url();?>public_html/upload/gallery_img/<?php echo $cil_row['gl_img'];?>" alt="">
												</div>
											</div>
										</div>
									</div>
							<?php
								}
							?>
														
                        </div>
					</div>
				</div>
			</div>			
		</section>
		