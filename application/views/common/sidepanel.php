<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Rock Wood  </title>
		<!-- Google Font: Source Sans Pro -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
		<!-- Font Awesome Icons -->
		<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/plugins/fontawesome-free/css/all.min.css">
		<!-- overlayScrollbars -->
		<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
		<!-- DataTables -->
		<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
		<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
		<!-- Theme style -->
		<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/dist/css/adminlte.min.css">
		<link href='<?php echo base_url();?>public_html/admin_assets/added_plugins/jquery-sortable-master/source/css/vendor.css' rel='stylesheet'>
		<link href='<?php echo base_url();?>public_html/admin_assets/added_plugins/jquery-sortable-master/source/css/application.css' rel='stylesheet'>
		<link href='<?php echo base_url();?>public_html/admin_assets/added_plugins/sortable/jquery-ui.css' rel='stylesheet'>

		<!-- jQuery -->
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/jquery/jquery.min.js"></script>
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/sortable/jquery-ui.js"></script>
		<!-- Bootstrap -->
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
		
		<!-- file upload pulgin -->
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/vendor/jquery.ui.widget.js"></script> 
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/jquery.iframe-transport.js"></script> 
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/jquery.fileupload.js"></script>

		<!-- toastr pulgin -->			
		<link href="<?php echo base_url();?>public_html/admin_assets/added_plugins/toastr/build/toastr.min.css" rel="stylesheet">
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/toastr/build/toastr.min.js"></script>


		<!-- summernote -->
		<!-- <link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/plugins/summernote/summernote-bs4.min.css">
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/summernote/summernote-bs4.min.js"></script> -->
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/ckeditor5-master/ckeditor.js"></script>  <!-- https://ckeditor.com/ckeditor-5/demo/#document -->

		
		<script src="https://cdn.ckeditor.com/ckeditor5/34.0.0/classic/ckeditor.js"></script>
		<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/jquery-sortable-master/source/JS/jquery-sortable.js"></script>



		<style>
			.menu_p_cls{
				border: 1px solid #FFAB50;
				padding: 5px;
				text-align: center;
				width: 204px;
				background: #D13C30;
				color: #fff;

			}
			li{
				color:#fff;
			}
		</style>

		<style>
			.note-editor.note-frame .note-editing-area .note-editable {
				height : 200px;
			}
		</style>
		
	</head>
	<body class="hold-transition  sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
		<div class="wrapper">
			<!-- Preloader -->
			<div class="preloader flex-column justify-content-center align-items-center">
				<img class="animation__wobble" src="<?php echo base_url();?>public_html/admin_assets/dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
			</div>
			<!-- Navbar -->
			<nav class="main-header navbar navbar-expand navbar-dark">
				<!-- Left navbar links -->
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
					</li>
					<!-- <li class="nav-item d-none d-sm-inline-block">
						<a href="<?php echo base_url();?>admin_login_ctrl/index" class="nav-link">Home</a>
					</li>
					<li class="nav-item d-none d-sm-inline-block">
						<a href="#" class="nav-link">Contact</a>
					</li> -->
				</ul>
				<!-- Right navbar links -->
				<ul class="navbar-nav ml-auto">
					<!-- Navbar Search -->
					<li class="nav-item">
						<a class="nav-link" data-widget="navbar-search" href="#" role="button">
						<i class="fas fa-search"></i>
						</a>
						<div class="navbar-search-block">
							<form class="form-inline">
								<div class="input-group input-group-sm">
									<input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
									<div class="input-group-append">
										<button class="btn btn-navbar" type="submit">
										<i class="fas fa-search"></i>
										</button>
										<button class="btn btn-navbar" type="button" data-widget="navbar-search">
										<i class="fas fa-times"></i>
										</button>
									</div>
								</div>
							</form>
						</div>
					</li>
					 
					<li class="nav-item">
						<a class="nav-link" data-widget="fullscreen" href="#" role="button">
						<i class="fas fa-expand-arrows-alt"></i>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url();?>admin_login_ctrl/logout" role="button">
						<i class="fas fa-sign-out-alt"></i>
						</a>
					</li>
					<!-- <li class="nav-item">
						<a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
						<i class="fas fa-th-large"></i>
						</a>
					</li> -->
				</ul>
			</nav>
			<!-- /.navbar -->
			<!-- Main Sidebar Container -->
			<aside class="main-sidebar sidebar-dark-primary elevation-4">
				<!-- Brand Logo -->
				<a href="<?php echo base_url();?>admin_login_ctrl/index" class="brand-link">
				<img src="<?php echo base_url();?>public_html/admin_assets/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
				<span class="brand-text font-weight-light">Rock Wood</span>
				</a>
				<!-- Sidebar -->
				<div class="sidebar">
					<!-- Sidebar user panel (optional) -->
					<!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
						<div class="image">
							<img src="<?php echo base_url();?>public_html/admin_assets/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
						</div>
						<div class="info">
							<a href="#" class="d-block">Alexander Pierce</a>
						</div>
					</div> -->
					<!-- SidebarSearch Form -->
					<div class="form-inline">
						<div class="input-group" data-widget="sidebar-search">
							<input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
							<div class="input-group-append">
								<button class="btn btn-sidebar">
								<i class="fas fa-search fa-fw"></i>
								</button>
							</div>
						</div>
					</div>
					<!-- Sidebar Menu -->
					<nav class="mt-2">
						<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
							<!-- Add icons to the links using the .nav-icon class
								with font-awesome or any other icon font library -->
							 
							<li class="nav-item">
								<a href="<?php echo base_url();?>admin_login_ctrl/index" class="nav-link">
									<i class="nav-icon fas fa-th"></i>
									<p>
									Dashboard 
									</p>
								</a>
							</li> 
							<li class="nav-item">
								<a href="<?php echo base_url();?>menus_list" class="nav-link">
									<i class="nav-icon fas fa-th"></i>
									<p>
										Menus List
									</p>
								</a>
							</li>
							<?php  
								foreach($ml_list as $ml_key=>$ml_row){ 
							?>
									<li class="nav-item">
										<a href="<?php echo base_url();?>page_ctrl/create_page/<?php echo $ml_row['ml_id']?>" class="nav-link">
											<i class="nav-icon fas fa-th"></i>
											<p>
												<?php echo $ml_row['ml_name'];?>
											</p>
										</a>
									</li>
							<?php
								}
							?>
							<?php  
								foreach($ml_cli_list as $ml_cli_key=>$ml_cli_row){ 
							?>
									<li class="nav-item">
										<a href="<?php echo base_url();?>page_ctrl/create_page/<?php echo $ml_cli_row['ml_id']?>" class="nav-link">
											<i class="nav-icon fas fa-th"></i>
											<p>
												<?php echo $ml_cli_row['ml_name'];?>
											</p>
										</a>
									</li>
							<?php
								}
							?>
							<?php  
								foreach($ml_cus_list as $ml_cus_key=>$ml_cus_row){ 
							?>
									<li class="nav-item">
										<a href="<?php echo base_url();?>page_ctrl/create_page/<?php echo $ml_cus_row['ml_id']?>" class="nav-link">
											<i class="nav-icon fas fa-th"></i>
											<p>
												<?php echo $ml_cus_row['ml_name'];?>
											</p>
										</a>
									</li>
							<?php
								}
							?>
							<!-- <li class="nav-item">
								<a href="<?php echo base_url();?>page_ctrl/create_page1" class="nav-link">
									<i class="nav-icon fas fa-th"></i>
									<p>
										Add Content to Pages 
									</p>
								</a>
							</li>
							<li class="nav-item">
								<a href="<?php echo base_url();?>menus_ctrl/arrange_menu_list" class="nav-link">
									<i class="nav-icon fas fa-th"></i>
									<p>
									Arrange menu 
									</p>
								</a>
							</li> -->

							 
						</ul>
					</nav>
					<!-- /.sidebar-menu -->
				</div>
				<!-- /.sidebar -->
			</aside>
			<!-- Content Wrapper. Contains page content -->
			<div class="content-wrapper">
				<!-- Content Header (Page header) -->
				<div class="content-header">
					<div class="container-fluid">
						<div class="row mb-2">
							<div class="col-sm-6">
								<h1 class="m-0"><?php if(!isset($page_name)){echo $view_name;}else{{echo $page_name;}}?></h1>
							</div>
							<!-- /.col -->
							<div class="col-sm-6">
								<ol class="breadcrumb float-sm-right">
									<li class="breadcrumb-item"><a href="#">Home</a></li>
									<li class="breadcrumb-item active"><?php echo $view_name;?></li>
								</ol>
							</div>
							<!-- /.col -->
						</div>
						<!-- /.row -->
					</div>
					<!-- /.container-fluid -->
				</div>
				<!-- /.content-header -->
				<section class="content">
      				<div class="container-fluid">
					  	<?php 
							if(isset($subview)&&isset($sdata)) 
								$this->load->view(''.$subview,$sdata);
							// else  $this->load->view('subviews/dashboard');
						?>
					</div>
					<!--/. container-fluid -->
				</section>
				<!-- /.content -->


			</div>
			<!-- /.content-wrapper -->
			<!-- Control Sidebar -->
			<aside class="control-sidebar control-sidebar-dark">
				<!-- Control sidebar content goes here -->
			</aside>
			<!-- /.control-sidebar -->
			<!-- Main Footer -->
			<footer class="main-footer">
				<strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
				All rights reserved.
				<div class="float-right d-none d-sm-inline-block">
					<b>Version</b> 3.1.0
				</div>
			</footer>
		</div>
		<!-- ./wrapper -->
		<!-- REQUIRED SCRIPTS -->
		
		
		<!-- overlayScrollbars -->
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
		<!-- AdminLTE App -->
		<script src="<?php echo base_url();?>public_html/admin_assets/dist/js/adminlte.js"></script>
		<!-- PAGE PLUGINS -->
		<!-- jQuery Mapael -->
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/raphael/raphael.min.js"></script>
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/jquery-mapael/jquery.mapael.min.js"></script>
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/jquery-mapael/maps/usa_states.min.js"></script>
		<!-- ChartJS -->
		<script src="<?php echo base_url();?>public_html/admin_assets/plugins/chart.js/Chart.min.js"></script>
		<!-- AdminLTE for demo purposes -->
		<script src="<?php echo base_url();?>public_html/admin_assets/dist/js/demo.js"></script>
		<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
		<script src="<?php echo base_url();?>public_html/admin_assets/dist/js/pages/dashboard2.js"></script>

		<script type="text/javascript">
			tos_success 	= '<?php echo $this->session->flashdata('tos_success');?>';
			tos_error 		= '<?php echo $this->session->flashdata('tos_error');?>';
			tos_warning 	= '<?php echo $this->session->flashdata('tos_warning');?>';
			if(tos_success!=''){
				toastr.success(''+tos_success+'', '')
			}
				
			if(tos_error!=''){
				toastr.error(''+tos_error+'', '')
			}
				
			if(tos_warning!=''){
				toastr.warning(''+tos_warning+'', '')
			}
		</script>
	</body>
</html>