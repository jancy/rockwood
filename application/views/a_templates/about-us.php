


<div class="col-12">
	<div class="card card-primary  card-default">
		<div class="card-header">
			<h3 class="card-title">  Our Story </h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse">
				<i class="fas fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-card-widget="remove">
				<i class="fas fa-times"></i>
				</button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/add_content_details/<?php if(isset($our_story['pcl_id'])){echo $our_story['pcl_id'];}?>">

				<div class="row">
					<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
						<input type="file" class="fileupload2 form-control" name="filename" id="avatar" style="opacity: 0;">
						<input type="hidden" class="form-control form-white pcl_img_cls filename2" name="pcl_img"  value="<?php if(isset($our_story['pcl_img'])){echo $our_story['pcl_img'];}?>"> 
						<label class="label_f_name2" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($our_story['pcl_img'])){echo $our_story['pcl_img'];}else{echo "Upload The Right Side Image";}?> </label>	
						<p class="pro_id_shop2" ></p>									
																
					</div>
					<div class="form-group col-md-4"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
						<input type="text" name="pcl_title" class="form-control pcl_title_cls" placeholder="Enter title" value="<?php if(isset($our_story['pcl_title'])){echo $our_story['pcl_title'];}?>"  required> 

						<input type="hidden" class="form-control form-white ml_id_cls" name="pcl_ml_id" value="<?php if(isset($our_story['pcl_ml_id'])){echo $our_story['pcl_ml_id'];}?>"> 
					</div>		
							
					<div class="form-group col-md-5"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
						<input type="text" name="pcl_sub_title" class="form-control pcl_stitle_cls" placeholder="Enter Years of Experience"  value="<?php if(isset($our_story['pcl_sub_title'])){echo $our_story['pcl_sub_title'];}?>" >   
					</div>

					<div class="col-md-12">
						<div class="card card-outline card-info">
							<div class="card-header">
								<h3 class="card-title">
									Content
								</h3>
							</div> 
							<div class="card-body">
								<textarea class="summernote pcl_content_cls" style="height:50px;" name="pcl_content">
									<?php if(isset($our_story['pcl_content'])){echo $our_story['pcl_content'];}else{echo "Please enter the content here ....";}?>
								</textarea>
							</div> 
						</div>
					</div>		
					
						
					 
					<div class="form-group col-md-12 "> 
					
						<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
					</div>
				</div>
			</form>
		</div>
		
		
	</div>
</div>

<div class="col-12">
	<div class="card card-primary  card-default">
		<div class="card-header">
			<h3 class="card-title">  Our Values </h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse">
				<i class="fas fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-card-widget="remove">
				<i class="fas fa-times"></i>
				</button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/add_content_details/<?php if(isset($our_value['pcl_id'])){echo $our_value['pcl_id'];}?>">

				<div class="row"> 
					<div class="form-group col-md-4"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
						<input type="hidden" name="pcl_title" class="form-control pcl_title_cls" placeholder="Enter title" value="<?php if(isset($our_value['pcl_title'])){echo $our_value['pcl_title'];}?>"  required> 

						<input type="hidden" class="form-control form-white ml_id_cls" name="pcl_ml_id" value="<?php if(isset($our_value['pcl_ml_id'])){echo $our_value['pcl_ml_id'];}?>"> 
					</div>		
							 

					<div class="col-md-12">
						<div class="card card-outline card-info">
							<div class="card-header">
								<h3 class="card-title">
									Content
								</h3>
							</div> 
							<div class="card-body">
								<textarea class="summernote1 pcl_content_cls" style="height:50px;" name="pcl_content">
									<?php if(isset($our_value['pcl_content'])){echo $our_value['pcl_content'];}else{echo "Please enter the content here ....";}?>
								</textarea>
							</div> 
						</div>
					</div>		
					
						
					 
					<div class="form-group col-md-12 "> 
					
						<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
					</div>
				</div>
			</form>
		</div>
		
		
	</div>
</div>



<script type="text/javascript">
	var up_status = '';
			
	$(document).ready(function(){
		$('.fileupload2').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_content_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop2').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename2').html('');
					$('.filename2').val(data.result.file); 					
					$('.label_f_name2').text(data.result.file);
					$('.pro_id_shop2').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				} else{
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				}
				$('.pro_id_shop2').html('');	
			}
		});
		
		


		
	});
			
	function move(id,p_id) {
		//if(up_status=='true'){
			$('#'+id+'').css('display','block');
			var elem = document.getElementById(''+id+''); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
					$('#'+p_id+'').text('');
					$('#'+p_id+'').text(width+'%');
				}
			}
		//}		
	}

	
</script>



<!-- <script>
    ClassicEditor
        .create( document.querySelector( '.summernote' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '.summernote1' ) )
        .catch( error => {
            console.error( error );
        } );
</script> -->

