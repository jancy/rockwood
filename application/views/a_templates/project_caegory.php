<div class="col-12">
	<div class="card card-primary  card-default">
		<div class="card-header">
			<h3 class="card-title"> Project Category Details</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse">
				<i class="fas fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-card-widget="remove">
				<i class="fas fa-times"></i>
				</button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/add_gallery_details/<?php echo $ml_id;?>/<?php echo $pro_cate_id;?>/<?php if(isset($gtl_data['gtl_id'])){echo $gtl_data['gtl_id'];}?>">

				<div class="row">
					<div class="form-group col-md-4" style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;">   
						<input type="text" name="gtl_title" class="form-control gtl_title" placeholder="Enter title" value="<?php if(isset($gtl_data['gtl_title'])){echo $gtl_data['gtl_title'];}?>"   required>  
					</div>
					<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;margin-right:10px;"> 
						<input type="file" class="fileupload_gtl form-control" name="filename" id="avatar" style="opacity: 0;">
						<input type="hidden" class="form-control form-white gtl_i_img_cls filename2" name="gtl_i_img" value="<?php if(isset($gtl_data['gtl_i_img'])){echo $gtl_data['gtl_i_img'];}?>"> 
						<label class="label_gtl_i_img" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($gtl_data['gtl_i_img'])){echo $gtl_data['gtl_i_img'];}else{echo "Upload Icon Image";}?> </label>	
						<p class="pro_id_shop_gtl" ></p>									
																
					</div>
					<div class=" col-md-4  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
						<input type="file" class="fileupload1 form-control" name="filename" id="avatar" style="opacity: 0;">									
						<input type="hidden" class="form-control form-white ml_id_cls" name="gtl_ml_id" value="<?php if(isset($pro_cate_id)){echo $pro_cate_id;}?>">  
						<label class="label_gtl_name" style="float: left;color:#000;margin-top: -27px;">  Upload Your Gallery Image </label>									
						<p class="pro_id_shop" ></p>									
					</div>		
					<div class="col-md-12" style="margin: 0px;display: inline-block;height: 50px;margin-bottom:10px;">  
						<textarea class="col-md-12 gtl_desc_cls"  name="gtl_desc">
						<?php if(isset($gtl_data['gtl_desc'])){echo $gtl_data['gtl_desc'];}else{echo "Short desciption here ....";}?> 
						</textarea>
								
					</div>

					<div class="col-md-12" >
						<div class="card card-outline card-info">
							<div class="card-header">
								<h3 class="card-title"> Main Content </h3>
							</div> 
							<div class="card-body">
								<textarea id="summernote_gtl1" class="gtl_content_cls" style="height:50px;" name="gtl_content"> 
									<?php if(isset($gtl_data['gtl_content'])){echo $gtl_data['gtl_content'];}?> 
								</textarea>
							</div> 
						</div>
					</div>
					<div class="col-md-12 other_content_cls" >
						<div class="card card-outline card-info">
							<div class="card-header">
								<h3 class="card-title">
									Location Details
								</h3>
							</div> 
							<div class="card-body">
								<textarea id="summernote_gtl2" class="gtl_content_cls" style="height:50px;" name="gtl_content2">
									<?php if(isset($gtl_data['gtl_content2'])){echo $gtl_data['gtl_content2'];}else{?> 
										<ul>
											<li>Location: <span>Dubai, UAE</span></li>
											<li>Company website: <span><a class="text-second" href="www.fabtechint.com" target="_blank">www.fabtechint.com</a></span></li>
											<li>Machines Installed: <span class="tm-project-detail-list">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></li>									
										</ul>
									<?php } ?>
								</textarea>
							</div> 
						</div>
					</div>	
					<div class="col-md-12" id="gallery_img_div"> 
						<?php 
							$sl_zno = 0;
							if(isset($gl_data)){
								foreach($gl_data as $gl_key=>$gl_row){ 
						?>
									<div class="col-lg-2 form-group row g_div_cls" id="gl_div_id_0<?php echo $gl_row['gl_id'];?>" style="margin: 0px !important; display: inline-block;">
										<div><a class="close_btn" data-delete_type="img_delete" data-delete_id="<?php echo $gl_row['gl_id'];?>" style="padding: 5px; margin-left: -7px; color: #000;" ><i class="fas fa-times"></i></a></div>

										<img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/<?php echo $gl_row['gl_img'];?>" alt=""width="100" height="100">
										<input type="hidden" class="filename_0<?php echo $gl_key;?>" readonly="" name="gl_img[]" value="<?php echo $gl_row['gl_img'];?>">  
										<input type="hidden" name="gl_id[]" value="<?php echo $gl_row['gl_id'];?>">  
									</div>
						<?php
								}
							}
						?>
					</div>
					<div class="form-group col-md-12 "> 
					
						<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
					</div>
				</div>
			</form>
		</div>
		
	</div>
</div>
<!-- /.card -->
<div class="col-12">
	
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"> Menu 's List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Sl No</th>	
						<th>Image</th>										
						<th>Title</th>										
						<th>Short Description</th> 
						<th> </th> 
					</tr>
				</thead>
				<tbody>
					
					<?php 
						$sl_zno = 0;
						foreach($gtl_list as $gtl_key=>$gtl_row){
							$sl_zno = $sl_zno+1;
					?>
							<tr>
								<td><?php echo $sl_zno;?></td>
								<td>
									<img src= "<?php echo base_url()?>/public_html/upload/gallery_img/thumb/<?php echo  $gtl_row['gtl_i_img'];?>">
								</td>
								<td> <?php echo $gtl_row['gtl_title'];?> </td> 
								<td> <?php echo $gtl_row['gtl_desc'];?> </td>   
								<td> 
									<a class="btn mb-1 btn-flat btn-outline-primary btn-sm" href="<?php echo base_url();?>page_ctrl/add_projects/<?php echo $ml_id;?>/<?php echo $pro_cate_id;?>/<?php echo $gtl_row['gtl_id'];?>"> <i class="fas fa-edit" aria-hidden="true"></i> </a> 
 
									<a class="btn mb-1 btn-flat btn-outline-danger btn-sm close_btn" data-delete_type="gtl_id_del" data-delete_id="<?php echo $gtl_row['gtl_id'];?>" > <i class="fa fa-trash" aria-hidden="true"></i> </a> 
												
												
								</td>
							</tr>
					<?php
						}
					?>
					
					
					
				</tbody>
				<!-- <tfoot>
					<tr>
						<th>Rendering engine</th>
						<th>Browser</th>
						<th>Platform(s)</th>
						<th>Engine version</th>
						<th>CSS grade</th>
					</tr>
				</tfoot> -->
			</table>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>

 

<script type="text/javascript">
	var up_status = '';
			
	$(document).ready(function(){		 
		$('.fileupload1').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_gallery_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 
				up_status = '';		
				if(data.result.status==true){
					this_div_count = $('.g_div_cls').length;  		
					up_status='true'; 
					$('#gallery_img_div').append('<div class="col-lg-2 form-group row g_div_cls" id="gl_div_id_'+this_div_count+'" style="margin: 0px !important; display: inline-block;">'+
								'<div><a class="close_cls" data-close_id="gl_div_id_'+this_div_count+'" style="padding: 5px; margin-left: -7px; color: #000;" ><i class="fas fa-times"></i></a></div>'+

								'<img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/'+data.result.file+'" alt=""width="100" height="100">'+
								'<input type="hidden" class="filename_'+this_div_count+'" readonly="" name="gl_img[]" value="'+data.result.file+'"> '+ 
								'<input type="hidden" name="gl_id[]" value="0">'+					
								'</div>');	
					$('.pro_id_shop').html('');	
				}else if(data.result.status==false){
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}else{
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}	
				$('.pro_id_shop').html('');
			}
		});
 
		$('.fileupload_gtl').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_gallery_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop_gtl').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.gtl_i_img_cls').html('');
					$('.gtl_i_img_cls').val(data.result.file); 					
					$('.label_gtl_i_img').text(data.result.file);
					$('.pro_id_shop_gtl').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop_gtl').html('');				
					$('.pro_id_shop_gtl').append('Try again!..');
				} else{
					$('.pro_id_shop_gtl').html('');				
					$('.pro_id_shop_gtl').append('Try again!..');
				}
				$('.pro_id_shop_gtl').html('');	
			}
		});


		
	});
			
	function move(id,p_id) {
		//if(up_status=='true'){
			$('#'+id+'').css('display','block');
			var elem = document.getElementById(''+id+''); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
					$('#'+p_id+'').text('');
					$('#'+p_id+'').text(width+'%');
				}
			}
		//}		
	}

	

	$('body').on('click', '.close_cls',function(){
		this_c_id = $(this).data('close_id');  
		$("#"+this_c_id+'').remove();
	});

	$('body').on('click', '.close_btn',function(){
		console.log('fff');
		this_del_type = $(this).data('delete_type');
		this_del_id = $(this).data('delete_id'); 
		
		if(this_del_type=="img_delete"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$("#gl_div_id_0"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}

		if(this_del_type=="gtl_id_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$("#gallery_div_m_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					setTimeout(function () {  location.reload(true); }, 2000);
					
				}
			});
		}

	});
</script>


<script>
    ClassicEditor
        .create( document.querySelector('#summernote_gtl1') )
        .catch( error => {
            console.error( error );
        } );
</script>

<script>
    ClassicEditor
        .create( document.querySelector('#summernote_gtl2') )
        .catch( error => {
            console.error( error );
        } );
</script>

 


<script>
    (function() {
        var $gallery = new SimpleLightbox('.g_img.gallery a', {});
    })();
</script>