
<?php 
	$banner_img = '';
	$ml_re = str_replace(' ','',$ml_data['ml_img']);
	if($ml_re!=''){ 
		$banner_img =  base_url().'public_html/upload/slider/'.$ml_data['ml_img'].'';
	}else if($ml_re==''){  
		$banner_img =  base_url().'public_html/site_assets/images/contact-banner.jpg';
	}
?>
		<div id="content" class="site-content">
			
			<div class="page-header" style="background-image: url('<?php echo $banner_img;?>');">
				<div class="container">
					<div class="breadc-box no-line">
						<div class="row">
							<div class="col-md-12">
								<h1 class="page-title"><?php echo $pcl_data['pcl_title'];?></h1>
								<ul id="breadcrumbs" class="breadcrumbs none-style">
									<li><a href="<?php echo base_url();?>">Home</a></li>
									<li><a href="<?php echo base_url();?>">Blog</a></li>
									<li class="active"><?php echo $pcl_data['pcl_title'];?></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="entry-content tm-post-wrap">
				<div class="container">
					<div class="row">
						<aside id="sidebar" class="widget-area primary-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
						    <div id="search-2" class="widget widget_search">
						        <form role="search" method="get" id="search-form" class="search-form" action="#">
						            <div class="input-group">
                                        <input type="search" name="EMAIL" class="form-control search-field" placeholder="search..." value="" name="s">
                                        <span class="input-group-btn">
                                        	<button type="submit" class="btn btn-search"><i class="icon ion-md-search"></i></button>
                                        </span>
                                    </div>
						        </form>
						    </div>
						    <!-- <div id="categories-2" class="widget widget_categories">
						        <h4 class="widget-title">Categories</h4>
						        <ul>
						            <li><a href="#">All post</a></li>
						            <li class="cat-item cat-item-3"><a href="#">Transportation & Distribution</a></li>
						            <li class="cat-item cat-item-4"><a href="#">Oil & Gas exploited</a></li>
						            <li class="cat-item cat-item-5"><a href="#">Automotive Manufacturing</a></li>
						            <li class="cat-item cat-item-6"><a href="#">Industrial Construction</a></li>
						            <li class="cat-item cat-item-1"><a href="#">Manufacture</a></li>
						            <li class="cat-item cat-item-1"><a href="#">Green Energy</a></li>
						        </ul>
						    </div> -->

						    <div id="recent_news-1" class="widget widget_recent_news">
						        <h4 class="widget-title">Latest Post</h4>
						        <ul class="recent-news clearfix">
								<?php 
									$pc = 0;
									foreach($pcl_list as $pcl_row){
										if($pc!=3){
											if($pcl_data['pcl_id']!=$pcl_row['pcl_id']){
								?>
												<li class="clearfix ">
													<div class="thumb">
														<a href="<?php echo base_url();?>post/<?php echo str_replace(' ','-',$pcl_row['pcl_title']);?>">
															<img src="<?php echo base_url();?>public_html/upload/content/<?php echo $pcl_row['pcl_img'];?>" alt="">
														</a>
													</div>
													<div class="entry-header">
														<h4>
															<a href="<?php echo base_url();?>post/<?php echo str_replace(' ','-',$pcl_row['pcl_title']);?>"><?php echo $pcl_row['pcl_title'];?></a>
														</h4>
														<span class="post-on">
															<span class="entry-date"><?php echo $pcl_row['pcl_sub_title'];?></span>
														</span>
													</div>
												</li>
						        <?php
												$pc =$pc+1;
											}
										}
									}
								?>  

						        </ul>

						    </div>

						    <!-- <div id="tag_cloud-2" class="widget widget_tag_cloud">
						        <h4 class="widget-title">Tags</h4>
						        <div class="tagcloud">
						        	<a href="#">News</a>
						            <a href="#">Factory</a>
						            <a href="#">Construction</a>
						            <a href="#">Automotive</a>
						            <a href="#">Oil & Gas</a>
						            <a href="#">Manufacture</a>
						            <a href="#">Green Energy</a>
						        </div>
						    </div> -->

						    <!-- <div id="recent_tweets-1" class="widget widget_recent_news">
						        <h4 class="widget-title">Recent Tweets</h4>
						        <ul class="recent-tweets clearfix">

						            <li class="clearfix ">
						                <div class="tweets-logo">
						                    <i class="icon ion-logo-twitter"></i>
						                </div>
						                <div class="tweets-content">
						                    <p>Freelancers, grab some thanking your clients @mstavakoli. <a href="#"> https://t.co/DOfxmjMNLz</a></p>
						                </div>
						            </li>

						            <li class="clearfix ">
						                <div class="tweets-logo">
						                    <i class="icon ion-logo-twitter"></i>
						                </div>
						                <div class="tweets-content">
						                    <p>Freelancers, grab some thanking your clients @mstavakoli. <a href="#"> https://t.co/DOfxmjMNLz</a></p>
						                </div>
						            </li>

						        </ul>

						    </div> -->

						</aside>

						<div id="primary" class="tm-post content-area col-lg-8 col-md-8 col-sm-12 col-xs-12">
						    <main id="main" class="site-main">

						        <article class="single-post">
					                <header class="entry-header meta-post seperate-border">

					                    <div class="entry-meta meta-left">
					                        <span class="posted-on"><span class="entry-date published"><?php echo $pcl_data['pcl_sub_title'];?></span></span>
					                        <!-- <span class="posted-in"><a href="#" rel="category tag">Oil & Gas</a></span> -->
					                    </div>

					                    <!-- <div class="entry-meta meta-right">
					                        <span class="meta-comments">Comments: <a href="#">03</a></span>
					                        <span class="meta-views">Views: <a href="#">63</a></span>
					                    </div> -->
					                    <!-- .entry-meta -->
					                </header>
					                <!-- .entry-header -->

					                <div class="entry-summary seperate-border">
										<?php echo $pcl_data['pcl_content'];?>
					                </div>
					                <!-- .entry-content -->
					                <div class="tags">
							            Tags: <a href="#" rel="tag">Contruction</a> <a href="#">Building</a>
							        </div>							        
						        </article>						        						       

						    </main>
						    <!-- #main -->
						</div>

						
					</div>
			    </div>
			</div>
		</div>
	    
		