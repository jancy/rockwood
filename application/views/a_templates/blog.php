


<div class="col-12">
	<div class="card card-primary  card-default">
		<div class="card-header">
			<h3 class="card-title">  Blogs </h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse">
				<i class="fas fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-card-widget="remove">
				<i class="fas fa-times"></i>
				</button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/add_content_details/<?php if(isset($bl_data['pcl_id'])){echo $bl_data['pcl_id'];}?>">

				<div class="row">
					<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
						<input type="file" class="fileupload2 form-control" name="filename" id="avatar" style="opacity: 0;">
						<input type="hidden" class="form-control form-white pcl_img_cls filename2" name="pcl_img"  value="<?php if(isset($bl_data['pcl_img'])){echo $bl_data['pcl_img'];}?>"> 
						<label class="label_f_name2" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($bl_data['pcl_img'])){echo $bl_data['pcl_img'];}else{echo "Upload The Main Image";}?> </label>	
						<p class="pro_id_shop2" ></p>									
																
					</div>
					<div class="form-group col-md-4"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
						<input type="text" name="pcl_title" class="form-control pcl_title_cls" placeholder="Enter title" value="<?php if(isset($bl_data['pcl_title'])){echo $bl_data['pcl_title'];}?>"  required> 

						<input type="hidden" class="form-control form-white ml_id_cls" name="pcl_ml_id" value="<?php if(isset($bl_data['pcl_ml_id'])){echo $bl_data['pcl_ml_id'];}else{echo $ml_id;}?>"> 
					</div>		
							
					<div class="form-group col-md-5"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
						<input type="text" name="pcl_sub_title" class="form-control pcl_stitle_cls" placeholder="Enter Date (March 4, 2022) "  value="<?php if(isset($bl_data['pcl_sub_title'])){echo $bl_data['pcl_sub_title'];}?>" >   
					</div>

					<div class="col-md-12">
						<div class="card card-outline card-info">
							<div class="card-header">
								<h3 class="card-title">
									Content
								</h3>
							</div> 
							<div class="card-body">
								<textarea class="summernote pcl_content_cls" style="height:50px;" name="pcl_content">
									<?php if(isset($bl_data['pcl_content'])){echo $bl_data['pcl_content'];}else{echo "Please enter the content here ....";}?>
								</textarea>
							</div> 
						</div>
					</div>		
					
						
					 
					<div class="form-group col-md-12 "> 
					
						<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
					</div>
				</div>
			</form>
		</div>
		
		
	</div>
</div>

<div class="col-12">
	
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"> Blog List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Sl No</th>	
						<th>Image</th>										
						<th>Content</th> 
						<th> </th> 
					</tr>
				</thead>
				<tbody>
					
					<?php 
						$slno = 0;
						foreach($bl_list as $bl_key=>$bl_row){
							$slno = $slno+1;
					?>
							<tr>
								<td><?php echo $slno;?></td>
								<td>
									<img src= "<?php echo base_url()?>/public_html/upload/content/thumb/<?php echo  $bl_row['pcl_img'];?>">
								</td> 
								<td> <?php $pcl_content =  strip_tags($bl_row['pcl_content']); echo substr($pcl_content,0,200);?> ... </td>  
								<td>
									<a class="btn mb-1 btn-flat btn-outline-primary btn-sm" href="<?php echo base_url();?>page_ctrl/create_page/<?php echo $bl_row['pcl_ml_id'];?>/<?php echo $bl_row['pcl_id'];?>"> <i class="fas fa-edit" aria-hidden="true"></i> </a> 
 
									<a class="btn mb-1 btn-flat btn-outline-danger btn-sm close_btn" data-delete_type="pcl_tr_del" data-delete_id="<?php echo $bl_row['pcl_id'];?>" > <i class="fa fa-trash" aria-hidden="true"></i> </a> 
												
												
								</td>
							</tr>
					<?php
						}
					?>
					 
					 
					
				</tbody>
				<!-- <tfoot>
					<tr>
						<th>Rendering engine</th>
						<th>Browser</th>
						<th>Platform(s)</th>
						<th>Engine version</th>
						<th>CSS grade</th>
					</tr>
				</tfoot> -->
			</table>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>

 


<script type="text/javascript">
	var up_status = '';
			
	$(document).ready(function(){
		$('.fileupload2').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_content_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop2').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename2').html('');
					$('.filename2').val(data.result.file); 					
					$('.label_f_name2').text(data.result.file);
					$('.pro_id_shop2').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				} else{
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				}
				$('.pro_id_shop2').html('');	
			}
		});
		
		


		
	});
			
	function move(id,p_id) {
		//if(up_status=='true'){
			$('#'+id+'').css('display','block');
			var elem = document.getElementById(''+id+''); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
					$('#'+p_id+'').text('');
					$('#'+p_id+'').text(width+'%');
				}
			}
		//}		
	}

	
	$('body').on('click', '.close_btn',function(){
		console.log('fff');
		this_del_type = $(this).data('delete_type');
		this_del_id = $(this).data('delete_id'); 
		 
		if(this_del_type=="pcl_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".pcl_tr_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}		 
		setTimeout(function () {  location.reload(true); }, 2000);
	});
</script>



<script>
    ClassicEditor
        .create( document.querySelector( '.summernote' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '.summernote1' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

