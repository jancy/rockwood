
        <div id="content" class="site-content">
            <div class="page-404" style="background-image: url('<?php echo base_url();?>public_html/site_assets/images/404.jpg');">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="error-title">4<i class="icon ion-ios-outlet"></i>4 Error</h1>
                            <hr>
                            <h3 class="error-subtitle">Opps...! sorry we can’t find this page!</h3>
                            <div class="industris-space"></div>
                            <a href="<?php echo base_url();?>" class="btn btn-primary">Back to home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        