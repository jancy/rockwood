<!--  light box  -->
<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/added_plugins/lightbox/simple-lightbox.css">
<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/lightbox/simple-lightbox.js"></script>

<?php if(isset($ml_data)){ ?>

<div class="row"> 
	<div class="col-12 col-sm-12">
		
		<div class="card card-primary card-outline card-outline-tabs">
			<div class="card-header p-0 border-bottom-0">
				<ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" id="tab-slider-tab" data-toggle="pill" href="#tab-slider" role="tab" aria-controls="tab-slider" aria-selected="true">Slider </a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-content-tab" data-toggle="pill" href="#tab-content" role="tab" aria-controls="tab-content" aria-selected="false">Page Content</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="tab-img-upload-tab" data-toggle="pill" href="#tab-img-upload" role="tab" aria-controls="tab-img-upload" aria-selected="false">Image Gallery </a>
					</li> 
					<li class="nav-item">
						<a class="nav-link" id="tab-video-upload-tab" data-toggle="pill" href="#tab-video-upload" role="tab" aria-controls="tab-video-upload" aria-selected="false">Video Gallery</a>
					</li>
				</ul>
			</div>
			<div class="card-body">
				<div class="tab-content" id="custom-tabs-four-tabContent">
					<div class="tab-pane fade show active" id="tab-slider" role="tabpanel" aria-labelledby="tab-slider-tab">					 
						<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/add_slider_details">

							<div class="row">
								<div class=" col-md-12  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
									<input type="file" class="fileupload form-control" name="filename" id="avatar" style="opacity: 0;">									
									<input type="hidden" class="form-control form-white ml_id_cls" name="sl_ml_id" value="<?php if(isset($sl_data['sl_ml_id'])){echo $sl_data['sl_ml_id'];}?>"> 
									<input type="hidden" class="form-control form-white filename1" name="sl_img" value="<?php if(isset($sl_data['sl_img'])){echo $sl_data['sl_img'];}?>" > 
									<label class="label_f_name" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($sl_data['sl_img'])){echo $sl_data['sl_img'];}else{echo "Upload Your Slider";}?> </label>	
											<p class="pro_id_shop" ></p>									
																			
								</div>
								<div class="col-md-12">
									<div class="card card-outline card-info">
										<div class="card-header">
											<h3 class="card-title"> Content </h3>
										</div> 
										<div class="card-body">
											<textarea id="summernote1" class="sl_content_cls" style="height:10px;" name="sl_content">
												<?php if(isset($sl_data['sl_content'])){echo $sl_data['sl_content'];}else{echo "Please enter the content here ....";}?>
											</textarea>
										</div> 
									</div>
								</div>
								
									
								
								<div class="form-group col-md-12 "> 
								
									<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
								</div>
							</div>
						</form>

						<div class="col-md-12" id="slider_div_id">		
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Sl No</th>	
										<th>Image</th>										
										<th>Content</th> 
										<th> </th> 
									</tr>
								</thead>
								<tbody>
							
									<?php 
										$sl_zno = 0;
										foreach($sl_list as $sl_key=>$sl_row){
											$sl_zno = $sl_zno+1;
									?>
											<tr>
												<td><?php echo $sl_zno;?></td>
												<td>
													<img src= "<?php echo base_url()?>/public_html/upload/slider/thumb/<?php echo  $sl_row['sl_img'];?>">
												</td> 
												<td> <?php echo $sl_row['sl_content'];?> </td>  
												<td>
													<a class="btn mb-1 btn-flat btn-outline-primary btn-sm edit_cls" data-edit_type="gtl_edit" data-edit_val_id="<?php echo $sl_row['sl_id'];?>"> <i class="fas fa-edit" aria-hidden="true"></i> </a> 
				
													<a class="btn mb-1 btn-flat btn-outline-danger btn-sm close_1_btn"  data-delete_type="slider_tr_del" data-delete_id="<?php echo $sl_row['sl_id'];?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a> 
																
																
												</td>
											</tr>
									<?php
										}
									?>
									
									
									
								</tbody> 
							</table>					
						</div>
					</div>
					<div class="tab-pane fade" id="tab-content" role="tabpanel" aria-labelledby="tab-content-tab">
						<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?>page_ctrl/add_content_details/<?php if(isset($pro_p_data['pcl_id'])){echo $pro_p_data['pcl_id'];}?>/<?php echo $ml_id;?>">

							<div class="row">
								<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
									<input type="file" class="fileupload2 form-control" name="filename" id="avatar" style="opacity: 0;">
									<input type="hidden" class="form-control form-white pcl_img_cls filename2" name="pcl_img"  value="<?php if(isset($pro_p_data['pcl_img'])){echo $pro_p_data['pcl_img'];}?>"> 
									<label class="label_f_name2" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($pro_p_data['pcl_img'])){echo $pro_p_data['pcl_img'];}else{echo "Upload The Right Side Image";}?> </label>	
									<p class="pro_id_shop2" ></p>									
																			
								</div>
								<div class="form-group col-md-4"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
									<input type="text" name="pcl_title" class="form-control pcl_title_cls" placeholder="Enter title" value="<?php if(isset($pro_p_data['pcl_title'])){echo $pro_p_data['pcl_title'];}?>"  required> 

									<input type="hidden" class="form-control form-white ml_id_cls" name="pcl_ml_id" value="<?php if(isset($pro_p_data['pcl_ml_id'])){echo $pro_p_data['pcl_ml_id'];}?>"> 
								</div>		
										
								<div class="form-group col-md-5"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">   
									<input type="text" name="pcl_sub_title" class="form-control pcl_stitle_cls" placeholder="Enter Sub Title"  value="<?php if(isset($pro_p_data['pcl_sub_title'])){echo $pro_p_data['pcl_sub_title'];}?>" >   
								</div>

								<div class="col-md-12">
									<div class="card card-outline card-info">
										<div class="card-header">
											<h3 class="card-title">
												Content
											</h3>
										</div> 
										<div class="card-body">
											<textarea class="summernote pcl_content_cls" style="height:50px;" name="pcl_content">
												<?php if(isset($pro_p_data['pcl_content'])){echo $pro_p_data['pcl_content'];}else{echo "Please enter the content here ....";}?>
											</textarea>
										</div> 
									</div>
								</div>		
								
									
								
								<div class="form-group col-md-12 "> 
								
									<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
								</div>
							</div>
						</form>
						 
					</div>
					<div class="tab-pane fade" id="tab-img-upload" role="tabpanel" aria-labelledby="tab-img-upload-tab">
						
						<form enctype="multipart/form-data" class="gtl_form_cls" method="POST" action="<?php echo base_url()?>page_ctrl/add_p_img_details/<?php echo $ml_id;?>/<?php echo $sub_cate_id;?>/<?php echo $sub_pro_id;?>/<?php if(isset($pro_gtl_list['gtl_id'])){echo $pro_gtl_list['gtl_id'];}?>">

							<div class="row">
								<div class="form-group col-md-4" style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;">   
									<input type="text" name="gtl_title" class="form-control gtl_title" placeholder="Enter title" value="<?php if(isset($pro_gtl_list['gtl_title'])){echo $pro_gtl_list['gtl_title'];}?>"   required>  
								</div>
								<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;margin-right:10px;"> 
									<input type="file" class="fileupload_gtl form-control" name="filename" id="avatar" style="opacity: 0;">
									<input type="hidden" class="form-control form-white gtl_i_img_cls filename2" name="gtl_i_img" value="<?php if(isset($pro_gtl_list['gtl_i_img'])){echo $pro_gtl_list['gtl_i_img'];}?>"> 
									<label class="label_gtl_i_img" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($pro_gtl_list['gtl_i_img'])){echo $pro_gtl_list['gtl_i_img'];}else{echo "Upload Icon Image";}?> </label>	
									<p class="pro_id_shop_gtl" ></p>									
																			
								</div>
								<div class=" col-md-4  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
									<input type="file" class="fileupload1 form-control" name="filename" id="avatar" style="opacity: 0;">									
									<input type="hidden" class="form-control form-white ml_id_cls" name="gtl_ml_id" value="<?php if(isset($sub_pro_id)){echo $sub_pro_id;}?>">  
									<label class="label_gtl_name" style="float: left;color:#000;margin-top: -27px;">  Upload Your Gallery Image </label>									
									<p class="pro_id_shop" ></p>									
								</div>		
							 
  	
								<div class="col-md-12" id="gallery_img_div"> 
									 
								</div>
								<div class="form-group col-md-12 "> 
								
									<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
								</div>
							</div>
						</form>
						<div class="col-12">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>Sl No</th>	 										
										<th>Group Name</th>				 
										<th> </th> 
									</tr>
								</thead>
								<tbody>
									
									<?php 
										$sl_zno = 0;
										foreach($pro_gtl_list as $gtl_key=>$gtl_row){
											$sl_zno = $sl_zno+1;
									?>
											<tr>
												<td><?php echo $sl_zno;?></td> 
												<td> <?php echo $gtl_row['gtl_title'];?> </td>  
												<td> 
													<a class="btn mb-1 btn-flat btn-outline-primary btn-sm edit_cls" data-edit_type="gtl_edit" data-edit_val_id="<?php echo $gtl_row['gtl_id'];?>" > <i class="fas fa-edit" aria-hidden="true"></i> </a> 
				
													<a class="btn mb-1 btn-flat btn-outline-danger btn-sm close_1_btn" data-delete_type="gtl_id_del" data-delete_id="<?php echo $gtl_row['gtl_id'];?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a> 
																
																
												</td>
											</tr>
									<?php
										}
									?>
									
									
									
								</tbody>
								<!-- <tfoot>
									<tr>
										<th>Rendering engine</th>
										<th>Browser</th>
										<th>Platform(s)</th>
										<th>Engine version</th>
										<th>CSS grade</th>
									</tr>
								</tfoot> -->
							</table>
						</div>
					</div>
					<div class="tab-pane fade" id="tab-video-upload" role="tabpanel" aria-labelledby="tab-video-upload-tab">
						<form class="form_submit"  method="POST" action="<?php echo base_url()?>page_ctrl/add_video_url_details/<?php echo $ml_id.'/'.$sub_cate_id.'/'.$sub_pro_id;?>">
							<div class="form-group col-md-4" style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;">   
								<input type="text" name="vgl_url" class="form-control" placeholder="Enter Yputubr url"   required> 
								<input type="hidden" class="form-control form-white ml_id_cls" name="vgl_ml_id" value="<?php if(isset($ml_data['ml_id'])){echo $ml_data['ml_id'];}?>"> 
								 
							</div> 					
							<div class=" col-md-1"  style=" margin: 0px; display: inline-block;margin-bottom:10px; ">					
								<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-12" style="top:20px;">Save</button>
							</div>							
						</form>

						<div class="col-md-12" >	
							
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr> 
										<th>Url</th>			 
										<th> </th> 
									</tr>
								</thead>
								<tbody>
									
									<?php  
										foreach($vl_data as $vl_key=>$vl_row){ 
											if($vl_row['vgl_active']=='1'){
									?>
												<tr> 
													<td> <?php echo $vl_row['vgl_url'];?> </td>   
													<td>  
														<a class="btn mb-1 btn-flat btn-outline-danger btn-sm close_1_btn" data-delete_type="vgl_tr_del" data-delete_id="<?php echo $vl_row['vgl_id'];?>" > <i class="fa fa-trash" aria-hidden="true"></i> </a> 
																	
																	
													</td>
												</tr>
									<?php
											}
										}
									?>
									
									
									
								</tbody> 
							</table>					
						</div>
					</div>
				</div>
			</div>
			<!-- /.card -->
		</div>
	</div>
</div>

<?php } ?>
<script type="text/javascript">
	ml_id = <?php echo json_encode($ml_id);?>;
	sub_cate_id = <?php echo json_encode($sub_cate_id);?>;
	sub_pro_id = <?php echo json_encode($sub_pro_id);?>;
	var up_status = '';
			
	$(document).ready(function(){
		$('.fileupload').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_slider_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename1').html('');
					$('.filename1').val(data.result.file); 					
					$('.label_f_name').text(data.result.file);
					$('.pro_id_shop').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				} else{
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}
				$('.pro_id_shop').html('');	
			}
		});
		 
		$('.fileupload1').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_gallery_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 
				up_status = '';		
				if(data.result.status==true){
					this_div_count = $('.g_div_cls').length;  		
					up_status='true'; 
					$('#gallery_img_div').append('<div class="col-lg-2 form-group row g_div_cls" id="gl_div_id_'+this_div_count+'" style="margin: 0px !important; display: inline-block;">'+
								'<img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/'+data.result.file+'" alt=""width="50" height="50">'+
								'<a class="close_cls" data-close_id="gl_div_id_'+this_div_count+'"><i class="fas fa-times"></i></a>'+
								'<input type="hidden" class="filename_'+this_div_count+'" readonly="" name="gl_img[]" value="'+data.result.file+'"> <br><br>'+ 
								'<input type="hidden" name="gl_id[]" value="0"> <br><br>'+					
								'</div>');	
					$('.pro_id_shop').html('');	
				}else if(data.result.status==false){
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}else{
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}	
				$('.pro_id_shop').html('');
			}
		});

		$('.fileupload2').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_content_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop2').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename2').html('');
					$('.filename2').val(data.result.file); 					
					$('.label_f_name2').text(data.result.file);
					$('.pro_id_shop2').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				} else{
					$('.pro_id_shop2').html('');				
					$('.pro_id_shop2').append('Try again!..');
				}
				$('.pro_id_shop2').html('');	
			}
		});

		$('.fileupload_gtl').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_gallery_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop_gtl').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.gtl_i_img_cls').html('');
					$('.gtl_i_img_cls').val(data.result.file); 					
					$('.label_gtl_i_img').text(data.result.file);
					$('.pro_id_shop_gtl').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop_gtl').html('');				
					$('.pro_id_shop_gtl').append('Try again!..');
				} else{
					$('.pro_id_shop_gtl').html('');				
					$('.pro_id_shop_gtl').append('Try again!..');
				}
				$('.pro_id_shop_gtl').html('');	
			}
		});


		
	});
			
	function move(id,p_id) {
		//if(up_status=='true'){
			$('#'+id+'').css('display','block');
			var elem = document.getElementById(''+id+''); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
					$('#'+p_id+'').text('');
					$('#'+p_id+'').text(width+'%');
				}
			}
		//}		
	}

	 
	$('body').on('click', '.edit_cls',function(){
		this_type = $(this).data('edit_type');
		this_id = $(this).data('edit_val_id');  
 


		if(this_type=="gtl_edit"){ 
			form_action = '<?php echo base_url()?>page_ctrl/add_p_img_details/'+ml_id+'/'+sub_cate_id+'/'+sub_pro_id; 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/get_menu_data_by_id",
				data: {"type":this_type,"id":this_id},
				dataType: "json",
				success: function(data){ 
					$('.gtl_i_img_cls').val('');  
					$('.gtl_i_img_cls').val(data['gtl_data']['gtl_i_img']);  
					$('.label_gtl_i_img').html('');  
					$('.label_gtl_i_img').html(data['gtl_data']['gtl_i_img']);  
					$('.gtl_title').val(data['gtl_data']['gtl_title']);    
					$('#gallery_img_div').html('');
					$.each(data.gl_data , function(i4,n4){ 	
						$('#gallery_img_div').append('<div class="col-lg-2 form-group row g_div_cls" id="gl_div_id_0'+n4.gl_id+'" style="margin: 0px !important; display: inline-block;">'+
								'<img src="<?php echo base_url();?>public_html/upload/gallery_img/thumb/'+n4.gl_img+'" alt=""width="50" height="50">'+
								'<a class="close_1_btn" data-delete_type="img_delete" data-delete_id="'+n4.gl_id+'"><i class="fas fa-times"></i></a>'+
								'<input type="hidden" class="filename_0'+i4+'" readonly="" name="gl_img[]" value="'+n4.gl_img+'"> <br><br>'+ 
								'<input type="hidden" name="gl_id[]" value="'+n4.gl_id+'"> <br><br>'+ 
								'</div>'); 
					});
					
					form_action = form_action+'/'+data['gtl_data']['gtl_id']; 

					$('.gtl_form_cls').attr('action',''); 
					$('.gtl_form_cls').attr('action',form_action); 
					
					
				}
			});
		}
	});


	$('body').on('click', '.close_cls',function(){
		this_c_id = $(this).data('close_id');  
		$("#"+this_c_id+'').remove();
	});

	$('body').on('click', '.close_1_btn',function(){
		console.log('fff');
		this_del_type = $(this).data('delete_type');
		this_del_id = $(this).data('delete_id'); 
		if(this_del_type=="slider_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".slider_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}
		if(this_del_type=="pcl_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".pcl_tr_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}
		if(this_del_type=="vgl_tr_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$(".vgl_tr_cls_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}

		if(this_del_type=="img_delete"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$("#gl_div_id_0"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}

		if(this_del_type=="gtl_id_del"){ 
			$.ajax({
				type: "POST",
				url: "<?php echo base_url()?>page_ctrl/delete_page_details",
				data: {"type":this_del_type,"id":this_del_id},
				dataType: "json",
				success: function(data){
					$("#gallery_div_m_"+this_del_id+'').remove();
					toastr.success('Deleted', '')
					
				}
			});
		}
		setTimeout(function () {  location.reload(true); }, 1000);

	});
</script>
 


<script>

	<!-- CKEDITOR.plugins.addExternal( 'abbr', '/myplugins/abbr/', 'plugin.js' );
	config.extraPlugins = 'sourcedialog'; -->
    CKEDITOR.replace( 'pcl_1content',{
        extraPlugins: 'abbr,sourcedialog'
} );
</script>

<!-- <script>
    ClassicEditor
        .create( document.querySelector( '#summernote1' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#summernote_gtl1' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

<script>
    ClassicEditor
        .create( document.querySelector( '#summernote_gtl2' ) )
        .catch( error => {
            console.error( error );
        } );
</script> -->




<script>
    (function() {
        var $gallery = new SimpleLightbox('.g_img.gallery a', {});
    })();
</script>