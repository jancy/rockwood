<div class="col-12">
	<div class="card card-primary  card-default">
		<div class="card-header">
			<h3 class="card-title"> <?php echo $view_type;?> Menu Details</h3>
			<div class="card-tools">
				<button type="button" class="btn btn-tool" data-card-widget="collapse">
				<i class="fas fa-minus"></i>
				</button>
				<button type="button" class="btn btn-tool" data-card-widget="remove">
				<i class="fas fa-times"></i>
				</button>
			</div>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<form enctype="multipart/form-data" method="POST" action="<?php echo base_url()?><?php if($view_type=="Add"){echo "add_menu_details";}else{echo "menus_ctrl/add_menu_details/";}if(isset($ml_data['ml_id'])){echo $ml_data['ml_id'];}?>">

				<div class="row">
					<div class="form-group col-md-4">   
						<input type="text" name="ml_name" class="form-control" id="exampleInputEmail1" title="Enter Name" placeholder="Enter Name" value="<?php if(isset($ml_data['ml_name'])){echo $ml_data['ml_name'];}?>" required> 
					</div>
					<div class="form-group col-md-4">   
						<input type="text" name="ml_slug" class="form-control" id="exampleInputEmail1" title="Enter Slug" placeholder="Enter Slug" value="<?php if(isset($ml_data['ml_slug'])){echo $ml_data['ml_slug'];}?>" required> 
					</div>
					<div class="form-group col-md-4">   
						<input type="text" name="ml_title" class="form-control" id="exampleInputEmail1" title="Enter Title" placeholder="Enter Title" value="<?php if(isset($ml_data['ml_title'])){echo $ml_data['ml_title'];}?>" required> 
					</div>
					<div class="form-group col-md-12">   
						<input type="text" name="ml_m_title" class="form-control" id="exampleInputEmail1" title="Enter Title" placeholder="Enter Metta Title" value="<?php if(isset($ml_data['ml_m_title'])){echo $ml_data['ml_m_title'];}?>" required> 
					</div>
					<div class="form-group col-md-4"> 
						<label>Enter Meta Description </label>
						<textarea class="form-control" name="ml_desc" rows="3" title="Enter Description" placeholder="Enter Meta Description ..." required> <?php if(isset($ml_data['ml_desc'])){echo $ml_data['ml_desc'];}?> </textarea>
					</div> 
					<div class="form-group col-md-4"> 
						<label>Enter Meta Keyword </label>
						<textarea class="form-control" name="ml_keyword" rows="3" title="Enter Keyword" placeholder="Enter Meta Keyword ..." required> <?php if(isset($ml_data['ml_keyword'])){echo $ml_data['ml_keyword'];}?> </textarea>
					</div>
					<div class="form-group col-md-4"> 
						<label>Enter Short Description </label>
						<textarea class="form-control" name="ml_s_desc" rows="3" title="Enter Keyword" placeholder="Enter Short Description ..." required> <?php if(isset($ml_data['ml_s_desc'])){echo $ml_data['ml_s_desc'];}?> </textarea>
					</div>
					<div class="form-group col-md-2"> 
						<select class="form-control form-white"  name="ml_type" required>
							<option value="" > Select Template  </option>
							<option value="home" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='home')){echo "selected=selected";}?>> home  </option>
							<option value="about-us" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='about')){echo "selected=selected";}?>>about  </option>
							<option value="project" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='project')){echo "selected=selected";}?>>project  </option>
							<option value="rentals" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='rental')){echo "selected=selected";}?>>rental  </option>
							<option value="services" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='services')){echo "selected=selected";}?>>services  </option>
							<option value="products" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='products')){echo "selected=selected";}?>>products  </option>
							<option value="productsub" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='productsub')){echo "selected=selected";}?>>product Sub  </option>
							<option value="productdetails" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='productdetails')){echo "selected=selected";}?>>product Details  </option>
							<option value="blog" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='blog')){echo "selected=selected";}?>>blog  </option>
							<option value="contact-us" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='contactus')){echo "selected=selected";}?>>contactus  </option>
							<option value="clients" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='clients')){echo "selected=selected";}?>>Clients  </option>
							<option value="Gallery" <?php if(isset($ml_data['ml_type']) && ($ml_data['ml_type']=='Gallery')){echo "selected=selected";}?>>Gallery  </option>
							
						</select>
					</div>
					<div class="form-group col-md-2"> 
						<select class="form-control form-white"  name="ml_active" required>
							<option value="1" <?php if(isset($ml_data['ml_active']) && ($ml_data['ml_active']=='1')){echo "selected=selected";}?>> Active  </option>

							<option value="0" <?php if(isset($ml_data['ml_active']) && ($ml_data['ml_active']=='0')){echo "selected=selected";}?>>Inactive  </option>
							
						</select>
					</div>
					<div class="form-group col-md-2"> 
						<select class="form-control form-white"  name="ml_menu_type" required>
							<option value="1" <?php if(isset($ml_data['ml_menu_type']) && ($ml_data['ml_menu_type']=='1')){echo "selected=selected";}?>> Main Menu   </option>

							<!-- <option value="2" <?php if(isset($ml_data['ml_menu_type']) && ($ml_data['ml_menu_type']=='2')){echo "selected=selected";}?>>Sub Menu  </option> -->

							<option value="3" <?php if(isset($ml_data['ml_menu_type']) && ($ml_data['ml_menu_type']=='3')){echo "selected=selected";}?>>Hidden Menu  </option>
							
						</select>
					</div>
					<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
						<input type="file" class="fileupload form-control" name="filename" id="avatar" style="opacity: 0;">									 
						<input type="hidden" class="form-control form-white filename1" name="ml_img" value="<?php if(isset($ml_data['ml_img']) && $ml_data['ml_img']!='' ){echo $ml_data['ml_img'];}?> " > 
						<label class="label_f_name" style="float: left;color:#000;margin-top: -27px;">  
							<?php if(isset($ml_data['ml_img']) && str_replace(' ','',$ml_data['ml_img'])!='' ){echo $ml_data['ml_img'];}else{ echo "Upload Banner Image File";}?>
						</label>	
						<p class="pro_id_shop" ></p>									
					</div>
					
					<div class=" col-md-3  "style="margin: 0px;display: inline-block;height: 40px;margin-bottom:10px;border:1px solid #dadfe3;"> 
						<input type="file" class="fileupload_sub form-control" name="filename" id="avatar" style="opacity: 0;">									 
						<input type="hidden" class="form-control form-white filename2" name="ml_sub_img" value="<?php if(isset($ml_data['ml_ml_sub_imgimg']) && $ml_data['ml_sub_img']!='' ){echo $ml_data['ml_sub_img'];}?> " > 
						<label class="label_sub_f_name" style="float: left;color:#000;margin-top: -27px;">  <?php if(isset($ml_data['ml_sub_img']) && str_replace(' ','',$ml_data['ml_sub_img'])!='' ){echo $ml_data['ml_sub_img'];}else{ echo "Upload Featued Image File";}?> </label>	
						<p class="pro_sub_id_shop" ></p>									
					</div>	
					 
					<div class="form-group col-md-12 "> 
					
						<button type="submit" class="btn btn-block btn-outline-primary btn-flat float-right col-md-2">Save</button>
					</div>
				</div>
			</form>
		</div>
		
	</div>
</div>
<!-- /.card -->
<div class="col-12">
	
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"> Menu 's List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Sl No</th>	
						<th>Main Type</th>										
						<th>Name</th>
						<th>Slug</th>
						<th> </th> 
					</tr>
				</thead>
				<tbody>
					
					<?php 
						$sl_zno = 0;
						foreach($ml_list as $ml_key=>$ml_row){
							$sl_zno = $sl_zno+1;
					?>
							<tr>
								<td><?php echo $sl_zno;?></td>
								<td>
									<?php
										if($ml_row['ml_menu_type']=="1"){
											echo "Main Menu";
										}else if($ml_row['ml_menu_type']=="2"){
											echo "Sub Menu";
										}else if($ml_row['ml_menu_type']=="3"){
											echo "Hidden Menu";
										}
									?>
								</td> 
								<td> <?php echo $ml_row['ml_name'];?> </td> 
								<td> <?php echo $ml_row['ml_slug'];?> </td>   
								<td>
									<a class="btn mb-1 btn-flat btn-outline-primary btn-sm" href="<?php echo base_url();?>menus_ctrl/menus_list/<?php echo $ml_row['ml_id'];?>"> <i class="fas fa-edit" aria-hidden="true"></i> </a> 
 
									<a class="btn mb-1 btn-flat btn-outline-danger btn-sm" href="<?php echo base_url();?>menus_ctrl/delete_menu/<?php echo $ml_row['ml_id'];?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a> 
												
												
								</td>
							</tr>
					<?php
						}
					?>
					<?php 
						foreach($ml_cli_list as $ml_cli_key=>$ml_cli_row){
							$sl_zno = $sl_zno+1;
					?>
							<tr>
								<td><?php echo $sl_zno;?></td>
								<td>
									<?php
										if($ml_cli_row['ml_menu_type']=="1"){
											echo "Main Menu";
										}else if($ml_cli_row['ml_menu_type']=="2"){
											echo "Sub Menu";
										}else if($ml_cli_row['ml_menu_type']=="3"){
											echo "Hidden Menu";
										}
									?>
								</td> 
								<td> <?php echo $ml_cli_row['ml_name'];?> </td> 
								<td> <?php echo $ml_cli_row['ml_slug'];?> </td>   
								<td>
									<a class="btn mb-1 btn-flat btn-outline-primary btn-sm" href="<?php echo base_url();?>menus_ctrl/menus_list/<?php echo $ml_cli_row['ml_id'];?>"> <i class="fas fa-edit" aria-hidden="true"></i> </a> 
 
									<a class="btn mb-1 btn-flat btn-outline-danger btn-sm" href="<?php echo base_url();?>menus_ctrl/delete_menu/<?php echo $ml_cli_row['ml_id'];?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a> 
												
												
								</td>
							</tr>
					<?php
						}
					?>
					<?php 
						foreach($ml_cus_list as $ml_key=>$ml_cus_row){
							$sl_zno = $sl_zno+1;
					?>
							<tr>
								<td><?php echo $sl_zno;?></td>
								<td>
									<?php
										if($ml_cus_row['ml_menu_type']=="1"){
											echo "Main Menu";
										}else if($ml_cus_row['ml_menu_type']=="2"){
											echo "Sub Menu";
										}else if($ml_cus_row['ml_menu_type']=="3"){
											echo "Hidden Menu";
										}
									?>
								</td> 
								<td> <?php echo $ml_cus_row['ml_name'];?> </td> 
								<td> <?php echo $ml_cus_row['ml_slug'];?> </td>   
								<td>
									<a class="btn mb-1 btn-flat btn-outline-primary btn-sm" href="<?php echo base_url();?>menus_ctrl/menus_list/<?php echo $ml_cus_row['ml_id'];?>"> <i class="fas fa-edit" aria-hidden="true"></i> </a> 
 
									<a class="btn mb-1 btn-flat btn-outline-danger btn-sm" onclick="You want delete this item??" href="<?php echo base_url();?>menus_ctrl/delete_menu/<?php echo $ml_cus_row['ml_id'];?>"> <i class="fa fa-trash" aria-hidden="true"></i> </a> 
												
												
								</td>
							</tr>
					<?php
						}
					?>
					
				</tbody>
				<!-- <tfoot>
					<tr>
						<th>Rendering engine</th>
						<th>Browser</th>
						<th>Platform(s)</th>
						<th>Engine version</th>
						<th>CSS grade</th>
					</tr>
				</tfoot> -->
			</table>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>

 
 <script type="text/javascript">
	var up_status = '';
			
	$(document).ready(function(){
		$('.fileupload').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_slider_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename1').html('');
					$('.filename1').val(data.result.file); 					
					$('.label_f_name').text(data.result.file);
					$('.pro_id_shop').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				} else{
					$('.pro_id_shop').html('');				
					$('.pro_id_shop').append('Try again!..');
				}
				$('.pro_id_shop').html('');	
			}
		});
		  
		$('.fileupload_sub').fileupload({
			url: "<?php echo base_url();?>Img_upload_ctrl/upload_slider_image",
			dataType: 'json',
			send: function(e, data) {
				$('.pro_sub_id_shop').html('<img src="<?php echo base_url();?>public_html/admin_assets/loading_gif.gif" width="20" style="float: right; margin-top: 7px;">');
			},
			done: function (e, data) { 				
				up_status = '';		
				if(data.result.status==true){					
					up_status='true';
					$('.filename2').html('');
					$('.filename2').val(data.result.file); 					
					$('.label_sub_f_name').text(data.result.file);
					$('.pro_sub_id_shop').html(''); 					
					 					
				}else if(data.result.status==false){
					$('.pro_sub_id_shop').html('');				
					$('.pro_sub_id_shop').append('Try again!..');
				} else{
					$('.pro_sub_id_shop').html('');				
					$('.pro_sub_id_shop').append('Try again!..');
				}
				$('.pro_sub_id_shop').html('');	
			}
		});
	});
			
	function move(id,p_id) {
		//if(up_status=='true'){
			$('#'+id+'').css('display','block');
			var elem = document.getElementById(''+id+''); 
			var width = 1;
			var id = setInterval(frame, 10);
			function frame() {
				if (width >= 100) {
					clearInterval(id);
				} else {
					width++; 
					elem.style.width = width + '%'; 
					$('#'+p_id+'').text('');
					$('#'+p_id+'').text(width+'%');
				}
			}
		//}		
	}

	 
</script>