<link rel="stylesheet" href="<?php echo base_url();?>public_html/admin_assets/added_plugins/dragArrange/drag_arranges.css">
<style>
    body {
        font-family: arial;
        background: rgb(242, 244, 246);
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
    }

    h3 {
        color: rgb(199, 204, 209);
        font-size: 28px;
        text-align: center;
    }

    #elements-container {
        text-align: center;
    }

    .draggable-element {
        display: inline-block;
        width: 200px;
        height: 200px;
        background: white;
        border: 1px solid rgb(196, 196, 196);
        line-height: 200px;
        text-align: center;
        margin: 10px;
        color: rgb(51, 51, 51);
        font-size: 30px;
        cursor: move;
    }

    .drag-list {
        width: 400px;
        margin: 0 auto;
    }

    .drag-list > li {
        list-style: none;
        background: rgb(255, 255, 255);
        border: 1px solid rgb(196, 196, 196);
        margin: 5px 0;
        font-size: 24px;
    }

    .drag-list .title {
        display: inline-block;
        width: 130px;
        padding: 6px 6px 6px 12px;
        vertical-align: top;
    }

    .drag-list .drag-area {
        display: inline-block;
        background: rgb(158, 211, 179);
        width: 60px;
        height: 40px;
        vertical-align: top;
        float: right;
        cursor: move;
    }

    .code {
        background: rgb(255, 255, 255);
        border: 1px solid rgb(196, 196, 196);
        width: 600px;
        margin: 22px auto;
        position: relative;
    }

    .code::before {
        content: 'Code';
        background: rgb(80, 80, 80);
        width: 96%;
        position: absolute;
        padding: 8px 2%;
        color: rgb(255, 255, 255);
    }
      
    .code pre {
        margin-top: 50px;
        padding: 0 13px;
        font-size: 1em;
    }

</style>
<script src="<?php echo base_url();?>public_html/admin_assets/added_plugins/dragArrange/drag-arrange.js"></script>

 
<div class="col-12">	
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"> Menu 's List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<section class="showcase showcase-2">
			 
				<ul class="drag-list">
					*************************************
				</ul>
                <a class="btn btn-primary change_btn"> change</a>
			</section>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>

<script type="text/javascript">
      $(function() {
      });
	  $(document).ready(function(){
		$('.li_cls').arrangeable({
			dragSelector: '.drag-area' 
		});		    
		$('.change_btn').on('click',function(){ 
		  	$('.li_cls').each(function(i,n){
				this_pos = i+1;
				$('.pos_'+i+'').html(''); 
				$('.pos_'+i+'').html(this_pos); 
				console.log('.pos_'+i+'');
			});
		}); 
	  });
</script>



  