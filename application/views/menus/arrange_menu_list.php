
<div class="col-12">	
	<div class="card">
		<div class="card-header">
			<h3 class="card-title"> Menu 's List</h3>
		</div>
		<!-- /.card-header -->
		<div class="card-body">
			<section class="showcase showcase-2">
          <div class="col-md-6"> 
                
              <form class="form_submit"  method="POST" action="<?php echo base_url()?>menus_ctrl/arrange_menu_details" > 
                  <ol class='nested_with_switch vertical'>
                      <?php 
                          $sl_no = 1; 
                          foreach($ml_list as $ml_key=>$ml_row){ 
                      ?>
                              <li class="parent_li_cls_<?php echo $sl_no;?>" data-this_val="<?php echo $sl_no;?>" data-ml_id="<?php echo $ml_row['ml_id'];?>">
                                  <p class="menu_p_cls"><?php echo $ml_row['ml_name'];?> </p>
                                  <input type="hidden" name="ml_parent[]" class="input_cls_<?php echo $ml_row['ml_id'];?>" value="<?php echo $ml_row['ml_parent_ml_id'];?>_<?php echo $ml_row['ml_id'];?>">
                                  <ol class="child_ol_cls_<?php echo $sl_no;?>"  data-this_main_li_val="<?php echo $sl_no;?>" data-parent_ml_id="<?php echo $ml_row['ml_parent_ml_id'];?>_<?php echo $ml_row['ml_id'];?>">
                                      <?php  
                                          $sl_no =$sl_no+1;                                          
                                          $submenu1 	= $this->admin_model->get_menu_details($ml_row['ml_id'],'','1'); 
                                            foreach($submenu1 as $sub1_key=>$sub1_row){
                                      ?>

                                                <li class="parent_li_cls_<?php echo $sl_no;?>" data-this_val="<?php echo $sl_no;?>" data-ml_id="<?php echo $sub1_row['ml_id'];?>">
                                                <p class="menu_p_cls"><?php echo $sub1_row['ml_name'];?> </p>
                                                <input type="hidden" name="ml_parent[]" class="input_cls_<?php echo $sub1_row['ml_id'];?>" value="<?php echo $ml_row['ml_main_menu_id'].'_'.$sub1_row['ml_parent_ml_id'].'_'.$sub1_row['ml_id'];?>">
                                                <ol class="child_ol_cls_<?php echo $sl_no;?>"  data-this_main_li_val="<?php echo $sl_no;?>" data-parent_ml_id="<?php echo $sub1_row['ml_parent_ml_id'];?>_<?php echo $sub1_row['ml_id'];?>">
                                                    <?php  
                                                        $sl_no =$sl_no+1;                                          
                                                        $submenu2 	= $this->admin_model->get_menu_details($ml_row['ml_id'],$sub1_row['ml_id'],'2'); 
                                                          foreach($submenu2 as $sub2_key=>$sub2_row){
                                                    ?>

                                                              <li class="parent_li_cls_<?php echo $sl_no;?>" data-this_val="<?php echo $sl_no;?>" data-ml_id="<?php echo $sub2_row['ml_id'];?>">
                                                              <p class="menu_p_cls"><?php echo $sub2_row['ml_name'];?> </p>
                                                              <input type="hidden" name="ml_parent[]" class="input_cls_<?php echo $sub2_row['ml_id'];?>" value="<?php echo $ml_row['ml_main_menu_id'].'_'.$sub1_row['ml_parent_ml_id'].'_'.$sub2_row['ml_child_p_ml_id'].'_'.$sub2_row['ml_id'];?>">
                                                              <ol class="child_ol_cls_<?php echo $sl_no;?>"  data-this_main_li_val="<?php echo $sl_no;?>" data-parent_ml_id="<?php echo $sub2_row['ml_parent_ml_id'];?>_<?php echo $sub2_row['ml_id'];?>">
                                                              </ol>
                                                    <?php        
                                                        }  		 
                                                        
                                                    ?>
                                                </ol>
                                      <?php        
                                          }  		 
                                          
                                      ?>
                                  </ol>
                              </li>    
                      <?php 
                              $sl_no =$sl_no+1;
                          } 	
                      ?>
                        
                  </ol>
                  <button type="submit" class="btn btn-primary change_btn"> Save</button>
                    
              </form>
          </div>
			</section>
		</div>
		<!-- /.card-body -->
	</div>
	<!-- /.card -->
</div>
 
<script>       
  var oldContainer;
  $("ol.nested_with_switch").sortable({
    group: 'nested',
    afterMove: function (placeholder, container) {
    //   console.log("1111");
      // console.log(container);
      if(oldContainer != container){
        if(oldContainer)
          oldContainer.el.removeClass("active");
        container.el.addClass("active");

        oldContainer = container;
      }
    },
    onDrop: function ($item, container, _super) {
      this_cls = $item['0']['classList'][0];                                                   
      console.log("222");
      this_ml_id = $("."+this_cls+"").attr('data-ml_id');
      console.log(this_ml_id);
      
      parent_id = $("."+this_cls+"").parent().attr('data-parent_ml_id');
      console.log(parent_id);
      
      main_li_val = $("."+this_cls+"").parent().attr('data-this_main_li_val');
      console.log(main_li_val);
      if(typeof(parent_id)!='undefined' && parent_id!== null){
        str_m = parent_id.split('_'); 
        get_p_input_val = $('.input_cls_'+str_m[1]+'').val(); 
      }  
      $('.input_cls_'+this_ml_id+'').val('');   
      
      if(typeof(get_p_input_val)!='undefined' && get_p_input_val!== null){
          $('.input_cls_'+this_ml_id+'').val(''+get_p_input_val+'_'+this_ml_id+''); 

            console.log(''+get_p_input_val+'_'+this_ml_id+'');
          
      }else{
            $('.input_cls_'+this_ml_id+'').val(''+parent_id+'_'+this_ml_id+'');           
      }

      if(typeof(parent_id)=='undefined' && parent_id== null){ 
          $('.input_cls_'+this_ml_id+'').val('0_'+this_ml_id+'');            
      }
     
      
      container.el.removeClass("active");
      _super($item, container);
    }
  });

  $(".switch-container").on("click", ".switch", function  (e) {
    console.log("333");
    
    var method = $(this).hasClass("active") ? "enable" : "disable";
    $(e.delegateTarget).next().sortable(method);
  });     
</script>            