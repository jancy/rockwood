<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller']                = 'site_ctrl/index'; 
$route['admin/login']                       = 'admin_login_ctrl/index';  
$route['admin']                             = 'admin_login_ctrl/index';   
$route['admin_login_ctrl/submit']           = 'admin_login_ctrl/submit';   
$route['admin_login_ctrl/index']            = 'admin_login_ctrl/index';   
$route['admin_login_ctrl/logout']            = 'admin_login_ctrl/logout';
   
$route['Img_upload_ctrl/upload_slider_image']            = 'Img_upload_ctrl/upload_slider_image';   
$route['Img_upload_ctrl/upload_content_image']            = 'Img_upload_ctrl/upload_content_image';   
$route['Img_upload_ctrl/upload_gallery_image']            = 'Img_upload_ctrl/upload_gallery_image'; 

$route['menus_list']                       = 'Menus_ctrl/menus_list';   
$route['Menus_ctrl/menus_list/(:any)']     = 'Menus_ctrl/menus_list/$1';      
 
$route['Page_ctrl/create_page']                     = 'Page_ctrl/create_page';   
$route['Page_ctrl/get_menu_data']                   = 'Page_ctrl/get_menu_data';   
$route['Page_ctrl/get_menu_data_by_id']             = 'Page_ctrl/get_menu_data_by_id';   
$route['Page_ctrl/delete_page_details']             = 'Page_ctrl/delete_page_details';   
$route['Page_ctrl/add_slider_details']              = 'Page_ctrl/add_slider_details';   
$route['Page_ctrl/add_content_details']             = 'Page_ctrl/add_content_details';   
$route['Page_ctrl/add_gallery_details']             = 'Page_ctrl/add_gallery_details';   
$route['Page_ctrl/add_video_url_details']           = 'Page_ctrl/add_video_url_details';   

$route['index'] = 'site_ctrl/index';
$route['(:any)'] = 'site_ctrl/page_loading/$1';
$route['products/(:any)'] = 'site_ctrl/page_loading/$1/$2';
$route['products/(:any)/(:any)'] = 'site_ctrl/page_loading/$1/$2/$3';
$route['projects/(:any)'] = 'site_ctrl/page_details/$1/$2';
$route['post/(:any)'] = 'site_ctrl/post_details/$1/$2';
// $route['not_found_page'] = 'site_ctrl/error_page';

$route['404_override'] = 'site_ctrl/error_page';
$route['translate_uri_dashes'] = FALSE;
