-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 25, 2022 at 01:21 AM
-- Server version: 10.3.34-MariaDB-log-cll-lve
-- PHP Version: 7.3.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ruwi1tech3_rockwood`
--

-- --------------------------------------------------------

--
-- Table structure for table `gallery_list`
--

CREATE TABLE `gallery_list` (
  `gl_id` int(11) UNSIGNED NOT NULL,
  `gl_ml_id` int(11) NOT NULL,
  `gl_gtl_id` int(11) NOT NULL,
  `gl_img` varchar(500) NOT NULL,
  `gl_desc` varchar(500) DEFAULT NULL,
  `gl_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive  , 1- active',
  `gl_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_list`
--

INSERT INTO `gallery_list` (`gl_id`, `gl_ml_id`, `gl_gtl_id`, `gl_img`, `gl_desc`, `gl_active`, `gl_created`) VALUES
(1, 5, 1, 'gallery_img_r9k1rx.jpg?t=r9k1rx', NULL, '1', '2022-03-30 05:41:59'),
(2, 5, 2, 'gallery_img_r9k1sy.jpg?t=r9k1sy', NULL, '1', '2022-03-30 05:42:36'),
(3, 25, 3, 'gallery_img_r9k38s.jpg?t=r9k38s', NULL, '1', '2022-03-30 06:15:07'),
(4, 25, 3, 'gallery_img_r9k390.jpg?t=r9k390', NULL, '1', '2022-03-30 06:15:07'),
(5, 25, 3, 'gallery_img_r9k397.jpg?t=r9k397', NULL, '1', '2022-03-30 06:15:07'),
(6, 25, 3, 'gallery_img_r9k39c.jpg?t=r9k39c', NULL, '1', '2022-03-30 06:15:07'),
(7, 25, 3, 'gallery_img_r9k39i.jpg?t=r9k39i', NULL, '1', '2022-03-30 06:15:07'),
(8, 25, 3, 'gallery_img_r9k39u.jpg?t=r9k39u', NULL, '1', '2022-03-30 06:15:07'),
(9, 25, 3, 'gallery_img_r9k3a0.jpg?t=r9k3a0', NULL, '1', '2022-03-30 06:15:07'),
(10, 25, 3, 'gallery_img_r9k3a9.jpg?t=r9k3a9', NULL, '1', '2022-03-30 06:15:07'),
(11, 25, 3, 'gallery_img_r9k3am.jpg?t=r9k3am', NULL, '1', '2022-03-30 06:15:07'),
(12, 26, 4, 'gallery_img_r9k3iq.jpg?t=r9k3iq', NULL, '1', '2022-03-30 06:20:12'),
(13, 26, 5, 'gallery_img_r9k3kp.jpg?t=r9k3kp', NULL, '1', '2022-03-30 06:20:51'),
(14, 26, 6, 'gallery_img_r9k3le.jpg?t=r9k3le', NULL, '1', '2022-03-30 06:21:29'),
(15, 26, 7, 'gallery_img_r9k3mi.jpg?t=r9k3mi', NULL, '1', '2022-03-30 06:22:08'),
(16, 26, 8, 'gallery_img_r9k3nk.jpg?t=r9k3nl', NULL, '1', '2022-03-30 06:22:48'),
(17, 26, 9, 'gallery_img_r9k3ox.jpg?t=r9k3ox', NULL, '1', '2022-03-30 06:23:23'),
(18, 27, 10, 'gallery_img_r9lnpu.jpg?t=r9lnpu', NULL, '1', '2022-03-31 02:33:59'),
(19, 27, 10, 'gallery_img_r9lnq0.jpg?t=r9lnq0', NULL, '1', '2022-03-31 02:33:59'),
(20, 27, 10, 'gallery_img_r9lnq6.jpg?t=r9lnq6', NULL, '1', '2022-03-31 02:33:59'),
(21, 27, 10, 'gallery_img_r9lnqd.jpg?t=r9lnqd', NULL, '1', '2022-03-31 02:33:59'),
(22, 28, 11, 'gallery_img_r9lwyb.jpg?t=r9lwyb', NULL, '1', '2022-03-31 05:53:39'),
(23, 28, 11, 'gallery_img_r9lwyi.jpg?t=r9lwyi', NULL, '1', '2022-03-31 05:53:39'),
(24, 28, 11, 'gallery_img_r9lwyp.jpg?t=r9lwyp', NULL, '1', '2022-03-31 05:53:39'),
(25, 28, 11, 'gallery_img_r9lwyt.jpg?t=r9lwyt', NULL, '1', '2022-03-31 05:53:39'),
(26, 28, 11, 'gallery_img_r9lwyy.jpg?t=r9lwyy', NULL, '1', '2022-03-31 05:53:39'),
(27, 29, 12, 'gallery_img_r9lx2j.jpg?t=r9lx2j', NULL, '1', '2022-03-31 05:57:01'),
(28, 29, 12, 'gallery_img_r9lx2p.jpg?t=r9lx2p', NULL, '1', '2022-03-31 05:57:01'),
(29, 29, 12, 'gallery_img_r9lx2w.jpg?t=r9lx2w', NULL, '1', '2022-03-31 05:57:01'),
(30, 29, 12, 'gallery_img_r9lx32.jpg?t=r9lx32', NULL, '1', '2022-03-31 05:57:01'),
(31, 29, 12, 'gallery_img_r9lx39.jpg?t=r9lx39', NULL, '1', '2022-03-31 05:57:01'),
(32, 27, 13, 'gallery_img_r9lx68.jpg?t=r9lx68', NULL, '1', '2022-03-31 05:59:36'),
(33, 27, 13, 'gallery_img_r9lx6b.jpg?t=r9lx6b', NULL, '1', '2022-03-31 05:59:36'),
(34, 27, 13, 'gallery_img_r9lx6e.jpg?t=r9lx6e', NULL, '1', '2022-03-31 05:59:36'),
(35, 27, 13, 'gallery_img_r9lx6i.jpg?t=r9lx6i', NULL, '1', '2022-03-31 05:59:36'),
(36, 27, 13, 'gallery_img_r9lx6l.jpg?t=r9lx6l', NULL, '1', '2022-03-31 05:59:36'),
(37, 27, 13, 'gallery_img_r9lx6o.jpg?t=r9lx6o', NULL, '1', '2022-03-31 05:59:36'),
(38, 27, 13, 'gallery_img_r9lx6s.jpg?t=r9lx6s', NULL, '1', '2022-03-31 05:59:36'),
(39, 20, 14, 'gallery_img_r9vhql.jpg?t=r9vhql', NULL, '1', '2022-04-05 10:00:46'),
(40, 20, 14, 'gallery_img_r9vhqy.jpg?t=r9vhqy', NULL, '1', '2022-04-05 10:00:46'),
(41, 20, 15, 'gallery_img_r9vhs3.jpg?t=r9vhs3', NULL, '1', '2022-04-05 10:01:26'),
(42, 20, 15, 'gallery_img_r9vhs7.jpg?t=r9vhs7', NULL, '1', '2022-04-05 10:01:26'),
(43, 20, 16, 'gallery_img_r9vhsz.jpg?t=r9vhsz', NULL, '1', '2022-04-05 10:01:50'),
(44, 20, 17, 'gallery_img_r9vhue.jpg?t=r9vhue', NULL, '1', '2022-04-05 10:02:42');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_title_list`
--

CREATE TABLE `gallery_title_list` (
  `gtl_id` int(10) UNSIGNED NOT NULL,
  `gtl_ml_id` int(11) NOT NULL,
  `gtl_title` varchar(500) NOT NULL,
  `gtl_i_img` varchar(500) DEFAULT NULL,
  `gtl_desc` text DEFAULT NULL,
  `gtl_content` text DEFAULT NULL,
  `gtl_content2` text DEFAULT NULL,
  `gtl_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery_title_list`
--

INSERT INTO `gallery_title_list` (`gtl_id`, `gtl_ml_id`, `gtl_title`, `gtl_i_img`, `gtl_desc`, `gtl_content`, `gtl_content2`, `gtl_created`) VALUES
(1, 5, 'Ink mark Labeling', NULL, '', '										\r\n										Ink mark Labeling for your packages  on the carton and wooden packings.', '											<div><br></div>\r\n										', '2022-03-30 05:41:59'),
(2, 5, 'Engraving', NULL, NULL, 'Engraving for spare parts, finished goods, made in country as well as the product brand name.', NULL, '2022-03-30 05:42:35'),
(3, 25, 'clients', NULL, NULL, NULL, NULL, '2022-03-30 06:15:06'),
(4, 26, 'Advanced Technologies', NULL, NULL, NULL, NULL, '2022-03-30 06:20:12'),
(5, 26, '1000+ Satisfied Clients', NULL, NULL, NULL, NULL, '2022-03-30 06:20:51'),
(6, 26, 'Trusted Q&A Partner', NULL, NULL, NULL, NULL, '2022-03-30 06:21:29'),
(7, 26, 'Experts With Experience', NULL, NULL, NULL, NULL, '2022-03-30 06:22:08'),
(8, 26, 'Par Excellence Service', NULL, NULL, NULL, NULL, '2022-03-30 06:22:48'),
(9, 26, 'Customer-First Approach', NULL, NULL, NULL, NULL, '2022-03-30 06:23:23'),
(10, 27, 'Fabtech', 'gallery_img_rablpf.jpg?t=rablpf', 'Fabtech International Group of companies is one of the largest and most technically capable steel fabricators in the Middle East. Founded in 1994 based on the vision  ....', '<p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold;\">Fabtech International&nbsp;</strong>Group of companies is one of the largest and most technically capable steel fabricators in the Middle East. Founded in 1994 based on the vision of its founder Dr. Harry Moraes, the Group has grown dynamically and has consistently delivered in order to earn its reputation for exceeding expectations of clients in both manufacturing and product excellence.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">Headquartered in Dubai and with strategic locations for operations across the globe, Fabtech provides a full range of services to the oil, gas, power, water and other process industries. Our range of services has been developed by creating specialist divisions in oil&nbsp;&nbsp; gas, pressure vessels, rolling&nbsp;&nbsp; dished ends, material handling, specialized engineering, construction, coating, grating, tanks and trading.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">As an ISO:9001 certified company and accredited by all major institutions the Group consists of experienced specialists that continuously strive to enhance the value proposition to clients as well as the firm vision of consistent reinvestment to provide state-of-the-art facilities in our offering. In Jebel Ali Free Zone, Dubai alone the Group boasts manufacturing facilities of over 330,000 m2, including 80,000 m2 of enclosed shops.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">Fabtech has established itself as a viable manufacturing and business partner for both EPC contractors and end users alike and has successfully delivered projects to virtually every manufacturing segment on both hemispheres of the globe.</p>', '											<ul>\r\n												<li><font color=\"#000000\">Location: <span>Dubai, UAE</span></font></li>\r\n												<li><font color=\"#000000\">Company website: <span><a class=\"text-second\" href=\"www.fabtechint.com\" target=\"_blank\">www.fabtechint.com</a></span></font></li>\r\n												<li><font color=\"#000000\">Machines Installed: <span class=\"tm-project-detail-list\">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton&nbsp;</span></font></li>									\r\n											</ul>\r\n										', '2022-03-31 02:33:59'),
(11, 28, 'EGA', 'gallery_img_rablv0.jpg?t=rablv0', 'EGA is one of the world’s largest ‘premium aluminum’ producers and the biggest industrial company in the UAE outside oil and gas....', '<p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold;\">EGA</strong>&nbsp;is one of the world’s largest ‘premium aluminum’ producers and the biggest industrial company in the UAE outside oil and gas<strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold;\">.</strong></p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">EGA operates aluminum smelters in Abu Dhabi and Dubai and is developing an alumina refinery in the UAE and a bauxite mine and associated export facilities in the Guinea. Our production capacity is 2.5 million tons of aluminum per year.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">EGA has focused on innovation for over 25 years. We have used our own technology in every smelter expansion since the 1990s and retrofitted our older production lines. In 2016 we became the first UAE industrial company to license our process technology internationally.</p>										\r\n										', '											<ul>\r\n												<li><font color=\"#000000\">Location: <span>Dubai, UAE</span></font></li>\r\n												<li><font color=\"#000000\">Company website: www.ega.ae</font></li>\r\n												<li><font color=\"#000000\">Machines Installed: <span class=\"tm-project-detail-list\">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></font></li>									\r\n											</ul>\r\n										', '2022-03-31 05:53:39'),
(12, 29, 'FLYDUBAI', 'gallery_img_rablxa.jpg?t=rablxa', 'Rockwood international is very proud to associate with FLYDUBAI to supply the machine for their flight maintenance facility sheet metal shop.', '<span style=\"color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">Rockwood international is very proud to associate with&nbsp;</span><strong style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">FLYDUBAI</strong><span style=\"color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">&nbsp;to supply the machine for their flight maintenance facility sheet metal shop. The engineering facility is located at the north side of Dubai International Airport, with a footprint of approximately 3,326 square meters and a built-up area of around 7,884 square meters, to handle the maintenance requirements of fly Dubai. The facility consists of ground, mezzanine and first floors that house main storage, engine storage, engine cleaning, interiors shop, sheet metal shop, avionics shop, cleaning bay, office space, prayer room, locker room, break room, meeting room and nitrogen plant.</span>', '											<ul>\r\n												<li><font color=\"#000000\">Location: <span>Dubai, UAE</span></font></li>\r\n												<li><font color=\"#000000\">Company website: www.flydubai.com</font></li>\r\n												<li><font color=\"#000000\">Machines Installed: CNC Shearing Machine, Folding Machine , Plate Rolling Machine, Portable Bandsaw</font></li>									\r\n											</ul>\r\n										', '2022-03-31 05:57:01'),
(13, 27, 'ABJ Engineering', 'gallery_img_rablth.jpg?t=rablth', 'ABJ Engineering & Contracting Co. KSC, is a wholly owned subsidiary of Kharafi National, is an expanding global company, commencing its .....', '<strong style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">ABJ Engineering &amp; Contracting Co. KSC,&nbsp;</strong><span style=\"color: rgb(97, 98, 99); font-family: Roboto; text-align: justify;\">is a wholly owned subsidiary of Kharafi National, is an expanding global company, commencing its journey in 1993 and is now at the leading edge of fabrication technology. From sophisticated design to precision manufacturing, the Company offers a diversified range of products such as Pressure Vessels, Columns/Towers, Process Equipment Modules, Modularized Industrial Structures like Pipe racks, Piping &amp; Valve Skids, Shell &amp; Tube Heat Exchangers, Packaged Evaporator &amp; Deaerator Units for Desalination Plants, Pre-fabricated Pipe spools, Shop fabricated Tanks, Architectural Roof Structures for International Stadiums.</span>										\r\n										', '											<ul>\r\n												<li>Location: Kuwait</li>\r\n												<li>Company website: www.abjengineering.com</li>\r\n												<li>Machines Installed: Welding Roller, Circle Burner And Nozzle Welder</li>									\r\n											</ul>\r\n										', '2022-03-31 05:59:36'),
(14, 20, 'For Plates', NULL, '									Short desciption here ....\r\n								', '										\r\n										', '											<ul>\r\n												<li>Location: <span>Dubai, UAE</span></li>\r\n												<li>Company website: <span><a class=\"text-second\" href=\"www.fabtechint.com\" target=\"_blank\">www.fabtechint.com</a></span></li>\r\n												<li>Machines Installed: <span class=\"tm-project-detail-list\">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></li>									\r\n											</ul>\r\n										', '2022-04-05 10:00:46'),
(15, 20, 'For Pipes: Manual and Auto Pipe Cutting Machine', NULL, '									Short desciption here ....\r\n								', '										\r\n										', '											<ul>\r\n												<li>Location: <span>Dubai, UAE</span></li>\r\n												<li>Company website: <span><a class=\"text-second\" href=\"www.fabtechint.com\" target=\"_blank\">www.fabtechint.com</a></span></li>\r\n												<li>Machines Installed: <span class=\"tm-project-detail-list\">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></li>									\r\n											</ul>\r\n										', '2022-04-05 10:01:25'),
(16, 20, 'Beam Cutting Machine', NULL, '									Short desciption here ....\r\n								', '										\r\n										', '											<ul>\r\n												<li>Location: <span>Dubai, UAE</span></li>\r\n												<li>Company website: <span><a class=\"text-second\" href=\"www.fabtechint.com\" target=\"_blank\">www.fabtechint.com</a></span></li>\r\n												<li>Machines Installed: <span class=\"tm-project-detail-list\">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></li>									\r\n											</ul>\r\n										', '2022-04-05 10:01:50'),
(17, 20, 'Outside Plate Profile Cutting Machine', NULL, '									Short desciption here ....\r\n								', '										\r\n										', '											<ul>\r\n												<li>Location: <span>Dubai, UAE</span></li>\r\n												<li>Company website: <span><a class=\"text-second\" href=\"www.fabtechint.com\" target=\"_blank\">www.fabtechint.com</a></span></li>\r\n												<li>Machines Installed: <span class=\"tm-project-detail-list\">CNC Plate Rolling, High Speed Dishing and Flanging Machine 2000 Ton</span></li>									\r\n											</ul>\r\n										', '2022-04-05 10:02:41');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `l_id` int(11) NOT NULL,
  `i_user_id` int(11) NOT NULL,
  `i_type` enum('0','1') NOT NULL,
  `l_user` varchar(500) NOT NULL,
  `l_user_name` varchar(500) NOT NULL,
  `l_pwd` varchar(500) NOT NULL COMMENT 'adminpwd : admin_123',
  `l_status` smallint(3) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`l_id`, `i_user_id`, `i_type`, `l_user`, `l_user_name`, `l_pwd`, `l_status`) VALUES
(1, 0, '0', 'admin@123.com', 'admin_user', 'd6bf4bb9a66419380a7e8b034270d381', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menus_list`
--

CREATE TABLE `menus_list` (
  `ml_id` int(11) NOT NULL,
  `ml_menu_position` int(11) NOT NULL DEFAULT 0,
  `ml_main_menu_id` int(11) NOT NULL DEFAULT 1,
  `ml_parent_ml_id` int(11) DEFAULT 0 COMMENT 'parent menu id',
  `ml_child_p_ml_id` int(11) DEFAULT 0 COMMENT 'child parent id',
  `ml_name` varchar(500) DEFAULT NULL,
  `ml_slug` varchar(500) DEFAULT NULL,
  `ml_title` varchar(500) DEFAULT NULL,
  `ml_m_title` varchar(500) DEFAULT NULL,
  `ml_desc` text DEFAULT NULL,
  `ml_keyword` text DEFAULT NULL,
  `ml_s_desc` varchar(500) DEFAULT NULL,
  `ml_menu_type` enum('1','2','3') DEFAULT '2' COMMENT '1 - main menu ,2 - sub menu , 3-hiden menu',
  `ml_img` varchar(500) DEFAULT NULL,
  `ml_sub_img` varbinary(500) DEFAULT NULL,
  `ml_type` enum('home','project','services','products','blog','clients','Gallery','productsub','productdetails','about-us','rentals','contact-us') DEFAULT NULL,
  `ml_active` enum('0','1') DEFAULT NULL COMMENT '0 - not acive ,1 -active',
  `ml_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus_list`
--

INSERT INTO `menus_list` (`ml_id`, `ml_menu_position`, `ml_main_menu_id`, `ml_parent_ml_id`, `ml_child_p_ml_id`, `ml_name`, `ml_slug`, `ml_title`, `ml_m_title`, `ml_desc`, `ml_keyword`, `ml_s_desc`, `ml_menu_type`, `ml_img`, `ml_sub_img`, `ml_type`, `ml_active`, `ml_created`) VALUES
(1, 1, 0, 0, 0, 'Home', 'home', 'Home', NULL, 'home', 'home', NULL, '1', ' ', NULL, 'home', '1', '2022-03-29 02:10:21'),
(2, 2, 0, 0, 0, 'About Us', 'about-us', 'About Us', NULL, 'about us', 'about us', NULL, '1', 'slider_r9jsro.jpg?t=r9jsrp', NULL, 'about-us', '1', '2022-03-29 02:11:04'),
(3, 3, 0, 0, 0, 'Products', 'products', 'Our Products', 'Our Products', 'products', 'proucts', '  ', '1', 'slider_r9z3e9.jpg?t=r9z3e9 ', 0x20, 'products', '1', '2022-03-29 02:11:46'),
(4, 4, 0, 0, 0, 'Rentals', 'rentals', 'Rentals', NULL, 'rentals', 'rentals', NULL, '1', 'slider_r9k0yc.jpg?t=r9k0yc', NULL, 'rentals', '1', '2022-03-29 02:12:42'),
(5, 5, 0, 0, 0, 'Services', 'services', 'Services', NULL, 'Services', 'Services', NULL, '1', 'slider_r9k1cp.jpg?t=r9k1cp', NULL, 'services', '1', '2022-03-29 02:13:53'),
(6, 6, 0, 0, 0, 'Projects', 'projects', 'Projects', NULL, 'Projects', 'Projects', NULL, '1', 'slider_r9z3dt.jpg?t=r9z3dt', NULL, 'project', '1', '2022-03-29 02:14:37'),
(7, 7, 0, 0, 0, 'Blog', 'blog', 'Blog', NULL, 'Blog', 'Blog', NULL, '1', 'slider_r9z3f7.jpg?t=r9z3f7', NULL, 'blog', '1', '2022-03-29 02:17:54'),
(8, 8, 0, 0, 0, 'Contact Us', 'contact-us', 'Contact Us', NULL, 'Contact Us', 'Contact Us', NULL, '1', 'slider_r9k294.jpg?t=r9k294', NULL, 'contact-us', '1', '2022-03-29 02:18:48'),
(9, 0, 1, 3, 0, 'Cutting & Drilling', 'cutting-crilling', 'Cutting & Drilling', NULL, 'Cutting & Drilling', 'Cutting & Drilling', NULL, '2', 'slider_r9lz1i.jpg?t=r9lz1i ', 0x736c696465725f72397a357a6e2e6a70673f743d72397a357a6e, 'productsub', '1', '2022-03-29 02:23:21'),
(10, 9, 1, 3, 0, 'Notching', 'notching', 'Notching', NULL, 'Notching', 'Notching', NULL, '2', '  ', 0x736c696465725f72397a3578792e6a70673f743d72397a357879, 'products', '1', '2022-03-29 02:23:54'),
(11, 10, 1, 3, 0, 'Edge Preparation', 'Edge-Preparation', 'Edge Preparation', NULL, 'Edge Preparation', 'Edge Preparation', NULL, '2', '', 0x736c696465725f72397a35796a2e6a70673f743d72397a35796a, 'products', '1', '2022-03-29 02:24:20'),
(12, 11, 1, 3, 0, 'Bending & Forming', 'Bending-Forming', 'Bending & Forming', NULL, 'Bending & Forming', 'Bending & Forming', NULL, '2', '', 0x736c696465725f72397a357a6e2e6a70673f743d72397a357a6e, 'products', '1', '2022-03-29 02:25:40'),
(13, 9, 1, 3, 0, 'Welding', 'Welding', 'Welding', NULL, 'Welding', 'Welding', NULL, '2', '  ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:26:07'),
(14, 10, 1, 3, 0, 'Welding Automation', 'Welding-Automation', 'Welding Automation', NULL, 'Welding Automation', 'Welding Automation', NULL, '2', ' ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:26:50'),
(15, 9, 1, 3, 0, 'Marking Solutions', 'Marking-Solutions', 'Marking Solutions', NULL, 'Marking Solutions', 'Marking Solutions', NULL, '2', ' ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:27:57'),
(16, 10, 1, 3, 0, 'Surface Preparation', 'Surface-Preparation', 'Surface Preparation', NULL, 'Surface Preparation', 'Surface Preparation', NULL, '2', ' ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:28:32'),
(17, 11, 1, 3, 0, 'Material Handling', 'Material-Handling', 'Material Handling', NULL, 'Material Handling', 'Material Handling', NULL, '2', ' ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:28:59'),
(18, 12, 1, 3, 0, 'Workshop Machinery', 'Workshop-Machinery', 'Workshop Machinery', NULL, 'Workshop Machinery', 'Workshop Machinery', NULL, '2', ' ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:29:23'),
(19, 13, 1, 3, 0, 'Road & Solar Construction Equipment', 'Road-Solar', 'Road & Solar Construction Equipment', NULL, 'Road & Solar Construction Equipment', 'Road & Solar Construction Equipment', NULL, '2', ' ', 0x736c696465725f72397a3630392e6a70673f743d72397a363039, 'products', '1', '2022-03-29 02:29:55'),
(20, 0, 1, 3, 9, 'Pug Cutting Machines', 'Pug-Cutting-Machines', 'Pug-Cutting-Machines', 'Pug Cutting Machines', 'Pug Cutting Machines', 'Pug Cutting Machines', '    Stud welding machines help in the powerful and reliable methods of applying a single side fastening to a metal component  ', '2', 'slider_r9z7fc.jpg?t=r9z7fc    ', 0x736c696465725f7261366c31372e6a70673f743d7261366c3137, 'products', '1', '2022-03-29 02:30:44'),
(21, 0, 1, 3, 9, 'Portable Bandsaw', 'Portable-Bandsaw', 'Portable Bandsaw', NULL, 'Portable Bandsaw', 'Portable Bandsaw', NULL, '2', 'slider_r9z7fc.jpg?t=r9z7fc ', 0x736c696465725f7261313278322e6a70673f743d726131327832, 'products', '1', '2022-03-29 02:31:12'),
(22, 0, 1, 3, 9, 'Bandsaws', 'Bandsaws', 'Bandsaws', NULL, 'Bandsaws', 'Bandsaws', NULL, '2', 'slider_r9z7fc.jpg?t=r9z7fc ', 0x736c696465725f7261313278322e6a70673f743d726131327832, 'products', '1', '2022-03-29 02:31:41'),
(23, 0, 1, 3, 9, 'CNC Pipe Profile Cutting Machine', 'CNC-Pipe-Profile-Cutting-Machine', 'CNC Pipe Profile Cutting Machine', NULL, 'CNC Pipe Profile Cutting Machine', 'CNC Pipe Profile Cutting Machine', NULL, '2', 'slider_r9z7fc.jpg?t=r9z7fc ', 0x736c696465725f7261313278322e6a70673f743d726131327832, 'products', '1', '2022-03-29 02:32:21'),
(24, 0, 1, 3, 9, 'CNC Joint Plate Drilling Machine', 'CNC-Joint-Plate-Drilling-Machine', 'CNC Joint Plate Drilling Machine', NULL, 'CNC Joint Plate Drilling Machine', 'CNC Joint Plate Drilling Machine', NULL, '2', 'slider_r9z7fc.jpg?t=r9z7fc ', 0x736c696465725f7261313278322e6a70673f743d726131327832, 'products', '1', '2022-03-29 02:47:34'),
(25, 0, 1, 0, 0, 'Clients', 'Clients', 'Clients', NULL, 'Clients', 'Clients', NULL, '3', ' ', NULL, 'clients', '1', '2022-03-30 06:04:48'),
(26, 0, 1, 0, 0, 'OUR AREAS OF EXPERTIZE', 'OUR-AREAS-OF-EXPERTIZE', 'OUR AREAS OF EXPERTIZE', NULL, 'OUR AREAS OF EXPERTIZE', 'OUR AREAS OF EXPERTIZE', NULL, '3', ' ', NULL, 'Gallery', '1', '2022-03-30 06:17:28'),
(27, 0, 1, 0, 0, 'Oil and Gas', 'Oil-and-Gas', 'Oil and Gas', NULL, 'Oil and Gas', 'Oil and Gas', NULL, '3', '  ', NULL, 'project', '1', '2022-03-30 07:45:12'),
(28, 0, 1, 0, 0, 'Manufacturing', 'Manufacturing', 'Manufacturing', NULL, 'Manufacturing', 'Manufacturing', NULL, '3', ' ', NULL, 'project', '1', '2022-03-31 01:57:36'),
(29, 0, 1, 0, 0, 'Turnkey Solutions', 'Turnkey-Solutions', 'Turnkey Solutions', NULL, 'Turnkey Solutions', 'Turnkey Solutions', NULL, '3', ' ', NULL, 'project', '1', '2022-03-31 01:58:00'),
(30, 0, 1, 0, 0, 'Structural Steel', 'Structural-Steel', 'Structural Steel', NULL, 'Structural Steel', 'Structural Steel', NULL, '3', ' ', NULL, 'project', '1', '2022-03-31 01:58:34'),
(31, 0, 1, 0, 0, 'Ship Building', 'Ship-Building', 'Ship Building', NULL, 'Ship Building', 'Ship Building', NULL, '3', ' ', NULL, 'project', '1', '2022-03-31 02:00:27'),
(32, 0, 1, 0, 0, 'customers say', 'customers-say', 'customers say', NULL, 'customers say', 'customers say', NULL, '3', ' ', NULL, '', '1', '2022-04-01 05:36:49');

-- --------------------------------------------------------

--
-- Table structure for table `page_content_list`
--

CREATE TABLE `page_content_list` (
  `pcl_id` int(10) UNSIGNED NOT NULL,
  `pcl_ml_id` int(11) NOT NULL,
  `pcl_title` varchar(500) NOT NULL,
  `pcl_sub_title` varchar(500) DEFAULT NULL,
  `pcl_content` text NOT NULL,
  `pcl_img` varchar(200) DEFAULT NULL,
  `pcl_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive  , 1- active',
  `pcl_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_content_list`
--

INSERT INTO `page_content_list` (`pcl_id`, `pcl_ml_id`, `pcl_title`, `pcl_sub_title`, `pcl_content`, `pcl_img`, `pcl_active`, `pcl_created`) VALUES
(1, 2, 'OUR STORY', '14', '<h2>About Rockwood Machinery</h2>\r\n								<p>Rockwood Machinery is one of the best heavy machinery and industrial equipment providers headquartered in the UAE. We operate alongside our suppliers and employees to help manufacture and build metal structures and systems on a large scale with our experienced team of welders, aluminum fabrication experts, and installation crew.&nbsp;</p><div>If you need an industrial metal and machinery project to be delivered on time and with ease, you have come to the right place. Rockwood Machinery got you covered from even small Fabrication works to Large Structural Manufacturing projects. Our expert team is fully trained and ready to carry your project, no matter what size, from start to finish with precision and transform your needs, ideas and vision into a structural reality.<br></div>', 'content_r9z3p5.jpg?t=r9z3p5', '1', '2022-03-30 05:18:01'),
(2, 2, 'Our values', '', '<p>Since we started our journey 16 years ago, Rockwood Machinery has become a benchmark in providing welding and steel fabrication services, heavy machinery and industrial equipment to businesses across industry verticals in the UAE. Our every goal and action is guided by the principle \"Success is not an accidental or sudden achievement.\" We approach every problem or challenge with the sole aim to come out with solutions and positive lessons. Rockwood International has adopted the path of hard work and discipline to ensure sustained growth. Backed by our focus on quality, this approach has generated remarkable dividends for the company. This is also reflected in the healthy rate of growth recorded by us.</p>', '', '1', '2022-03-30 05:19:18'),
(3, 4, 'ROCKWOOD MACHINERY', 'Need a machine for a short time frame? Rent with us', '<p><strong class=\"text-primary\">Stud Welding Machine, Marking Machine, Welding Rotators &amp; Weld Cleaning Machine</strong> for Rent in <strong>Dubai</strong></p>\r\n							<p>Machine rental is a great option if you Have a short run job or Need extra capacity temporarily or Need a machine for a short time frame or budget constrains for capex buying a machine and Rockwood machinery supports your projects with flexible and cost effective job management and scheduling options by providing equipment our most common machines on rental basis and following is the option of a machines available for rental.</p>', 'content_ra7t3b.jpg?t=ra7t3b', '1', '2022-03-30 05:29:51'),
(4, 5, 'OVERVIEW', 'Industrial Services', 'Product labeling indicating the Made In and brand name are nowadays compulsory for any products to be exported in most of the countries and we are doing the services for', '', '1', '2022-03-30 05:36:03'),
(5, 7, 'Why Automation in Welding is Gaining Importance', 'May 30, 2019', '<p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">Automatic welding is categorized into two: machine-based automatic welding and robotic welding. While the former is extremely beneficial in factory settings for repeated welding requirements, robotic welding is used for advanced industrial purposes which require higher grade of precision welding. Automation is efficient and consistent. More and more industries are adopting automation into its processes owing to its ability to perform an assigned task with the absolute precision until a technician makes changes to the parameters.Market analysis indicates that automated robotic welding in the manufacturing and industrial sectors will play a disruptive role in the next few years.</p><h3 style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 18px; margin-left: 0px; color: rgb(40, 40, 40); font-family: suisse-intl-bold; font-size: 30px;\">Popular Types of Welding</h3><ul style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-left: 18px; list-style-position: initial; list-style-image: initial; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"><li style=\"box-sizing: inherit; line-height: 30px;\">MIG Gas Metal Arc Welding (GMAW)</li><li style=\"box-sizing: inherit; line-height: 30px;\">TIG/Gas Tungsten Arc Welding (GTAW)</li><li style=\"box-sizing: inherit; line-height: 30px;\">Shielded Metal Arc Welding (SMAW) Stick Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Flux core arc (FCAW) Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Plasma Arc Welding (PAW</li><li style=\"box-sizing: inherit; line-height: 30px;\">Electron Beam and Laser Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Gas Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Ultrasonic Welding</li></ul><h3 style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 18px; margin-left: 0px; color: rgb(40, 40, 40); font-family: suisse-intl-bold; font-size: 30px;\">Welding using Automation and Robots</h3><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">When automation is applied to industrial processes like welding, the overall throughput improves, work environment safety increases, and production efficiency is enhanced. Parts and assemblies of machines and other industrial tools done using automated welding rarely have defects and are inform in shape, size and form. Welding for such purposes is done using sensors, program, and logic controllers etc. This helps in controlling and managing specs. Mentioned below are some of the benefits applied to welding applications using robots.</p><ul style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-left: 18px; list-style-position: initial; list-style-image: initial; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"><li style=\"box-sizing: inherit; line-height: 30px;\">Advanced “smart” technology helps collect information about operations, positioning, and wire or electrode status</li><li style=\"box-sizing: inherit; line-height: 30px;\">Software to perform real-time data analysis and adjustment machine movements</li><li style=\"box-sizing: inherit; line-height: 30px;\">Easy configuration and programming, where the technician can perform the movements on the robotic arm once and save and store the process as a program for future purposes</li><li style=\"box-sizing: inherit; line-height: 30px;\">No exposure to shielding gases, heat, and sparks, thereby enhancing operator</li></ul><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">With more and more industries exploring the opportunities of automation, the demand for automated welding machines are also increasing. Welding machines with robotic capabilities are available for rent and sale in Dubai and other major countries.</p><div class=\"industris-space-20 sm-hidden\" style=\"box-sizing: inherit; height: 20px; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"></div><div class=\"single-img\" style=\"box-sizing: inherit; display: table; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"><div class=\"img-left\" style=\"box-sizing: inherit; position: relative; width: 485.087px; float: left; padding-right: 15px;\"><img src=\"https://via.placeholder.com/457x352.png\" alt=\"\" style=\"box-sizing: inherit; border-width: 0px; border-color: initial; border-image: initial; height: auto; max-width: 100%; width: 470.087px;\"></div><div class=\"img-right\" style=\"box-sizing: inherit; position: relative; width: 284.896px; float: left; padding-left: 15px;\"><img src=\"https://via.placeholder.com/260x160.png\" alt=\"\" style=\"box-sizing: inherit; border-width: 0px; border-color: initial; border-image: initial; height: auto; max-width: 100%; margin-bottom: 30px; width: 269.896px;\">&nbsp;<img src=\"https://via.placeholder.com/260x160.png\" alt=\"\" style=\"box-sizing: inherit; border-width: 0px; border-color: initial; border-image: initial; height: auto; max-width: 100%; margin-bottom: 0px; width: 269.896px;\"></div></div><div class=\"industris-space-50\" style=\"box-sizing: inherit; height: 50px; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"></div><div class=\"industris-space-5\" style=\"box-sizing: inherit; height: 5px; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"></div><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">“The business landscape is changing rapidly, which means all types of companies need to consider where they fit in to new digital business processes.” says Bengt Nordström, CEO at Northstream. “With this analysis we also want to highlight the challenge for businesses to face digital transformation on their own – and thus the need to build partnerships with other actors in the ecosystem.”</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">In addition to this latest report, Telenor Connexion has published a number of guides and papers covering topics related to IoT and connected devices. Those include Connectivity Technologies for IoT, a guide which was first created in collaboration with Northstream in 2016 and updated at the end of 2018.</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">That paper, and a number of others, can be found on the Telenor Connexion Knowledge Hub. More papers and guides in this vein will be published in the months to come. Parties interested in Five Predictions for IoT and future publications can contact the marketing team at:<br style=\"box-sizing: inherit;\"><a class=\"text-second\" href=\"http://localhost/rockwood_html/post.html#\" style=\"box-sizing: inherit; transition: all 0.3s linear 0s; color: rgb(0, 174, 239) !important;\">hello@Industris.com</a></p>', 'content_rabrop.jpg?t=rabrop', '1', '2022-03-30 06:31:59'),
(6, 7, 'Everything You Need to Know About Laser Marking Machine', 'March 4, 2022', '<p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">A pipe notching machine is used to prepare tube joints for the welding process. It is a specific procedure done on pipes and tubular square or rectangular tubes in any metallic material, generally iron and steel. Notching is a shearing-related metalworking operation in which undesired material is selectively cut from the outer edges of a workpiece using a punch press. After the workpiece is fixed in place, the punch press uses a cutting tool to remove the undesirable material from the workpiece’s outside edges. As a result, the workpiece is smaller and has a different shape.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Features of Pipe Notching Machine</strong></p><ol style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; outline: 0px; padding: 10px 0px 0px 20px; vertical-align: baseline; list-style-position: inherit; list-style-image: initial; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">User friendly</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">No need to use cutting oil</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Higher resistance to corrosion</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Higher precision rates</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Longer machine lifespan</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Less maintenance costs</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Professional design</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Less metal wastage</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Applicable across metals, and industries.</li></ol><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Applications of the Pipe Notching Machine</strong></p><ul style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; outline: 0px; padding: 0px 0px 0px 20px; vertical-align: baseline; list-style: outside square; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Excellent for Coping round tubes or pipes together handrails</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Extensively used in companies that manufacture furniture, metal gates, electrical appliances</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Gymnasium equipment, hospital equipment, petrochemical plants.</li></ul><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Types of Grinding Machines and Their Features</strong></p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Belt Grinder Highspeed 75 D and 150 D</strong></p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">The MetalWolff High-speed 75-D belt grinder is a high-speed, revolutionary belt grinder. (45 miles per hour). The width of the belt is 75 mm. The Metal Wolff&nbsp;&nbsp;High-speed 150-D belt grinder is a high-speed, revolutionary belt grinder. The belt is 150 mm wide.&nbsp;Adjusting or changing the belt and working height is very easy and takes only a few seconds. Because this belt grinder is a high-speed machine, it will grind much more quickly.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">Features</p><ul style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; outline: 0px; padding: 0px 0px 0px 20px; vertical-align: baseline; list-style: outside square; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Double speed motor</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Rapid belt changing system</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Vibration-free operation.</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Adjustable working height</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Incl. dust collection box.</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Incl. grinding belt 75×2000 mm for Highspeed 75 D and Incl. grinding belt 150 x 2000 mm for Highspeed 150 D</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Top table for flat grinding + deburring</li></ul><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Pipe Grinder 100 Triple Grind</strong></p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">The grinding machine for pipes Metal Wolff 100 Triple Grind is a multipurpose, revolutionary machine. A pipe notcher is located on the front side, a deburring table is located on the top, and a belt grinder is located on the backside. It only takes a few seconds to switch from pipe notcher to belt grinder! Furthermore, the powerful clamping mechanism increases the accuracy and successful results of the finished product.</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">Features</p><ul style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; outline: 0px; padding: 0px 0px 0px 20px; vertical-align: baseline; list-style: outside square; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Angle pipe notcher 30°-90° (0°-60°)</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Grinding roller 42,4mm standard.</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px 0px 10px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Grinding belt 100 x 2000 mm.</li><li style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s;\">Dust extraction optional.</li></ul><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; clear: both; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Types of Notching Processes</strong>&nbsp;&nbsp;</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">There are multiple different notching methods, each of which removes material from the outer edges of a workpiece using a different mechanism. For example, pipe notching is used to remove extra and undesirable material from hollow metal workpieces, whereas end notching is used to remove material from one or both ends of a hollow metal workpiece. There’s also side notching, which removes superfluous material from a tube’s side to allow it to be twisted and distorted further.&nbsp;&nbsp;</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">Further Reading:&nbsp;<a href=\"https://www.rockwoodmachinery.com/why-automation-in-welding-is-gaining-importance/\" target=\"_blank\" rel=\"noreferrer noopener\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(255, 94, 20);\">https://www.rockwoodmachinery.com/why-automation-in-welding-is-gaining-importance/</a></p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\"><strong style=\"background: transparent; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; font-weight: bold; transition: all 0.3s linear 0s;\">Uses of Pipe Notching Machine</strong>&nbsp;&nbsp;</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">Bike frames, tables with tube stands, building construction, automotive (seat frames, stabilizers, exhaust), cycle, motorcycle frames, irrigation systems, boiler tubes, material handling equipment, and refrigeration pipes are all common uses for notching machines. &nbsp;&nbsp;</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">Rockwood machinery provides high-quality pipe notching machines in UAE. Our machines are known for their high efficiency, long service life, ease of operation, cost-effectiveness, low maintenance costs, higher rates of profile matching with various tubes and notches, quality, and long-term performance. There are many different types of high-quality standard notching machines on the market, including pipe and tube notching machines, hydraulic notching machines, and standard notching machines. &nbsp;&nbsp;</p><p style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(102, 102, 102); font-family: Roboto;\">Know More About our&nbsp;<a href=\"http://rockwoodmachinery.com/products/plate-edge-bevelling/\" target=\"_blank\" rel=\"noreferrer noopener\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; margin: 0px; outline: 0px; padding: 0px; vertical-align: baseline; transition: all 0.3s linear 0s; color: rgb(255, 94, 20);\">Chamfering &amp; Plate Bevelling Machines in UAE</a></p>', 'content_rabrt3.jpg?t=rabrt3', '1', '2022-03-30 06:33:07'),
(7, 7, 'The Benefits of Rapid Prototyping for Industrialists', 'March 9, 2021', '<p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">Automatic welding is categorized into two: machine-based automatic welding and robotic welding. While the former is extremely beneficial in factory settings for repeated welding requirements, robotic welding is used for advanced industrial purposes which require higher grade of precision welding. Automation is efficient and consistent. More and more industries are adopting automation into its processes owing to its ability to perform an assigned task with the absolute precision until a technician makes changes to the parameters.Market analysis indicates that automated robotic welding in the manufacturing and industrial sectors will play a disruptive role in the next few years.</p><h3 style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 18px; margin-left: 0px; color: rgb(40, 40, 40); font-family: suisse-intl-bold; font-size: 30px;\">Popular Types of Welding</h3><ul style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-left: 18px; list-style-position: initial; list-style-image: initial; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"><li style=\"box-sizing: inherit; line-height: 30px;\">MIG Gas Metal Arc Welding (GMAW)</li><li style=\"box-sizing: inherit; line-height: 30px;\">TIG/Gas Tungsten Arc Welding (GTAW)</li><li style=\"box-sizing: inherit; line-height: 30px;\">Shielded Metal Arc Welding (SMAW) Stick Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Flux core arc (FCAW) Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Plasma Arc Welding (PAW</li><li style=\"box-sizing: inherit; line-height: 30px;\">Electron Beam and Laser Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Gas Welding</li><li style=\"box-sizing: inherit; line-height: 30px;\">Ultrasonic Welding</li></ul><h3 style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 18px; margin-left: 0px; color: rgb(40, 40, 40); font-family: suisse-intl-bold; font-size: 30px;\">Welding using Automation and Robots</h3><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">When automation is applied to industrial processes like welding, the overall throughput improves, work environment safety increases, and production efficiency is enhanced. Parts and assemblies of machines and other industrial tools done using automated welding rarely have defects and are inform in shape, size and form. Welding for such purposes is done using sensors, program, and logic controllers etc. This helps in controlling and managing specs. Mentioned below are some of the benefits applied to welding applications using robots.</p><ul style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding-left: 18px; list-style-position: initial; list-style-image: initial; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"><li style=\"box-sizing: inherit; line-height: 30px;\">Advanced “smart” technology helps collect information about operations, positioning, and wire or electrode status</li><li style=\"box-sizing: inherit; line-height: 30px;\">Software to perform real-time data analysis and adjustment machine movements</li><li style=\"box-sizing: inherit; line-height: 30px;\">Easy configuration and programming, where the technician can perform the movements on the robotic arm once and save and store the process as a program for future purposes</li><li style=\"box-sizing: inherit; line-height: 30px;\">No exposure to shielding gases, heat, and sparks, thereby enhancing operator</li></ul><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">With more and more industries exploring the opportunities of automation, the demand for automated welding machines are also increasing. Welding machines with robotic capabilities are available for rent and sale in Dubai and other major countries.</p><div class=\"industris-space-20 sm-hidden\" style=\"box-sizing: inherit; height: 20px; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"></div><div class=\"single-img\" style=\"box-sizing: inherit; display: table; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"><div class=\"img-left\" style=\"box-sizing: inherit; position: relative; width: 485.087px; float: left; padding-right: 15px;\"><img src=\"https://via.placeholder.com/457x352.png\" alt=\"\" style=\"box-sizing: inherit; border-width: 0px; border-color: initial; border-image: initial; height: auto; max-width: 100%; width: 470.087px;\"></div><div class=\"img-right\" style=\"box-sizing: inherit; position: relative; width: 284.896px; float: left; padding-left: 15px;\"><img src=\"https://via.placeholder.com/260x160.png\" alt=\"\" style=\"box-sizing: inherit; border-width: 0px; border-color: initial; border-image: initial; height: auto; max-width: 100%; margin-bottom: 30px; width: 269.896px;\">&nbsp;<img src=\"https://via.placeholder.com/260x160.png\" alt=\"\" style=\"box-sizing: inherit; border-width: 0px; border-color: initial; border-image: initial; height: auto; max-width: 100%; margin-bottom: 0px; width: 269.896px;\"></div></div><div class=\"industris-space-50\" style=\"box-sizing: inherit; height: 50px; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"></div><div class=\"industris-space-5\" style=\"box-sizing: inherit; height: 5px; color: rgb(37, 37, 37); font-family: suisse-intl-regular;\"></div><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">“The business landscape is changing rapidly, which means all types of companies need to consider where they fit in to new digital business processes.” says Bengt Nordström, CEO at Northstream. “With this analysis we also want to highlight the challenge for businesses to face digital transformation on their own – and thus the need to build partnerships with other actors in the ecosystem.”</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">In addition to this latest report, Telenor Connexion has published a number of guides and papers covering topics related to IoT and connected devices. Those include Connectivity Technologies for IoT, a guide which was first created in collaboration with Northstream in 2016 and updated at the end of 2018.</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 30px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">That paper, and a number of others, can be found on the Telenor Connexion Knowledge Hub. More papers and guides in this vein will be published in the months to come. Parties interested in Five Predictions for IoT and future publications can contact the marketing team at:<br style=\"box-sizing: inherit;\"><a class=\"text-second\" href=\"http://localhost/rockwood_html/post.html#\" style=\"box-sizing: inherit; transition: all 0.3s linear 0s; color: rgb(0, 174, 239) !important;\">hello@Industris.com</a></p>', 'content_rajcht.jpg?t=rajcht', '1', '2022-03-30 06:34:23'),
(8, 9, 'SMART MACHINES', 'Find the perfect machine to suit your needs', '<p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">We lead the market in offering a qualitative assortment of Machines that will help you achieve every one of your short and long-term business goals.</p><p style=\"box-sizing: inherit; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; font-family: suisse-intl-regular; line-height: 26px; color: rgb(37, 37, 37);\">Rockwood ensures that every piece of equipment we source out to industries in UAE and neighboring counties is on par with the global standards and advanced technology in compliance with universal norms. We provide equipment for a myriad of purposes and technical specifications.</p>', 'content_r9lye6.jpg?t=r9lye6', '1', '2022-03-31 06:24:20'),
(9, 20, 'Portable Pug Cutting Machines in UAE', 'test', '<h1 class=\"vc_custom_heading\" style=\"background-image: initial; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; border: 0px; font-size: 44px; margin-right: 0px; margin-bottom: 20px; margin-left: 0px; outline: 0px; padding: 0px; vertical-align: baseline; line-height: normal; font-weight: 600; font-family: Poppins; letter-spacing: 0.5px; color: rgb(0, 0, 0); transition: all 0.3s linear 0s;\"><span style=\"color: rgb(102, 102, 102); font-family: Roboto; font-size: 16px; font-weight: 400; letter-spacing: normal;\">Portable Pug Cutting Machines in UAE is widely used by heavy-duty Fabricators which includes gas cutting machine and it helps to reduce production costs and thus improves the efficiency of work. RockWoods’ high-quality Pug Cutting Machines and are designed to do the work with 100% precision. We have a wide range of Pug cutting Machines for straight-line cutting of Plates, Pipes, to cut the Beam.</span><br></h1>', '', '1', '2022-03-31 07:51:09'),
(10, 32, 'Micheal Sejuro', 'Manager Fusion themes company', '\"A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.\"', '', '1', '2022-04-01 05:38:06'),
(11, 32, 'Emilia Clarke', 'Manager Avenger company', '\"I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.\"\r\n', '', '1', '2022-04-01 05:38:52'),
(12, 32, 'Emilia Clarke', 'Manager Avenger company', '“I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment.”\r\n', '', '1', '2022-04-01 05:41:14'),
(13, 3, 'Steel Fabrication Machinery', '', '<h2>Steel Fabrication Machinery <span class=\"text-primary\">Products in UAE</span></h2>\r\n									<p>Explore our state of the art product range caters the whole steel fabrication industry from cutting to metal forming to marking to material finishing. Based on the application, we have wide variety of Steel fabrication machinery products in UAE. Let us try it out.							</p>', 'content_r9yz3h.jpg?t=r9yz3h', '1', '2022-04-07 07:08:12'),
(14, 21, 'Portable Bandsaw Machine in UAE', 'test', '<span style=\"color: rgb(102, 102, 102); font-family: Roboto; text-align: justify;\">RockWood has a huge collection of economical Portable Bandsaw Machine in UAE with the best quality. The smaller models Bandsaws are easier to carry and can be used in the factory as well as at site. Depending on your job requirements, we have a wide range of models available from basic to completely automatic models, and please get in touch with your sales team to select the model which suits your requirement.</span>', '', '1', '2022-04-18 12:54:58');

-- --------------------------------------------------------

--
-- Table structure for table `sliders_list`
--

CREATE TABLE `sliders_list` (
  `sl_id` int(10) UNSIGNED NOT NULL,
  `sl_ml_id` int(11) NOT NULL,
  `sl_img` varchar(500) NOT NULL,
  `sl_content` text DEFAULT NULL,
  `sl_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive  , 1- active',
  `sl_create` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders_list`
--

INSERT INTO `sliders_list` (`sl_id`, `sl_ml_id`, `sl_img`, `sl_content`, `sl_active`, `sl_create`) VALUES
(1, 1, 'slider_r9i8jh.jpg?t=r9i8jh', '<h1><span class=\"text-primary\">WELD IT <br>RIGHT WITH</span> ROCKWOOD</h1>\r\n									<p>We provide a range of state-of-the-art heavy machines and industrial equipment for effectively and timely management operation and on-time completion of projects.</p>', '1', '2022-03-29 00:43:54'),
(2, 1, 'slider_r9i8xp.jpg?t=r9i8xp', '<h1><span class=\"text-primary\">Leader in Commercial and industrial\r\n									</span> ENGINEERING</h1>\r\n									<p>100% safety compliant and suitable for heavy industry or regulated site</p>', '1', '2022-03-29 00:51:27'),
(3, 1, 'slider_r9i8ym.jpg?t=r9i8ym', '<h1><span class=\"text-primary\">Steel fabrication & </span>machinery products</h1>\r\n									<p>We provide a range of state-of-the-art heavy machines and industrial equipment for effectively and timely management operation and on-time completion of projects.</p>\r\n									', '1', '2022-03-29 00:52:00'),
(4, 2, 'slider_r9jsro.jpg?t=r9jsrp', '											Please enter the content here ....\r\n										', '1', '2022-03-29 20:57:27'),
(5, 4, 'slider_r9k0yc.jpg?t=r9k0yc', 'Machines for Rent in Dubai						', '1', '2022-03-29 23:54:14'),
(6, 5, 'slider_r9k1cp.jpg?t=r9k1cp', 'Our Services', '1', '2022-03-30 00:02:51'),
(7, 8, 'slider_r9k294.jpg?t=r9k294', 'Contact Us', '1', '2022-03-30 00:22:40'),
(8, 9, 'slider_r9lz1i.jpg?t=r9lz1i', 'Cutting & Drilling', '1', '2022-03-31 01:08:15'),
(9, 20, 'slider_r9m2gs.jpg?t=r9m2gs', '											Please enter the content here ....\r\n										', '1', '2022-03-31 02:22:06'),
(10, 20, 'slider_r9m2h9.jpg?t=r9m2h9', '											Please enter the content here ....\r\n										', '1', '2022-03-31 02:22:23'),
(11, 20, 'slider_r9m2ht.jpg?t=r9m2ht', '											Please enter the content here ....\r\n										', '1', '2022-03-31 02:22:43'),
(12, 20, 'slider_r9m2i8.jpg?t=r9m2i8', '											Please enter the content here ....\r\n										', '1', '2022-03-31 02:22:58'),
(13, 20, 'slider_r9m2io.jpg?t=r9m2io', '											Please enter the content here ....\r\n										', '1', '2022-03-31 02:23:14'),
(14, 20, 'slider_r9m2j3.jpg?t=r9m2j3', '											Please enter the content here ....\r\n										', '1', '2022-03-31 02:23:28'),
(15, 6, 'slider_r9pr81.jpg?t=r9pr81', 'Our Projects ', '1', '2022-04-02 02:09:39'),
(16, 7, 'slider_r9tnrh.jpg?t=r9tnrh', 'Blogs', '1', '2022-04-04 04:45:19'),
(17, 21, 'slider_rajd3t.jpg?t=rajd3t', '											Please enter the content here ....\r\n										', '1', '2022-04-18 12:52:45'),
(18, 21, 'slider_rajd4i.jpg?t=rajd4i', '											Please enter the content here ....\r\n										', '1', '2022-04-18 12:53:08');

-- --------------------------------------------------------

--
-- Table structure for table `video_gallery_list`
--

CREATE TABLE `video_gallery_list` (
  `vgl_id` int(10) UNSIGNED NOT NULL,
  `vgl_ml_id` int(11) NOT NULL,
  `vgl_url` varchar(500) NOT NULL,
  `vgl_active` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0 - inactive  , 1- active',
  `vgl_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video_gallery_list`
--

INSERT INTO `video_gallery_list` (`vgl_id`, `vgl_ml_id`, `vgl_url`, `vgl_active`, `vgl_created`) VALUES
(1, 20, 'https://www.youtube.com/embed/If614gVPMR4', '1', '2022-04-05 09:54:24'),
(2, 20, 'https://www.youtube.com/embed/kvjcePRwq-o', '1', '2022-04-05 09:54:42'),
(3, 20, 'https://www.youtube.com/embed/IZRAXgFlwpA', '1', '2022-04-05 09:54:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `gallery_list`
--
ALTER TABLE `gallery_list`
  ADD PRIMARY KEY (`gl_id`);

--
-- Indexes for table `gallery_title_list`
--
ALTER TABLE `gallery_title_list`
  ADD PRIMARY KEY (`gtl_id`);

--
-- Indexes for table `menus_list`
--
ALTER TABLE `menus_list`
  ADD PRIMARY KEY (`ml_id`);

--
-- Indexes for table `page_content_list`
--
ALTER TABLE `page_content_list`
  ADD PRIMARY KEY (`pcl_id`);

--
-- Indexes for table `sliders_list`
--
ALTER TABLE `sliders_list`
  ADD PRIMARY KEY (`sl_id`);

--
-- Indexes for table `video_gallery_list`
--
ALTER TABLE `video_gallery_list`
  ADD PRIMARY KEY (`vgl_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `gallery_list`
--
ALTER TABLE `gallery_list`
  MODIFY `gl_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `gallery_title_list`
--
ALTER TABLE `gallery_title_list`
  MODIFY `gtl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `menus_list`
--
ALTER TABLE `menus_list`
  MODIFY `ml_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `page_content_list`
--
ALTER TABLE `page_content_list`
  MODIFY `pcl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `sliders_list`
--
ALTER TABLE `sliders_list`
  MODIFY `sl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `video_gallery_list`
--
ALTER TABLE `video_gallery_list`
  MODIFY `vgl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
